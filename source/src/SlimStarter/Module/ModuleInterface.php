<?php
namespace SlimStarter\Module;

interface ModuleInterface{
    public function getModuleName();
    public function getModuleAccessor();
    public function getTemplatePath();
    public function registerAdminRoute();
    public function registerStaffRoute();
    public function registerEstablishmentRoute();
    public function registerAdminMenu();
    public function registerStaffMenu();
    public function registerEstablishmentMenu();
    public function registerHook();
    public function boot();
    public function install();
    public function uninstall();
    public function activate();
    public function deactivate();
}