<?php
class Town extends Model {
    protected $table = "town";
    
    public function getAllTowns() {
    	$towns = Town::all();
    	foreach ($towns as $key => $town) {
			$brgys = Barangay::where("town_id","=",$town->id)->get();
			$town -> barangays = $brgys;
		}
		return $towns;
    }
}
