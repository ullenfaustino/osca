<?php

namespace Products\Controllers;

use \User;
use \Users;
use \App;
use \View;
use \Menu;
use \Input;
use \Sentry;
use \Request;
use \Response;
use \Exception;
use \Establishment\BaseController;
use \Cartalyst\Sentry\Users\UserNotFoundException;

use \EstablishmentProducts;
use \EstablishmentUsers;
use \CodeGenerator;

class ProductsController extends BaseController {

	public function __construct() {
		parent::__construct();
		$this -> data['active_menu'] = 'establishment_products';
	}

	public function index() {
		$user = Sentry::getUser();
		$this -> data['title'] = 'Products';
		
		$est_user = EstablishmentUsers::where("user_id", "=", $user -> id) -> first();
		$establishment_id = $est_user -> establishment_id;
		$this -> data["products"] = EstablishmentProducts::where("establishment_id", "=", $establishment_id) -> get();

		App::render('@products/index.twig', $this -> data);
	}

	public function addProducts() {
		$user = Sentry::getUser();
		$est_user = EstablishmentUsers::where("user_id", "=", $user -> id) -> first();
		$establishment_id = $est_user -> establishment_id;

		$item_id = Input::post("item_id");
		$item_name = Input::post("item_name");
		$item_description = Input::post("item_description");
		$srp = Input::post("srp");
		$item_type = Input::post("item_type");

		$product = EstablishmentProducts::find($item_id);
		if (!$product) {
			$codeGen = new CodeGenerator();
			$product = new EstablishmentProducts();
			$product -> establishment_id = $establishment_id;
			$product -> ref_code = $codeGen -> getToken(12);
		}
		$product -> item_name = $item_name;
		$product -> item_description = $item_description;
		$product -> srp = $srp;
		$product -> item_type = $item_type;
		$product -> save();

		App::flash('message', "Product Successfully Saved.");
		App::flash('message_status', true);
		Response::redirect($this -> siteUrl('establishment/products'));
	}

}
