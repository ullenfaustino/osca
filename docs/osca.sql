-- phpMyAdmin SQL Dump
-- version 4.6.0
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Mar 22, 2017 at 12:40 AM
-- Server version: 5.7.12-1~exp1+deb.sury.org~trusty+1
-- PHP Version: 5.5.9-1ubuntu4.17

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `osca`
--

-- --------------------------------------------------------

--
-- Table structure for table `barangay`
--

CREATE TABLE `barangay` (
  `id` int(11) NOT NULL,
  `town_id` int(11) DEFAULT NULL,
  `name` text,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `barangay`
--

INSERT INTO `barangay` (`id`, `town_id`, `name`, `created_at`, `updated_at`) VALUES
(1, 1, 'San Jose', '2017-02-11 02:21:48', NULL),
(2, 1, 'Ibayo', '2017-02-11 02:23:54', NULL),
(3, 1, 'Tenejero', '2017-02-11 02:23:54', NULL),
(4, 1, 'Malabia', '2017-02-11 02:24:23', NULL),
(5, 1, 'Tuyo', '2017-02-11 02:24:23', NULL),
(6, 1, 'Cupang North', '2017-02-11 02:25:11', NULL),
(7, 1, 'Cupang Proper', '2017-02-11 02:25:11', NULL),
(8, 1, 'Cupang West', '2017-02-11 02:25:26', NULL),
(9, 1, 'Poblacion', '2017-02-11 02:26:10', NULL),
(10, 1, 'Cataning', '2017-02-11 02:26:10', NULL),
(11, 1, 'Bagumbayan', '2017-02-23 04:49:29', '2017-02-23 04:49:29'),
(12, 1, 'Cabog-Cabog', '2017-02-23 04:49:38', '2017-02-23 04:49:38'),
(13, 1, 'Camacho', '2017-02-23 04:49:44', '2017-02-23 04:49:44'),
(14, 1, 'Central', '2017-02-23 04:49:50', '2017-02-23 04:49:50'),
(15, 1, 'Dangcol', '2017-02-23 04:49:55', '2017-02-23 04:49:55'),
(16, 1, 'Doña Francisca', '2017-02-23 04:50:01', '2017-02-23 04:50:01'),
(17, 1, 'Munting Batangas', '2017-02-23 04:50:07', '2017-02-23 04:50:07'),
(18, 1, 'Pto. Rivas Ibaba', '2017-02-23 04:50:18', '2017-02-23 04:50:18'),
(19, 1, 'Pto. Rivas Itaas', '2017-02-23 04:50:24', '2017-02-23 04:50:24'),
(20, 1, 'Lote Pto. Rivas', '2017-02-23 04:50:30', '2017-02-23 04:50:30'),
(21, 1, 'Sibacan', '2017-02-23 04:50:36', '2017-02-23 04:50:36'),
(22, 1, 'Talisay', '2017-02-23 04:50:41', '2017-02-23 04:50:41'),
(23, 1, 'Tanato', '2017-02-23 04:50:49', '2017-02-23 04:50:49'),
(24, 1, 'Tortugas', '2017-02-23 04:50:54', '2017-02-23 04:50:54');

-- --------------------------------------------------------

--
-- Table structure for table `barangay_users`
--

CREATE TABLE `barangay_users` (
  `user_id` int(11) DEFAULT NULL,
  `barangay_id` int(11) DEFAULT NULL,
  `is_admin` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `barangay_users`
--

INSERT INTO `barangay_users` (`user_id`, `barangay_id`, `is_admin`) VALUES
(5, 1, 1),
(4, 2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `booklet`
--

CREATE TABLE `booklet` (
  `id` int(11) NOT NULL,
  `booklet_transaction_id` int(11) DEFAULT NULL,
  `commodities` text,
  `item_name` text,
  `item_description` text,
  `quantity` decimal(10,2) DEFAULT '0.00',
  `total_amount` decimal(10,2) DEFAULT '0.00',
  `discount` decimal(10,2) DEFAULT '0.00',
  `discounted_amount` decimal(10,2) DEFAULT '0.00',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `booklet`
--

INSERT INTO `booklet` (`id`, `booklet_transaction_id`, `commodities`, `item_name`, `item_description`, `quantity`, `total_amount`, `discount`, `discounted_amount`, `created_at`, `updated_at`) VALUES
(1, 1, 'Basic Commodities', 'Bactidol', 'Antibiotic', '2.00', '1000.00', '3.00', '970.00', '2017-03-21 16:30:16', '2017-03-21 16:30:16'),
(2, 1, 'Basic Commodities', 'sample others 1', '"Basic"', '5.00', '250.00', '3.00', '242.50', '2017-03-21 16:30:16', '2017-03-21 16:30:16'),
(3, 2, 'Medicine', 'Bactidol', 'Antibiotic', '1.00', '500.00', '3.00', '485.00', '2017-03-21 16:38:59', '2017-03-21 16:38:59');

-- --------------------------------------------------------

--
-- Table structure for table `booklet_transaction`
--

CREATE TABLE `booklet_transaction` (
  `id` int(11) NOT NULL,
  `trans_code` varchar(500) DEFAULT NULL,
  `sc_member_id` int(11) DEFAULT NULL,
  `establishment_id` int(11) DEFAULT NULL,
  `total_amount` decimal(10,2) DEFAULT '0.00',
  `discount` decimal(10,2) DEFAULT '0.00',
  `discounted_amount` decimal(10,2) DEFAULT '0.00',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `booklet_transaction`
--

INSERT INTO `booklet_transaction` (`id`, `trans_code`, `sc_member_id`, `establishment_id`, `total_amount`, `discount`, `discounted_amount`, `created_at`, `updated_at`) VALUES
(1, '2017aEX-0322ly2-002948QA1', 1, 2, '1250.00', '0.00', '0.00', '2017-03-21 16:30:16', '2017-03-21 16:30:16'),
(2, '2017U0b-03222Bd-003845KU7', 1, 2, '500.00', '0.00', '0.00', '2017-03-21 16:38:59', '2017-03-21 16:38:59');

-- --------------------------------------------------------

--
-- Table structure for table `contributions`
--

CREATE TABLE `contributions` (
  `id` int(11) NOT NULL,
  `trans_code` varchar(255) DEFAULT NULL,
  `sc_member_id` int(11) DEFAULT NULL,
  `barangay_id` int(11) DEFAULT NULL,
  `amount` decimal(10,2) DEFAULT '0.00',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `establishment`
--

CREATE TABLE `establishment` (
  `id` int(11) NOT NULL,
  `name` text,
  `description` text,
  `address` text,
  `is_pharmacy` tinyint(1) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `establishment`
--

INSERT INTO `establishment` (`id`, `name`, `description`, `address`, `is_pharmacy`, `created_at`, `updated_at`) VALUES
(1, 'Mercury Drug', 'Pharmaceutical Drug store', 'Balanga Public Market Branch', 1, '2017-02-11 07:26:47', '2017-02-11 11:28:18'),
(2, 'Generika Pharmacy', 'Pharmaceutical Drug store', 'Balanga Public Market Branch', 1, '2017-02-11 07:27:42', NULL),
(3, 'PureGold', 'General Merchandise', 'Poblacion Balanga City Bataan', 0, '2017-02-11 07:29:22', NULL),
(4, 'Elizabeth Supermarket', 'General Merchandise', 'San Jose Balanga City Bataan', 0, '2017-02-11 11:28:56', '2017-02-11 11:28:56');

-- --------------------------------------------------------

--
-- Table structure for table `establishment_products`
--

CREATE TABLE `establishment_products` (
  `id` int(11) NOT NULL,
  `establishment_id` int(11) DEFAULT NULL,
  `ref_code` varchar(500) DEFAULT NULL,
  `item_name` text,
  `item_description` text,
  `srp` decimal(10,2) DEFAULT '0.00',
  `item_type` tinyint(1) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `establishment_products`
--

INSERT INTO `establishment_products` (`id`, `establishment_id`, `ref_code`, `item_name`, `item_description`, `srp`, `item_type`, `created_at`, `updated_at`) VALUES
(1, 2, 'Cf39QVH0jx2t', 'Bactidol', 'Antibiotic', '500.00', 2, '2017-03-08 07:46:56', '2017-03-08 08:18:53'),
(2, 2, 'FznF4tmms97Q', 'sample others 1', '"Basic"', '50.00', 2, '2017-03-21 16:08:36', '2017-03-21 16:08:36'),
(3, 2, 'SdQFTaJLQxBM', 'sample basic 1', 'sample basic 1', '150.00', 2, '2017-03-21 16:08:50', '2017-03-21 16:08:50');

-- --------------------------------------------------------

--
-- Table structure for table `establishment_users`
--

CREATE TABLE `establishment_users` (
  `user_id` int(11) DEFAULT NULL,
  `establishment_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `establishment_users`
--

INSERT INTO `establishment_users` (`user_id`, `establishment_id`) VALUES
(6, 3),
(3, 2);

-- --------------------------------------------------------

--
-- Table structure for table `events`
--

CREATE TABLE `events` (
  `id` int(11) NOT NULL,
  `ref_code` varchar(255) DEFAULT NULL,
  `barangay_id` int(11) DEFAULT NULL,
  `title` text,
  `content` longtext,
  `thumbnail` varchar(500) DEFAULT NULL,
  `budget` decimal(10,2) DEFAULT '0.00',
  `event_start` datetime DEFAULT NULL,
  `event_end` datetime DEFAULT NULL,
  `notif_date` datetime DEFAULT NULL,
  `event_color` varchar(250) DEFAULT '#4C90B1',
  `is_notified` tinyint(1) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `events`
--

INSERT INTO `events` (`id`, `ref_code`, `barangay_id`, `title`, `content`, `thumbnail`, `budget`, `event_start`, `event_end`, `notif_date`, `event_color`, `is_notified`, `created_at`, `updated_at`) VALUES
(1, 'eQeLIqDI', 1, 'Sample Event 1', 'Event 1 Content', NULL, '5000.00', '2017-02-20 08:00:00', '2017-02-24 17:00:00', '2017-02-18 23:10:00', '#0dee19', 1, '2017-02-18 15:11:00', '2017-02-18 15:17:39'),
(2, '0nrRGfTf', 24, 'kahit ano', '<p style="text-align: center;">zumba class</p>', NULL, '5000.00', '2017-03-21 08:00:00', '2017-03-21 17:00:00', '2017-03-20 20:51:00', '#20fd00', 1, '2017-03-20 20:21:18', '2017-03-20 20:51:31');

-- --------------------------------------------------------

--
-- Table structure for table `general_settings`
--

CREATE TABLE `general_settings` (
  `id` int(11) NOT NULL,
  `module_name` text,
  `content` text,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE `groups` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `permissions` text,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `name`, `permissions`, `created_at`, `updated_at`) VALUES
(1, 'Administrators', '{"admin":1}', '2017-02-11 02:22:36', '2017-02-11 02:22:36'),
(2, 'Establishment', '{"establishment":1}', '2017-02-11 02:22:36', '2017-02-11 02:22:36'),
(3, 'Staff', '{"staff":1}', '2017-02-11 02:22:36', '2017-02-11 02:22:36');

-- --------------------------------------------------------

--
-- Table structure for table `master_password`
--

CREATE TABLE `master_password` (
  `id` int(11) NOT NULL,
  `password` varchar(255) DEFAULT '',
  `is_active` tinyint(1) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `master_password`
--

INSERT INTO `master_password` (`id`, `password`, `is_active`, `created_at`, `updated_at`) VALUES
(1, 'eb0a191797624dd3a48fa681d3061212', 1, '2015-05-24 11:19:47', '2015-06-17 01:36:33');

-- --------------------------------------------------------

--
-- Table structure for table `sc_members`
--

CREATE TABLE `sc_members` (
  `id` int(11) NOT NULL,
  `rf_id` varchar(50) DEFAULT NULL,
  `ref_code` varchar(50) DEFAULT NULL,
  `barangay_id` int(11) DEFAULT NULL,
  `first_name` varchar(255) DEFAULT NULL,
  `middle_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `gender` varchar(50) DEFAULT NULL,
  `birthday` date DEFAULT NULL,
  `address` text,
  `mobile` varchar(50) DEFAULT NULL,
  `landline` varchar(50) DEFAULT NULL,
  `is_deceased` tinyint(4) DEFAULT '0',
  `deceased_date` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `sc_members`
--

INSERT INTO `sc_members` (`id`, `rf_id`, `ref_code`, `barangay_id`, `first_name`, `middle_name`, `last_name`, `gender`, `birthday`, `address`, `mobile`, `landline`, `is_deceased`, `deceased_date`, `created_at`, `updated_at`) VALUES
(1, '0123456789', 'v1GSR2AeKq', 1, 'Mike', 'Gerard', 'Tyson', 'Male', '1952-06-30', '537 Rose st. San Jose Balanga City Bataan', '09165465649', '2372918', 0, NULL, '2017-02-18 15:15:23', '2017-03-20 19:55:36'),
(2, '1', 'NRyLE2cJBr', 1, 'q', 'q', 'q', 'Male', '1932-02-02', 'q', '1', '1', 0, NULL, '2015-02-23 04:53:24', '2017-02-23 04:53:24'),
(3, '2', 'rmByqL1gSC', 1, 'w', 'w', 'w', 'Male', '1934-01-31', 'q', '2', '2', 0, NULL, '2015-02-23 04:54:01', '2017-02-23 04:54:01'),
(4, '3', 'XLxfAFyYgv', 1, 'r', 'r', 'r', 'Male', '1935-04-02', 'w', '1', '1', 0, NULL, '2014-02-23 04:54:21', '2017-02-23 04:54:21'),
(5, '4', '0uutzqEdBQ', 1, 'q', 'q', 'q', 'Male', '1930-02-03', 'w', '2', '2', 0, NULL, '2014-02-23 04:54:51', '2017-02-23 04:54:51'),
(6, '5', 'vfhYUxh3rn', 1, 'q', 'q', 'q', 'Female', '1930-07-06', 'q', '1', '1', 0, NULL, '2014-02-23 04:55:11', '2017-02-23 04:55:11'),
(7, '6', 'PWnt3w9xZP', 1, 'q', 'q', 'q', 'Male', '1920-09-04', 'q', '1', '1', 0, NULL, '2014-02-23 04:55:44', '2017-02-23 04:55:42'),
(8, '7', '7umh2FxQx9', 1, 'q', 'q', 'q', 'Male', '1921-01-05', 'q', '1', '1', 0, NULL, '2015-02-23 04:56:06', '2017-02-23 04:56:06'),
(9, '8', 'IqGKmKtNlU', 1, 'q', 'q', 'q', 'Male', '1922-08-08', 'r', '1', '1', 0, NULL, '2015-02-23 04:56:33', '2017-02-23 04:56:33'),
(10, '9', 'b0wqWIDN40', 1, 'q', 'q', 'q', 'Male', '1938-10-11', 'qw', '1', '1', 0, NULL, '2014-02-23 04:57:51', '2017-02-23 04:57:51'),
(11, '10', 'SbQRjLjdgs', 1, 'q', 'q', 'q', 'Male', '1929-11-11', 'w', '1', '1', 1, '2017-02-16', '2015-02-23 04:58:18', '2017-02-23 05:19:20'),
(12, '11', 'pBf59Eca0a', 1, 'q', 'q', 'q', 'Male', '1919-05-05', 'q', '1', '1', 1, '2014-04-04', '2017-02-23 04:59:38', '2017-02-23 05:21:01'),
(13, '12', '2CrR0h1tlx', 1, 'w', 'w', 'w', 'Male', '1920-04-04', 'w', '1', '1', 1, '2014-03-04', '2017-02-23 04:59:54', '2017-02-23 05:22:19'),
(14, '13', 'bnXYPJ8DLV', 1, 'w', 'w', 'w', 'Male', '1925-01-01', 'w', '1', '1', 1, '2011-08-04', '2017-02-23 05:00:11', '2017-02-23 05:03:58'),
(15, '14', 'WtWqsdyuvD', 1, 'w', 'w', 'w', 'Female', '1930-09-09', 'w', '1', '1', 1, '2015-02-04', '2017-02-23 05:00:43', '2017-02-23 05:23:25'),
(16, '15', '9JazvPhrXy', 1, 'q', 'q', 'q', 'Male', '1955-03-21', 'w', '1', '1', 1, '2017-01-02', '2017-02-23 05:01:22', '2017-02-23 05:23:49'),
(17, '1', 'Ei9w9ij6El', 2, 'q', 'q', 'q', 'Male', '1955-03-04', 'Ibayo', '09165465649', '1234567', 0, NULL, '2017-02-23 05:34:31', '2017-02-25 00:43:31'),
(18, '00000', '6y6Fm1W1kh', 24, 'q', 'q', 'q', 'Female', '1955-02-03', 'q', '09174539334', NULL, 0, NULL, '2017-03-20 20:19:35', '2017-03-20 20:35:38'),
(19, '00001', 'kGTqktI6TH', 24, 'q', 'q', 'q', 'Female', '1943-04-02', 'q', '09466467154', NULL, 0, NULL, '2017-03-20 20:31:34', '2017-03-20 20:31:34'),
(20, '000002', 'vrwqjenYFL', 24, 'w', 'w', 'w', 'Female', '1949-01-01', 'w', '09062341196', NULL, 1, '2011-02-04', '2017-03-20 20:35:21', '2017-03-20 20:50:04'),
(21, '000005', 'vJhrGrQkNw', 24, 'w', 'q', 'q', 'Male', '1949-02-02', 'q', '09175224154', NULL, 0, NULL, '2017-03-20 20:45:28', '2017-03-20 20:45:52');

-- --------------------------------------------------------

--
-- Table structure for table `throttle`
--

CREATE TABLE `throttle` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `ip_address` varchar(45) DEFAULT NULL,
  `attempts` varchar(45) DEFAULT NULL,
  `suspended` tinyint(1) DEFAULT NULL,
  `banned` tinyint(1) DEFAULT NULL,
  `last_attempt_at` timestamp NULL DEFAULT NULL,
  `suspended_at` timestamp NULL DEFAULT NULL,
  `banned_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `town`
--

CREATE TABLE `town` (
  `id` int(11) NOT NULL,
  `name` text,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `town`
--

INSERT INTO `town` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Balanga City', '2017-02-11 02:21:10', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `ref_code` varchar(255) DEFAULT 'XXXX-XXXX-XXXX',
  `username` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `canonical_hash` varchar(500) DEFAULT NULL,
  `full_name` varchar(255) DEFAULT '',
  `avatar` varchar(500) DEFAULT 'default-avatar.png',
  `permissions` text,
  `user_type` tinyint(1) DEFAULT '0' COMMENT '1=Admin, 2=brgy_admin, 3=establishment',
  `activated` tinyint(1) DEFAULT '0',
  `activation_code` varchar(255) DEFAULT '',
  `activated_at` timestamp NULL DEFAULT NULL,
  `last_login` timestamp NULL DEFAULT NULL,
  `persist_code` varchar(255) DEFAULT NULL,
  `reset_password_code` varchar(255) DEFAULT NULL,
  `is_registered` tinyint(1) DEFAULT '0',
  `is_primary` tinyint(1) DEFAULT '1',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `ref_code`, `username`, `email`, `password`, `canonical_hash`, `full_name`, `avatar`, `permissions`, `user_type`, `activated`, `activation_code`, `activated_at`, `last_login`, `persist_code`, `reset_password_code`, `is_registered`, `is_primary`, `created_at`, `updated_at`) VALUES
(1, 'BkwA1486779757ETNU', 'admin1', 'admin1@osca.org', '$2y$10$TObE7RWk3.R0UiIbLKnw..7QZiqkVxGMu0hqFIbrUl.23MPbhzTzG', 'cGFzc3dvcmQ=', 'Administrator 1', 'default-avatar.png', '{"admin":1}', 1, 1, '', NULL, '2017-03-21 15:56:17', '$2y$10$ZyndXwDkcK7Fw6A0n4zIzuznPOWkTYJpQUnIWqKFpI1WY6iHRSOqS', NULL, 0, 1, '2017-02-11 02:22:37', '2017-03-21 15:56:17'),
(2, '2aNr1486779757fzeE', 'admin2', 'admin2@osca.org', '$2y$10$fVdyqlO1ivkL.NKGmHobQ.7y4UufHhyz0p8ukBl8GB2iLvQJAux/i', 'cGFzc3dvcmQ=', 'Administrator 2', 'default-avatar.png', '{"admin":1}', 1, 1, '', NULL, NULL, NULL, NULL, 0, 1, '2017-02-11 02:22:37', '2017-02-11 02:22:37'),
(3, 'bkaz1486779757DY28', 'store1', 'store1@osca.org', '$2y$10$99JHDW2XH.YoUQdNjImQ.exh0vCciW1z3NEaJXKiJ6ktJ4G1IXNW.', 'cGFzc3dvcmQ=', 'user1', 'default-avatar.png', '{"establishment":1}', 3, 1, '', NULL, '2017-03-21 16:17:27', '$2y$10$3QNiZJ5qcSHhx0iHqdiryup0v55ykO51162QIEzjjQXPvndfhM.Ii', NULL, 0, 1, '2017-02-11 02:22:37', '2017-03-21 16:17:27'),
(4, 'vnJn14867797579sUy', 'staff1', 'martin040694@gmail.com', '$2y$10$J4LLj67TnQkxiw/QRMbl5e4oF2Iv/U3VmA.2G2HehlpLDxRnB9Xx2', 'cGFzc3dvcmQ=', 'Martin Charles T. Irog', 'default-avatar.png', '{"staff":1}', 2, 1, '', NULL, '2017-03-20 19:35:53', '$2y$10$uAcpmYClS8dkV9gXkyve/O9oAYKjes4CpIqq8yHaPHNVwWlSyUNLy', NULL, 0, 1, '2017-02-11 02:22:37', '2017-03-20 19:35:53'),
(5, 'aTuw1486781583XWbK', 'san_jose_lX9hk', 'martin040694@gmail.com', '$2y$10$c62B5hjcDBvNpzBTtlXBweWkbiZt0AioQRMbH7zNH3V4yZ8SkYtTy', 'VjBySXE=', 'Martin Charles T. Irog', 'default-avatar.png', '{"staff":1}', 2, 1, '', NULL, NULL, NULL, NULL, 0, 1, '2017-02-11 02:53:03', '2017-03-20 19:28:17'),
(6, 'qLci1486798926kVU3', '58FFU', 'martin040694@gmail.com', '$2y$10$0z3lYwxDsdmN8R0nusPcCeBtlLyZJ2BLZrF3/QVIRUl.mITGZJa2y', 'NlJHcUM=', 'Martin Charles T. Irog', 'default-avatar.png', '{"establishment":1}', 3, 1, '', NULL, '2017-03-21 16:16:53', '$2y$10$24rRuf9CWfpGkL7UEJ4ujOcNNb8o1SIPXmKn6ubsm14QR4HvhId7a', NULL, 0, 1, '2017-02-11 07:42:06', '2017-03-21 16:16:53');

-- --------------------------------------------------------

--
-- Table structure for table `users_groups`
--

CREATE TABLE `users_groups` (
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users_groups`
--

INSERT INTO `users_groups` (`user_id`, `group_id`) VALUES
(1, 1),
(2, 1),
(3, 2),
(6, 2),
(4, 3),
(5, 3);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `barangay`
--
ALTER TABLE `barangay`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `booklet`
--
ALTER TABLE `booklet`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `booklet_transaction`
--
ALTER TABLE `booklet_transaction`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contributions`
--
ALTER TABLE `contributions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `establishment`
--
ALTER TABLE `establishment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `establishment_products`
--
ALTER TABLE `establishment_products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `events`
--
ALTER TABLE `events`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `general_settings`
--
ALTER TABLE `general_settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `master_password`
--
ALTER TABLE `master_password`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sc_members`
--
ALTER TABLE `sc_members`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `throttle`
--
ALTER TABLE `throttle`
  ADD PRIMARY KEY (`id`,`user_id`),
  ADD KEY `fk_throttle_users_idx` (`user_id`);

--
-- Indexes for table `town`
--
ALTER TABLE `town`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users_groups`
--
ALTER TABLE `users_groups`
  ADD PRIMARY KEY (`user_id`,`group_id`),
  ADD KEY `fk_users_group_groups1_idx` (`group_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `barangay`
--
ALTER TABLE `barangay`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `booklet`
--
ALTER TABLE `booklet`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `booklet_transaction`
--
ALTER TABLE `booklet_transaction`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `contributions`
--
ALTER TABLE `contributions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `establishment`
--
ALTER TABLE `establishment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `establishment_products`
--
ALTER TABLE `establishment_products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `events`
--
ALTER TABLE `events`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `general_settings`
--
ALTER TABLE `general_settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `groups`
--
ALTER TABLE `groups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `master_password`
--
ALTER TABLE `master_password`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `sc_members`
--
ALTER TABLE `sc_members`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `throttle`
--
ALTER TABLE `throttle`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `town`
--
ALTER TABLE `town`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `throttle`
--
ALTER TABLE `throttle`
  ADD CONSTRAINT `fk_throttle_users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `users_groups`
--
ALTER TABLE `users_groups`
  ADD CONSTRAINT `fk_users_group_groups1` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_users_group_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
