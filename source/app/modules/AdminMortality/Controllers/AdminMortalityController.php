<?php

namespace AdminMortality\Controllers;

use \raw;
use \User;
use \Users;
use \App;
use \View;
use \Menu;
use \Input;
use \Sentry;
use \Request;
use \Response;
use \Exception;
use \Admin\BaseController;
use \Cartalyst\Sentry\Users\UserNotFoundException;

use \SCMembers;
use \Town;
use \GenericHelper;
use \Barangay;

class AdminMortalityController extends BaseController {

	public function __construct() {
		parent::__construct();
		$this -> data['active_menu'] = 'admin_mortality';
	}

	public function index() {
		$user = Sentry::getUser();
		$this -> data['title'] = 'Mortality Rate';

		$towns = Town::getAllTowns();
        $this -> data['towns'] = $towns;
        
        $years = $this -> getYears();
		$this -> data['years'] = $years;
		
		App::render('@adminmortality/index.twig', $this -> data);
	}
    
    public function mortalityBarangaysPerYear($year) {
        $barangays = Barangay::where("town_id", "=", 1) -> get();
        foreach ($barangays as $key => $barangay) {
            $totalCount = SCMembers::where("barangay_id","=",$barangay -> id)
                                -> whereRaw(sprintf("YEAR(`created_at`) = '%s'", $year)) 
                                -> count();
                                
            $aliveCount = SCMembers::where("barangay_id","=",$barangay -> id)
                                -> whereRaw(sprintf("YEAR(`created_at`) = '%s'", $year)) 
                                -> where("is_deceased", "=", 0)
                                -> count();
                                
            $deceasedCount = SCMembers::where("barangay_id","=",$barangay -> id)
                                -> whereRaw(sprintf("YEAR(`deceased_date`) = '%s'", $year))
                                -> where("is_deceased", "=", 1)
                                -> count();
                                
            $barangay -> total_count = $totalCount;
            $barangay -> alive_count = $aliveCount;
            $barangay -> deceased_count = $deceasedCount;
        }
        Response::setBody(json_encode($barangays));
    }
    
	public function mortalityRatePerMonth($barangay_id, $year) {
		$months = GenericHelper::monthList();
		foreach ($months as $key => $month) {
			$totalCount = SCMembers::where("barangay_id","=",$barangay_id)
								-> whereRaw(sprintf("MONTH(`created_at`) = '%s' AND YEAR(`created_at`) = '%s'", $month -> month, $year)) 
								-> count();
								
			$aliveCount = SCMembers::where("barangay_id","=",$barangay_id)
								-> whereRaw(sprintf("MONTH(`created_at`) = '%s' AND YEAR(`created_at`) = '%s'", $month -> month, $year)) 
								-> where("is_deceased", "=", 0)
								-> count();
								
			$deceasedCount = SCMembers::where("barangay_id","=",$barangay_id)
								-> whereRaw(sprintf("MONTH(`deceased_date`) = '%s' AND YEAR(`deceased_date`) = '%s'", $month -> month, $year))
								-> where("is_deceased", "=", 1)
								-> count();
			
			$month -> total_count = $totalCount;
			$month -> alive_count = $aliveCount;
			$month -> deceased_count = $deceasedCount;
		}
		Response::setBody(json_encode($months));
	}
	
	public function mortalityRatePerYear($barangay_id) {
		$years = $this -> getYears();
		foreach ($years as $key => $year) {
			$totalCount = SCMembers::where("barangay_id","=",$barangay_id)
								-> whereRaw(sprintf("YEAR(`created_at`) = '%s'", $year -> year)) 
								-> count();
								
			$aliveCount = SCMembers::where("barangay_id","=",$barangay_id)
								-> whereRaw(sprintf("YEAR(`created_at`) = '%s'", $year -> year)) 
								-> where("is_deceased", "=", 0)
								-> count();
								
			$deceasedCount = SCMembers::where("barangay_id","=",$barangay_id)
								-> whereRaw(sprintf("YEAR(`deceased_date`) = '%s'", $year -> year))
								-> where("is_deceased", "=", 1)
								-> count();
								
			$year -> total_count = $totalCount;
			$year -> alive_count = $aliveCount;
			$year -> deceased_count = $deceasedCount;
		}
		Response::setBody(json_encode($years));
	}
	
	private function getYears() {
		$arrYears = [];
	    $isYear_null = false;
		$years = SCMembers::select(new raw('count(id) as `data`'), 
								new raw('YEAR(deceased_date) year'))
							->groupby('year')
							->orderby('deceased_date','ASC')
							->get();
		foreach ($years as $key => $year) {
			if (!is_null($year -> year)) {
				array_push($arrYears, $year);
			}
		}
		// foreach ($years as $key => $year) {
		// 	if (is_null($year -> year)) {
		// 	    $isYear_null = true;
		// 	}
		// }
		// if ($isYear_null) {
		//     $years = SCMembers::select(new raw('count(id) as `data`'), 
  //                               new raw('YEAR(created_at) year'))
  //                           ->groupby('year')
  //                           ->orderby('updated_at','ASC')
  //                           ->get();
		// }
		return $arrYears;
	}
}
