var isEdit = false;
var edit_index = -1;
var transactions = [];
var booklet = $("#tbl-booklet").dataTable();

$("input[name=discount]").on('input', function() {
	setItemDiscount();
});

$("input[name=quantity]").on("input", function() {
	var srp = $("input[name=item_srp]").val();
	var total_amount = srp * this.value;
	$("input[name=total_amount]").val(total_amount);

	setItemDiscount();
});

function setItemDiscount() {
	var srp = $("input[name=item_srp]").val();
	var qty = $("input[name=quantity]").val();
	var item_type = $("input[name=item_type]").val();
	// var discount = $("input[name=discount]").val();
	var discount = 0;
	if (item_type == 1) {
		discount = 5;
	} else if(item_type == 2) {
		discount = 3;
	}
	$("input[name=discount]").val(discount);

	var temp_amount = qty * srp;
	var discount_amount = temp_amount * (discount / 100);
	var total_discounted_amount = temp_amount - discount_amount;
	$("input[name=discounted_amount]").val(total_discounted_amount);
}

function onItemSet() {
	if (!isEdit) {
		addItem();
	} else {
		updateItem(edit_index);
	}
}

function addItem() {
	var tempArr = [];
	tempArr.push(0);
	tempArr.push($("input[name=commodities]").val());
	tempArr.push($("input[name=item_name]").val());
	tempArr.push($("input[name=item_desc]").val());
	tempArr.push($("input[name=quantity]").val());
	tempArr.push($("input[name=total_amount]").val());
	tempArr.push($("input[name=discount]").val());
	tempArr.push($("input[name=discounted_amount]").val());
	tempArr.push("");
	transactions.push(tempArr);

	transactions.forEach(function(data, index) {
		// var action = "<center><button class='btn btn-success btn-xs' onclick='editItem(" + index + ")'>Edit</button>";
		var action = "<button class='btn btn-danger btn-xs' onclick='removeItem(" + index + ")'>Remove</button></center>";

		var ctr = index + 1;
		transactions[index][0] = ctr;
		transactions[index][8] = action;
	});

	renderDataOnTable();

	$("#add-item-modal").modal("hide");
	clearModal();
	setTransactionDetails();
}

function updateItem(index) {
	transactions[index][1] = $("input[name=commodities]").val();
	transactions[index][2] = $("input[name=item_name]").val();
	transactions[index][3] = $("input[name=item_desc]").val();
	transactions[index][4] = $("input[name=quantity]").val();
	transactions[index][5] = $("input[name=total_amount]").val();
	transactions[index][6] = $("input[name=discount]").val();
	transactions[index][7] = $("input[name=discounted_amount]").val();

	renderDataOnTable();

	$("#add-item-modal").modal("hide");
	clearModal();
	setTransactionDetails();

	isEdit = false;
	edit_index = -1;
}

function removeItem(index) {
	transactions.splice(index, 1);

	transactions.forEach(function(data, index) {
		var ctr = index + 1;
		transactions[index][0] = ctr;
	});

	renderDataOnTable();
	setTransactionDetails();
}

function editItem(index) {
	edit_index = index;
	isEdit = true;
	clearModal();

	var item = transactions[index];
	$("input[name=commodities]").val(item[1]);
	$("input[name=item_name]").val(item[2]);
	$("input[name=item_desc]").val(item[3]);
	$("input[name=quantity]").val(item[4]);
	$("input[name=total_amount]").val(item[5]);

	$("#add-item-modal").modal("show");
}

function renderDataOnTable() {
	booklet.fnClearTable();
	transactions.forEach(function(data, index) {
		booklet.fnAddData(data);
	});
}

function setTransactionDetails() {
	var totalAmount = 0;
	transactions.forEach(function(data, index) {
		totalAmount += parseFloat(data[5]);
	});
	$("input[name=total_sum]").val(totalAmount);
	$("input[name=total_sum_display]").val(totalAmount);

	setDiscountAmount();
}

function setDiscountAmount() {
	var totalAmount = $("input[name=total_sum]").val();
	var discount = $("input[name=discount]").val() / 100;
	var discount_amount = totalAmount * discount;
	var totalDiscountAmount = totalAmount - discount_amount;
	$("input[name=total_discount]").val(totalDiscountAmount);
	$("input[name=total_discount_display]").val(totalDiscountAmount);
}

function onSubmitTransaction() {
	var params = transactions;
	params.forEach(function(data, index) {
		params[index][8] = null;
	});

	var form_transaction = document.getElementById('form-transaction');

	var booklet_data = document.createElement('input');
	booklet_data.type = 'hidden';
	booklet_data.name = 'booklet_data';
	booklet_data.value = JSON.stringify(params);
	form_transaction.appendChild(booklet_data);

	form_transaction.submit();
}

function clearModal() {
	$("input[name=commodities]").val(null);
	$("input[name=item_name]").val(null);
	$("input[name=item_desc]").val(null);
	$("input[name=quantity]").val(1);
	$("input[name=total_amount]").val(1);
}

