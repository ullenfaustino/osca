<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>OSCA - Event</title>
		<meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no">
		<link rel="shortcut icon" href="favicon_16.ico"/>
		<link rel="bookmark" href="favicon_16.ico"/>
		<!-- site css -->
		<link rel="stylesheet" href="dist/css/site.min.css">
		<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,800,700,400italic,600italic,700italic,800italic,300italic" rel="stylesheet" type="text/css">
		<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
		<script type="text/javascript" src="dist/js/site.min.js"></script>
		<style>
			table, th, td {
			border: 2px solid black;
			border-collapse: collapse;
			border-spacing: 10px;
			}
			
			th,td{
			padding: 5px;

			}
		</style>
		<script src="res\jq\external/jquery/jquery.js"></script>
		<script src="res\jq\jquery-ui.js"></script>
		<link href="res\jq\jquery-ui.css" rel="stylesheet">
	</head>
	<body>
		<?php
			include 'OSCAFunctions.php';
			SESSION_START();
			if (!isset($_SESSION['userid'])){
				header("Location: login.php");
			}else{
				StartPage();
				$UserID=$_SESSION['userid'];
				$UserType=$_SESSION['usertype'];
				if ($_SESSION['usertype']=="Admin"){
					DrawAdminHeader();
				}else{
					DrawStaffHeader($_SESSION['corrbrgy']);
				}
				if (isset($_POST['evdate'])){
					NewEvent($_POST['evwhat'],$_POST['evwhere'],$_POST['evdate'],$_POST['budget'],$_POST['remarks']);
				}
				
				if (isset($_POST['updevid'])){
					UpdateEvent($_POST['updevid'],$_POST['updevwhat'],$_POST['updevwhere'],$_POST['updevdate'],$_POST['updbudget'],$_POST['updremarks']);
				}
			}
		?>
		<!--header-->
	
		<div class="container-fluid">
		<!--documents-->
			<div class="row row-offcanvas row-offcanvas-left">
				<?php 
					if ($_SESSION['usertype']=="Admin"){
						DrawAdminSidebar();
						echo '
							<table style="border: 0 solid black;">
								<tr style="border: 0 solid black;">
									<td style="border: 0 solid black;">
										<div>
											<form action="events.php" method="POST">
												<table style="width: 39%; border:thin white inset">
													<tr>
														<td colspan=2 style="border: 0 solid black;">
															<u>New Event</u>
														</td>
													</tr>
													<tr>
														<td colspan=2 style="border: 0 solid black;">
														<label>Date:</label><br>
															<input type="text" id="datepicker" value="" name="evdate" class="textInput" placeholder="YYYY-MM-DD" required>
														</td>
													</tr>
													<tr>
														<td style="border: 0 solid black;">
															<label>Event:</label><br>
															<input type="text"  value="" name="evwhat" class="textInput" placeholder="Event" required>
														</td>
														<td style="border: 0 solid black;">
															<label>Place:</label><br>
															<input type="text"  value="" name="evwhere" class="textInput" placeholder="Place" required>
														</td>
													</tr>
													<tr>
														<td colspan=2 style="border: 0 solid black;">
															<label>Budget:</label><br>
															<input type="number"  value="" name="budget" class="textInput" placeholder="Budget" required>
														</td>
													</tr>
													<tr>
														<td colspan=2 style="border: 0 solid black;">
															<label>Remarks:</label><br>
															<textarea rows="4" cols="50" name="remarks" placeholder="Put Remarks here..." required></textarea>
														</td>
													</tr>
													<tr>
														<td colspan=2 style="border: 0 solid black;">							
															<input type="Submit" value="Add Event">
														</td>
													</tr>
												</table>
											</form>
											<br /><br />
										</div>
									</td>';
									if (isset($_GET['evid'])){
										DrawEditEvent($_GET['evid']);
									}
								echo'
								</tr>
							</table>
						';
					}else{
						DrawStaffSidebar();
					}
				?>
				<div id="eventslist" style="width: 79%">
					<?php DrawEvents(); ?>
				</div>
			</div>
		</div>
		<script>
			$( "#datepicker" ).datepicker({
				inline: true,
				dateFormat: 'yy-mm-dd'
			});
			$( "#datepicker2" ).datepicker({
				inline: true,
				dateFormat: 'yy-mm-dd'
			});
		</script>
	</body>
<html>