<?php

namespace AdminSCEvents\Controllers;

use \User;
use \Users;
use \App;
use \View;
use \Menu;
use \Input;
use \Sentry;
use \Request;
use \Response;
use \Exception;
use \Admin\BaseController;
use \Cartalyst\Sentry\Users\UserNotFoundException;

use \Barangay;
use \BarangayUsers;
use \CodeGenerator;
use \Events;
use \Town;

class AdminSCEventsController extends BaseController {

	public function __construct() {
		parent::__construct();
		$this -> data['active_menu'] = 'admin_events';
	}

	public function index() {
		$user = Sentry::getUser();
		$this -> data['title'] = 'Events';
		
		$towns = Town::getAllTowns();
		$this -> data['towns'] = $towns;
		
		App::render('@adminscevents/index.twig', $this -> data);
	}
    
    public function getAllEvents() {
    	$user = Sentry::getUser();
		
		$events = Events::leftJoin("barangay as B", "B.id", "=", "events.barangay_id")
						-> select(array("*", "events.id as id", "events.created_at"))
						-> get();
						
	 	Response::setBody(json_encode($events));
    }
    
    public function setEvent() {
    	$codeGen = new CodeGenerator();
    	
    	$event_id = Input::post("event_id");
    	$barangay_id = Input::post("barangay");
    	$title = Input::post("title");
    	$content = Input::post("content");
    	$budget = Input::post("budget");
    	$event_color = Input::post("event_color");
    	
    	$notif_date = strtr(Input::post("notif_date"), '/', '-');
    	$notif_time = Input::post("notif_time");
    	$notif_datetime = date("Y-m-d H:i:s", strtotime(sprintf("%s %s", $notif_date, $notif_time)));
    	
    	$start_date = strtr(Input::post("start_date"), '/', '-');
    	$start_time = Input::post("start_time");
    	$start_datetime = date("Y-m-d H:i:s", strtotime(sprintf("%s %s", $start_date, $start_time)));
    	
    	$end_date = strtr(Input::post("end_date"), '/', '-');
    	$end_time = Input::post("end_time");
    	$end_datetime = date("Y-m-d H:i:s", strtotime(sprintf("%s %s", $end_date, $end_time)));
    	
    	if (strtotime(date("Y-m-d H:i:s")) > strtotime(sprintf("%s %s", $start_date, $start_time))) {
    		App::flash('message', "Unable to save this event, current date must not be prior to the date of event.");
        	App::flash('message_status', false);
        	Response::redirect($this -> siteUrl('admin/events'));
        	return;
    	}
    	
    	$event = Events::find($event_id);
    	if (!$event) {
    		$event = new Events();
    	}
    	
    	$hasConflict = Events::where("barangay_id", "=", $barangay_id)
							-> whereRaw(sprintf("(`event_start` <= '%s' and `event_end` >= '%s')", $end_datetime, $start_datetime))
							-> first();
		if ($hasConflict) {
			if ($hasConflict -> id != $event_id) {
				App::flash('message', "Conflict on Event Schedule.");
		        App::flash('message_status', false);
		        Response::redirect($this -> siteUrl('admin/events'));
		        return;
			}
		}
    	
    	$event -> ref_code = $codeGen -> getToken(8);
    	$event -> barangay_id = $barangay_id;
    	$event -> title = $title;
    	$event -> content = $content;
    	$event -> budget = $budget;
    	$event -> event_color = $event_color;
    	$event -> notif_date = $notif_datetime;
    	$event -> event_start = $start_datetime;
    	$event -> event_end = $end_datetime;
    	$event -> is_notified = 0;
    	$event -> save();
    	
    	App::flash('message', "Event successfully Saved.");
        App::flash('message_status', true);
        Response::redirect($this -> siteUrl('admin/events'));
    }
    
    public function deleteEvent() {
    	$event_id = Input::post("event_id_del");
    	Events::find($event_id) -> delete();
    	
    	App::flash('message', "Event successfully Deleted.");
        App::flash('message_status', true);
        Response::redirect($this -> siteUrl('admin/events'));
    }
    
}
