<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>OSCA - Update Senior</title>
		<meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no">
		<link rel="shortcut icon" href="favicon_16.ico"/>
		<link rel="bookmark" href="favicon_16.ico"/>
		<!-- site css -->
		<link rel="stylesheet" href="dist/css/site.min.css">
		<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,800,700,400italic,600italic,700italic,800italic,300italic" rel="stylesheet" type="text/css">
		<script src="https://code.jquery.com/jquery-1.12.4.js"></script>

		<script type="text/javascript" src="dist/js/site.min.js"></script>
		
		<script src="res\jq\external/jquery/jquery.js"></script>
		<script src="res\jq\jquery-ui.js"></script>
		<link href="res\jq\jquery-ui.css" rel="stylesheet">
	</head>
	<body>
		<?php
			include 'OSCAFunctions.php';
			SESSION_START();
			if (!isset($_SESSION['userid'])){
				header("Location: login.php");
			}else{
				StartPage();
				$UserID=$_SESSION['userid'];
				$UserType=$_SESSION['usertype'];
				if ($_SESSION['usertype']=="Admin"){
				}else{
					Logout();
				}
			}
			DrawAdminHeader();
			if (isset($_POST['FirstName'])) {
				UpdateSC($_POST['scid'], $_POST['FirstName'], $_POST['MiddleName'], 
				$_POST['LastName'], $_POST['Birthday'], $_POST['Gender'], 
				$_POST['address_value'], $_POST['SCStatus'], $_POST['ContactNumber']);
			}
		?>
		<!--header-->

		<div class="container-fluid">
		<!--documents-->
			<div class="row row-offcanvas row-offcanvas-left">
				<?php 
					DrawAdminSidebar();
					// include_once 'update_checker.php';
					echo '
						Update Senior Citizen Information
						<div id="frm">
							<form action="update_senior.php" method="GET">
								Search: <input type="text" name="id"/>
							</form>
						<div>
						<br /><br />
					';
					if (isset($_GET['id'])){
						$id = $_GET['id'];
						$con = mysqli_connect("localhost","root","","login");
						if (!$con) {
							die(mysql_error());
						}
						$results = $con->query("SELECT * FROM registration_barangay WHERE ID='$id'");
						$row=$results->fetch_array();
						if ($row['ID']==''){
							echo 'ID not existing';
						}else{
							echo '
								<div id="frm">
									<form action="update_senior.php" method="POST">
										<label>ID: '.  $row['ID']  .'</label> <br>
										<input type="hidden" value="'.  $row['ID']  .'" name="scid" class="textInput" placeholder="FirstName" required><br>
										<label>First Name</label> <br>
										<input type="text" value="'.  $row['Fname']  .'" name="FirstName" class="textInput" placeholder="FirstName" required><br>

										<label>Middle Name:</label><br>
										<input type="text"  value="'.  $row['Mname']  .'" name="MiddleName" class="textInput" placeholder="MiddleName" required><br>

										<label>Last Name:</label><br>
										<input type="text"  value="'.  $row['Lname']  .'" name="LastName" class="textInput" placeholder="LastName" required><br>

										<label>Birthday:</label><br>
										<input type="Birthday" id="datepicker"  value="'.  $row['Birthday']  .'" name="Birthday" class="Birthday" placeholder="YYYY-MM-DD" required><br>

										<label>Gender:</label><br>
										<input type="Gender" value="'.  $row['Gender']  .'" name="Gender" class="textInput" placeholder="Male/Female" required><br>

										<label for="Barangay"> Barangay: ('.$row['Address'].')</label><br>
										<select id="adress_option" name="address_value" >
										<option value="Unspecified">Select Barangay</option>
										<option value="Bagong Silang">Bagong Silang</option>
										<option value="Bgumbayan">Bgumbayan</option>
										<option value="Cabog-Cabog">Cabog-Cabog</option>
										<option value="Camacho">Camacho</option>
										<option value="Cataning">Cataning</option>
										<option value="Central">Central</option>
										<option value="Cupang North">Cupang North</option>
										<option value="Cupang West">Cupang West</option>
										<option value="Cupang Proper">Cupang Proper</option>
										<option value="Dangcol">Dangcol</option>
										<option value="Doña Francisca">Doña Francisca</option>
										<option value="Ibayo">Ibayo</option>
										<option value="Malabia">Malabia</option>
										<option value="Munting Batangas">Munting Batangas</option>
										<option value="Poblacion">Poblacion</option>
										<option value="Pto. Rivas Ibaba">Pto. Rivas Ibaba</option>
										<option value="Pto. Rivas Itaas">Pto. Rivas Itaas</option>
										<option value="Lote Pto. Rivas">Lote Pto. Rivas</option>
										<option value="San Jose">San Jose</option>
										<option value="Sibacan">Sibacan</option>
										<option value="Talisay">Talisay</option>
										<option value="Tanato">Tanato</option>
										<option value="Tenejero">Tenejero</option>
										<option value="Tortugas">Tortugas</option>
										<option value="Tuyo">Tuyo</option>
										</select><br>
										
										<label for="Status">Status: ('.$row['SCStatus'].')</label><br>
											<select id="SCStatus" name="SCStatus" >
											 <option value="Alive">Alive</option>
											 <option value="Deceased">Deceased</option>
										</select><br>
										
										<label>Contact Number:</label><br>
										<input type="number" value="'.  $row['Contactnumber']  .'" name="ContactNumber" placeholder="ContactNumber" required><br>

										<label>Submit</label>
										<input type="Submit" name="update_btn" value="Update">
									</form>
								</div>
							';
						}
					}
				?>
			</div>
		</div>
		<script>
			$( "#datepicker" ).datepicker({
				inline: true,
				dateFormat: 'yy-mm-dd'
			});
		</script>
	</body>
</html>