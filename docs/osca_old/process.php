<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>OSCA - Dashboard</title>
		<meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no">
		<link rel="shortcut icon" href="favicon_16.ico"/>
		<link rel="bookmark" href="favicon_16.ico"/>
		<!-- site css -->
		<link rel="stylesheet" href="dist/css/site.min.css">
		<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,800,700,400italic,600italic,700italic,800italic,300italic" rel="stylesheet" type="text/css">
		<!-- <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700' rel='stylesheet' type='text/css'> -->
		<!-- HTML5 shim, for IE6-8 support of HTML5 elements. All other JS at the end of file. -->
		<!--[if lt IE 9]>
		<script src="js/html5shiv.js"></script>
		<script src="js/respond.min.js"></script>
		<![endif]-->
		<script type="text/javascript" src="dist/js/site.min.js"></script>
		<style>
			table, th, td {
			border: 2px solid black;
			border-collapse: collapse;
			border-spacing: 10px;
			}
			th,td{
			padding: 5px;

			}
		</style>
	</head>
	<body>
		<?php
			include 'OSCAFunctions.php';
			SESSION_START();
			if (!isset($_SESSION['userid'])){
				header("Location: login.php");
			}else{
				StartPage();
				$UserID=$_SESSION['userid'];
				$UserType=$_SESSION['usertype'];
				if ($_SESSION['usertype']=="Admin"){
				}else{
					Logout();
				}
			}
			DrawAdminHeader();
		?>
		<div class="container-fluid">
			<!--documents-->
			<div class="row row-offcanvas row-offcanvas-left">
				<?php DrawAdminSidebar(); ?>
				<h3> Registered Señior Citizen</h3>
				<table>
					<thead>
						<tr>
							<td>
								FirstName
							</td>
							<td>
								MiddleName
							</td>
							<td>
								LastName
							</td>
							<td>
								Birthday
							</td>
							<td>
								Age
							</td>
							<td>
								Gender
							</td>
							<td>
								Barangay
							</td>
							<td>
								Status
							</td>
							<td>
								ContactNumber
							</td>
							<td colspan=2>
								Action
							</td>
						</tr>
					</thead>
					<tbody>
						<?php
						$results = mysql_query("SELECT * FROM registration_barangay");
						while ($row=mysql_fetch_array($results)){
						?>
						<tr>
						<td>
							<?php echo $row['Fname']?>
						</td>
						<td>
							<?php echo $row['Mname']?>
						</td>
						<td>
							<?php echo $row['Lname']?>
						</td>
						<td>
							<?php echo $row['Birthday']?>
						</td>
						<td>
							<?php
								echo date_diff(date_create($row['Birthday']), date_create('today'))->y;
							?>
						</td>
						<td>
							<?php echo $row['Gender']?>
						</td>
						<td>
							<?php echo $row['Address']?>
						</td>
						<td>
							<?php echo $row['SCStatus']?>
						</td>
						<td>
							<?php echo $row['Contactnumber']?>
						</td>
						<td>
							<a href="update_senior.php?id=<?php echo $row['ID'] ?>" class="btn btn-warning">EDIT</a>
						</td>
						<td>
							<a href="addcontributions.php?id=<?php echo $row['ID'] ?>" class="btn btn-warning">CONTRIBUTIONS</a>
						</td>
						</tr>
						<?php
						}
						?>
					</tbody>
				</table>
			</div>
		</div>
	</body>
</html>