<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>POS</title>
		<meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no">
		<link rel="shortcut icon" href="favicon_16.ico"/>
		<link rel="bookmark" href="favicon_16.ico"/>
		<!-- site css -->
		<link rel="stylesheet" href="dist/css/site.min.css">
		<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,800,700,400italic,600italic,700italic,800italic,300italic" rel="stylesheet" type="text/css">
		<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
		<script type="text/javascript" src="dist/js/site.min.js"></script>
		<style>
			table#t01 th td {
				border: 2px solid black;
				border-collapse: collapse;
				border-spacing: 10px;
				
			}
			table#t01 td{
				padding: 5px;
			}
		</style>
		<script src="res\jq\external/jquery/jquery.js"></script>
		<script src="res\jq\jquery-ui.js"></script>
		<link href="res\jq\jquery-ui.css" rel="stylesheet">
	</head>
	<body>
		<?php
			include '../OSCAFunctions.php';
			SESSION_START();
			if (isset($_POST['logout'])){
				session_destroy();
				header("Location: login.php");
			}
			if (!isset($_SESSION['posuserid'])){
				header("Location: login.php");
			}else{
				StartPage();
				$UserID=$_SESSION['posuserid'];
				$username=$_SESSION['username'];
			}
			
			if (isset($_POST['coinvid'])){
				Checkout($_POST['coinvid'],$_POST['coscid'],
				$_POST['cocommodity'],$_POST['coab'],$_POST['coad']);
			}
			
			$querygetmaxID=mysql_query("SELECT MAX(`invoiceid`) + 1 AS MAXID FROM `pos_trans_summary`");
			while ($row=mysql_fetch_array($querygetmaxID)){
				$maxid=$row['MAXID'];
			}
			
			if (isset($_GET['itemid'])){
				$itemid=$_GET['itemid'];
				$itemdesc=$_GET['itemdesc'];
				$itemprice=$_GET['price'];
			}else{
				$itemid="";
				$itemdesc="";
				$itemprice="";
			}
			
			if (isset($_POST['addtocart'])){
				AddtoCart($_POST['invid'],$_POST['itemid'],$_POST['qty']);
			}
		?>
		<div>
			POS <br />
			<?php
				echo 'User: ' . $username;
			?>
			<form action="index.php" method="POST">
				<input type="hidden" name="logout" />
				<input type="submit" value="Logout" />
			</form>
			<hr />
			<table style="width: 98%">
				<tr>
					<td valign="top" style="padding: 20px 50px 20px 50px; border: thin black inset; width: 35%">
						<!--
							Left side
							Cart Items
						-->
						<table>
							<tr>
								<td>
									<img src="img/cart.png" style="width: 50px" />
								</td>
								<td>
									Cart Items
								</td>
							</tr>
							<tr>
								<td colspan=2>	
									<hr />
								</td>
							</tr>
							<tr>
								<td colspan=2>
									<table>
										<tr>
											<td>
												Item ID
											</td>
											<td>
												Description
											</td>
											<td>
												Unit Price
											</td>
											<td>
												Quantity
											</td>
											<td>
												Amount Due
											</td>
										</tr>
										<?php 
											ShowCart($maxid);
											echo '
												<tr>
													<td colspan=5>
														<br />
													</td>
												</tr>
												<tr>
													<td colspan=4>
														TOTAL (Not Discounted)
													</td>
													<td>
														'.CalcTotal($maxid).'
													</td>
												</tr>
											';
										?>
										<tr>
											<td colspan=5>
												<br />
											</td>
										</tr>
										<tr>
											<form action="index.php" method="POST">
												<td colspan=2>
													Senior Citizen ID:
												</td>
												<td colspan=2>
													<input type="text" name="scid" value="" placeholder="Swipe Senior Citizen ID Tag" required/>
												</td>
											</form>
										</tr>
										<tr>
											<td colspan=2>
												Discounted Price <br />(-%5)
											</td>
											<td colspan=2>
												<?php  
													$scid='';
													$amtdue='';
													if (isset($_POST['scid'])){
														$amtdue=CalcTotalwdisc($maxid,$_POST['scid']);
														echo $amtdue;
														$scid=$_POST['scid'];
													}else{
														$amtdue=CalcTotal($maxid);
														echo $amtdue;
													}
												?>
											</td>
										</tr>
										<tr>
											<td colspan=3>
												<br />
												<form action="index.php" method="POST">
													<input type="hidden" name="coinvid" value="<?php echo $maxid; ?>">
													<input type="hidden" name="coscid" value="<?php echo $scid; ?>">
													<input type="hidden" name="coab" value="<?php echo $username; ?>">
													<input type="hidden" name="coad" value="<?php echo $amtdue; ?>">
													<input type="hidden" name="cocommodity" value="Assorted">
													<input type="submit" name="checkout" value="Check out">
												</form>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
					<td valign="top" style="padding: 20px 50px 20px 50px; border: thin black inset; width: 30%">
						<!--
							Middle
							Add Items to cart
						-->
						<form action="index.php" method="POST">
							<input type="hidden" name="addtocart" />
							<table>
								<tr>
									<td>
										Invoice ID:
									</td>
									<td>
										<?php echo $maxid; ?>
									</td>
								</tr>
								<tr>
									<td colspan=2>
										<br /><br />
									</td>
								</tr>
								<tr>
									<td>
										Item ID:
									</td>
									<td>
										<?php
											echo $itemid;
										?>
									</td>
								</tr>
								<tr>
									<td>
										Description:
									</td>
									<td>
										<?php
											echo $itemdesc;
										?>
									</td>
								</tr>
								<tr>
									<td>
										Price:
									</td>
									<td>
										<?php
											echo $itemprice;
										?>
									</td>
								</tr>
								<tr>
									<td>
										<br />
									</td>
								</tr>
								<tr>
									<td>
										Quantity:
									</td>
									<td>
										<input type="number" name="qty" required>
									</td>
								</tr>
								<tr>
									<td>
										<input type="submit" name="addtocart" value="Add to Cart">
									</td>
								</tr>
							</table>
							<?php
								echo '
									<input type="hidden" value="'.$maxid.'" name="invid">
									<input type="hidden" value="'.$itemid.'" name="itemid">
								';
							?>
						</form>
					</td>
					<td style="padding: 20px 50px 20px 50px; border: thin black inset; width: 35%">
						<!--
							Right side
							List of Items
						-->
						<table>
							<tr>
								<td>
									Item List
								</td>
							</tr>
							<tr>
								<td>
									<table border=1>
									<tr>
										<th style="text-align: left">
											Item ID
										</th>
										<th style="text-align: left">
											Item Description
										</th>
										<th style="text-align: left">
											Price
										</th>
										<th style="text-align: left">
											Action
										</th>
									</tr>
									<?php
										$querygetmaxID=mysql_query("SELECT * FROM `pos_items`");
										while ($rowitemlist=mysql_fetch_array($querygetmaxID)){
											echo '
												<form action="index.php" method="GET">
												<input type="hidden" value="'.$rowitemlist['itemid'].'" name="itemid">
												<input type="hidden" value="'.$rowitemlist['itemdesc'].'" name="itemdesc">
												<input type="hidden" value="'.$rowitemlist['itemprice'].'" name="price">
												<tr>
													<td>
														'.$rowitemlist['itemid'].'
													</td>
													<td>
														'.$rowitemlist['itemdesc'].'
													</td>
													<td>
														'.$rowitemlist['itemprice'].'
													</td>
													<td>
														<input type="submit" value="Select">
													</td>
												</tr>
												</form>
											';
										}
									?>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</div>
	</body>
<html>