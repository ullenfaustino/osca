<?php

namespace AdminRegistration\Controllers;

use \User;
use \Users;
use \App;
use \View;
use \Menu;
use \Input;
use \Sentry;
use \Request;
use \Response;
use \Exception;
use \Admin\BaseController;
use \Cartalyst\Sentry\Users\UserNotFoundException;

use \Town;
use \Barangay;
use \BarangayUsers;
use \Establishment;
use \EstablishmentUsers;
use \SCMembers;
use \CodeGenerator;
use \GenericHelper;

class AdminRegistrationController extends BaseController {

	public function __construct() {
		parent::__construct();
		$this -> data['active_menu'] = 'admin_registration';
	}

	// public function index() {
	// $user = Sentry::getUser();
	// $this -> data['title'] = 'Registration';
	// App::render('@adminregistration/index.twig', $this -> data);
	// }
	
	public function staffRegistrationView() {
		$user = Sentry::getUser();
		$this -> data['title'] = 'Staff Registration';
		$this -> data['sub_active_menu'] = 'admin_registration_staff';
		
		$towns = Town::getAllTowns();
		$this -> data['towns'] = $towns;
		
		$staffs = Users::getAllStaffs();
		$this -> data['staffs'] = $staffs;
		
		App::render('@adminregistration/staff_registration.twig', $this -> data);
	}
	
	public function establishmentRegistrationView() {
		$user = Sentry::getUser();
		$this -> data['title'] = 'Establishment Registration';
		$this -> data['sub_active_menu'] = 'admin_registration_establishment';
		
		$this -> data['establishments'] = Establishment::all();
		
		$establishments = Users::getAllEstablishments();
        $this -> data['establishment_users'] = $establishments;

		App::render('@adminregistration/establishment_registration.twig', $this -> data);
	}
	
	public function scRegistrationView() {
		$user = Sentry::getUser();
		$this -> data['title'] = 'Senior Citizen Registration';
		$this -> data['sub_active_menu'] = 'admin_registration_sc';
		
		$towns = Town::getAllTowns();
		$this -> data['towns'] = $towns;
		
		$members = SCMembers::leftJoin("barangay as B","B.id","=","sc_members.barangay_id")
							-> select(array("*", "sc_members.id as id","sc_members.created_at",
											"B.name as barangay_name"))
							-> get();
		$this -> data['members'] = $members;
		
		App::render('@adminregistration/sc_registration.twig', $this -> data);
	}
	
	public function addStaff() {
		try {
			$codeGen = new CodeGenerator();
			$full_name = Input::post("full_name");
			$email = Input::post("email");
			$barangay = Input::post("barangay");
			$user_id = Input::post("user_id");
			
			if (strlen($user_id) == 0) {
    			$brgy = Barangay::find($barangay);
    			$username = sprintf("%s_%s", strtolower(str_replace(" ","_",$brgy->name)), $codeGen->getToken(5));
    			$password = $codeGen -> getToken(5);
    		
    	        $credential =  ['ref_code' => $codeGen -> getReferenceCode(), 
    	                        'username' => trim($username), 
    	                        'email' => trim($email), 
    	                        'full_name' => trim($full_name), 
    	                        'password' => $password, 
    	                        'canonical_hash' => base64_encode($password),
    	                        'activated' => 1, 
    	                        'permissions' => array('staff' => 1),
    	                        'user_type' => 2
    	                       ];
    	        
    	        $user = Sentry::createUser($credential);
    	        $user -> addGroup(Sentry::getGroupProvider() -> findByName('Staff'));
    	        
    	        $brgy_users = new BarangayUsers();
    	        $brgy_users -> user_id = $user -> id;
    	        $brgy_users -> barangay_id = $barangay;
    	        $brgy_users -> is_admin = 1;
    	        $brgy_users -> save();
    	        
    	        App::flash('message', "Registration Completed.");
                App::flash('message_status', true);
	        } else {
                $user = Users::find($user_id);
                $user -> full_name = $full_name;
                $user -> email = $email;
                $user -> save();
                
                $brgy_user = BarangayUsers::where("user_id","=",$user -> id) -> first();
                if (!$brgy_user) {
                    $brgy_user = new BarangayUsers();
                    $brgy_user -> user_id = $user -> id;
                    $brgy_user -> is_admin = 1;
                }
                $brgy_user -> barangay_id = $barangay;
                $brgy_user -> save();
                
                App::flash('message', "User successfully Updated.");
                App::flash('message_status', true);
            }
        } catch ( Cartalyst\Sentry\Users\UserNotFoundException $e ) {
			App::flash('message_status', false);
			App::flash('message', "Sponsor must exist! Please enter a valid sponsor username.");
		} catch ( \Exception $e ) {
			App::flash('message_status', false);
			App::flash('message', "[Exception:]" . $e -> getMessage());
		}
		Response::redirect($this -> siteUrl('admin/registration/staff/view'));
	}
	
	public function addEstablishment() {
        try {
            $codeGen = new CodeGenerator();
            $full_name = Input::post("full_name");
            $email = Input::post("email");
            $establishment = Input::post("establishment");
            $user_id = Input::post("user_id");
            
            if (strlen($user_id) == 0) {
                $username = $codeGen->getUsername();
                $password = $codeGen -> getToken(5);
            
                $credential =  ['ref_code' => $codeGen -> getReferenceCode(), 
                                'username' => trim($username), 
                                'email' => trim($email), 
                                'full_name' => trim($full_name), 
                                'password' => $password, 
                                'canonical_hash' => base64_encode($password),
                                'activated' => 1, 
                                'permissions' => array('establishment' => 1),
                                'user_type' => 3
                               ];
                
                $user = Sentry::createUser($credential);
                $user -> addGroup(Sentry::getGroupProvider() -> findByName('Establishment'));
                
                $est_users = new EstablishmentUsers();
                $est_users -> user_id = $user -> id;
                $est_users -> establishment_id = $establishment;
                $est_users -> save();
                
                App::flash('message', "Registration Completed.");
                App::flash('message_status', true);
            } else {
                $user = Users::find($user_id);
                $user -> full_name = $full_name;
                $user -> email = $email;
                $user -> save();
                
                $est_users = EstablishmentUsers::where("user_id","=",$user -> id) -> first();
                if (!$est_users) {
                    $est_users = new EstablishmentUsers();
                    $est_users -> user_id = $user -> id;
                }
                $est_users -> establishment_id = $establishment;
                $est_users -> save();
                
                App::flash('message', "User successfully Updated.");
                App::flash('message_status', true);
            }
        } catch ( Cartalyst\Sentry\Users\UserNotFoundException $e ) {
            App::flash('message_status', false);
            App::flash('message', "Sponsor must exist! Please enter a valid sponsor username.");
        } catch ( \Exception $e ) {
            App::flash('message_status', false);
            App::flash('message', "[Exception:]" . $e -> getMessage());
        }
        Response::redirect($this -> siteUrl('admin/registration/establishment/view'));
    }
    
    public function addSCMember() {
        $codeGen = new CodeGenerator();
        
        $barangay_id = Input::post("barangay");
        $sc_id = Input::post("sc_id");
        $rf_id = Input::post("rf_id");
        $first_name =Input::post("first_name");
        $middle_name = Input::post("middle_name");
        $last_name = Input::post("last_name");
        $gender = Input::post("gender");
        $birthday = Input::post("birthday");
        $birthday = strtr($birthday, '/', '-');
        $address = Input::post("address");
        $mobile =Input::post("mobile");
        // $landline = Input::post("landline");
        $is_deceased = Input::post("is_deceased");
        $date_deceased = Input::post("date_deceased");
        $date_deceased = strtr($date_deceased, '/', '-');
        
        $d1 = new \DateTime($birthday);
        $d2 = new \DateTime("now");
        $age = $d2 -> diff($d1);
        
        if ($age->y < 60) {
            App::flash('message', "Error saving check birth year Saved.");
            App::flash('message_status', false);
            Response::redirect($this -> siteUrl('admin/registration/sc/view'));
            return;
        }
        
        $sc = SCMembers::find($sc_id);
        if (!$sc) {
            $sc = new SCMembers();
        }
        $sc -> rf_id = $rf_id;
        $sc -> ref_code = $codeGen -> getToken();
        $sc -> barangay_id = $barangay_id;
        $sc -> first_name = $first_name;
        $sc -> middle_name = $middle_name;
        $sc -> last_name = $last_name;
        $sc -> gender = $gender;
        $sc -> birthday = date("Y-m-d", strtotime($birthday));
        $sc -> address = $address;
        $sc -> mobile = $mobile;
        // $sc -> landline = $landline;
        $sc -> is_deceased = ($is_deceased === "on") ? 1 : 0;
        $sc -> deceased_date = ($is_deceased === "on") ? $date_deceased : null;
        $sc -> save();
        
        App::flash('message', "successfully Saved.");
        App::flash('message_status', true);
        Response::redirect($this -> siteUrl('admin/registration/sc/view'));
    }
	
	public function sendCredentialsToEmail() {
	    $user_id = Input::post("staff_id");
	    $user = Users::find($user_id);
	    
	    $body = "<h1>Welcome to OSCA!</h1>";
        $body .= "<p><strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>";
        $body .= "<p>&nbsp;</p>";
        $body .= "<p>Website Link: <a href='" . GenericHelper::baseUrl() . "'>" . GenericHelper::baseUrl() . "</a></p>";
        $body .= "<p>&nbsp;</p>";
        $body .= "<p style='text-align: left;'>Your Username is <span style='color: #000080; background-color: #ffff00;'><strong>" . $user -> username . "</strong></span></p>";
        $body .= "<p style='text-align: left;'>Your Password is&nbsp;<span style='color: #ff0000; background-color: #ffff00;'><strong>" . base64_decode($user -> canonical_hash) . "</strong></span></p>";
	    
	    $subject = "[OSCA] Account Credentials";
	    GenericHelper::sendMail($user -> email, $subject, $body);
	    
	    App::flash('message', "Credentials Successfully Sent.");
        App::flash('message_status', true);
        Response::redirect($this -> siteUrl('admin/registration/staff/view'));
	}
	
	public function sendCredentialsToEmailEstablishment() {
        $user_id = Input::post("establishment_admin_id");
        $user = Users::find($user_id);
        
        $body = "<h1>Welcome to OSCA!</h1>";
        $body .= "<p><strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>";
        $body .= "<p>&nbsp;</p>";
        $body .= "<p>Website Link: <a href='" . GenericHelper::baseUrl() . "'>" . GenericHelper::baseUrl() . "</a></p>";
        $body .= "<p>&nbsp;</p>";
        $body .= "<p style='text-align: left;'>Your Username is <span style='color: #000080; background-color: #ffff00;'><strong>" . $user -> username . "</strong></span></p>";
        $body .= "<p style='text-align: left;'>Your Password is&nbsp;<span style='color: #ff0000; background-color: #ffff00;'><strong>" . base64_decode($user -> canonical_hash) . "</strong></span></p>";
        
        $subject = "[OSCA] Account Credentials";
        GenericHelper::sendMail($user -> email, $subject, $body);
        
        App::flash('message', "Credentials Successfully Sent.");
        App::flash('message_status', true);
        Response::redirect($this -> siteUrl('admin/registration/staff/view'));
    }
}
