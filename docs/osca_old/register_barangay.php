<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Barangay Bagong Silang</title>
    <meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no">
    <link rel="shortcut icon" href="favicon_16.ico"/>
    <link rel="bookmark" href="favicon_16.ico"/>
    <!-- site css -->
    <link rel="stylesheet" href="dist/css/site.min.css">
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,800,700,400italic,600italic,700italic,800italic,300italic" rel="stylesheet" type="text/css">
    <!-- <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700' rel='stylesheet' type='text/css'> -->
    <!-- HTML5 shim, for IE6-8 support of HTML5 elements. All other JS at the end of file. -->
    <!--[if lt IE 9]>
      <script src="js/html5shiv.js"></script>
      <script src="js/respond.min.js"></script>
    <![endif]-->
    <script type="text/javascript" src="dist/js/site.min.js"></script>
  </head>
  <body>

    <!--nav-->
    <nav role="navigation" class="navbar navbar-custom">
        <div class="container-fluid">
          <!-- Brand and toggle get grouped for better mobile display -->
          <div class="navbar-header">
            <button data-target="#bs-content-row-navbar-collapse-5" data-toggle="collapse" class="navbar-toggle" type="button">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a href="#" class="navbar-brand">Barangay Bagong Silang</a>
          </div>

          <!-- Collect the nav links, forms, and other content for toggling -->
          <div id="bs-content-row-navbar-collapse-5" class="collapse navbar-collapse">
            <ul class="nav navbar-nav navbar-right">
              
              <li class="dropdown">
                <a data-toggle="dropdown" class="dropdown-toggle" href="#"><i class="fa fa-user fa-fw"></i>Barangay Official <b class="caret"></b></a>
                <ul role="menu" class="dropdown-menu">
                  <li class="disabled"><a href="logout.php"><i class="fa fa-sign-out fa-fw"></i>LOGOUT</a></li>
                </ul>
              </li>
            </ul>

          </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
      </nav>
           
             
    <!--header-->

    <div class="container-fluid">
    <!--documents-->
        <div class="row row-offcanvas row-offcanvas-left">
          <div class="col-xs-6 col-sm-3 sidebar-offcanvas" role="navigation">
            <ul class="list-group panel">
                <li class="list-group-item"><a href="#"><i class="glyphicon glyphicon-home"></i>Dashboard </a></li>
                <li class="list-group-item"><a href="register_senior.php"><i class="fa fa-user fa-fw"></i>Register Señior Citizen </a></li>

                <li class="list-group-item"><a href="update_senior.php"><i class="glyphicon glyphicon-floppy-open"></i>Update Records</a></li>

                <li class="list-group-item"><a href="sms.php"><i class="glyphicon glyphicon-envelope"></i>SMS</a></li>
                
                </div>

        <div id="frm">
         <form action="register_barangay_member.php" method="POST">
           <label>First Name</label> <br>
              <input type="text" name="FirstName" class="textInput" placeholder="FirstName" required><br>
             
           <label>Middle Name:</label><br>
            <input type="text" name="MiddleName" class="textInput" placeholder="MiddleName" required><br>
              
             <label>Last Name:</label><br>
             <input type="text" name="LastName" class="textInput" placeholder="LastName" required><br>

         

                <label for="Barangay"> Barangay:  </label><br>
                <select id="adress_option" name="address_value" >
                 <option value="0">Select Barangay</option>
                 <option value="Bagong Silang">Bagong Silang</option>
                 <option value="Bgumbayan">Bgumbayan</option>
                 <option value="Cabog-Cabog">Cabog-Cabog</option>
                 <option value="Camacho">Camacho</option>
                 <option value="Cataning">Cataning</option>
                 <option value="Central">Central</option>
                 <option value="Cupang North">Cupang North</option>
                 <option value="Cupang West">Cupang West</option>
                 <option value="Cupang Proper">Cupang Proper</option>
                 <option value="Dangcol">Dangcol</option>
                 <option value="Doña Francisca">Doña Francisca</option>
                 <option value="Ibayo">Ibayo</option>
                 <option value="Malabia">Malabia</option>
                 <option value="Munting Batangas">Munting Batangas</option>
                 <option value="Poblacion">Poblacion</option>
                 <option value="Pto. Rivas Ibaba">Pto. Rivas Ibaba</option>
                 <option value="Pto. Rivas Itaas">Pto. Rivas Itaas</option>
                 <option value="Lote Pto. Rivas">Lote Pto. Rivas</option>
                 <option value="San Jose">San Jose</option>
                 <option value="Sibacan">Sibacan</option>
                 <option value="Talisay">Talisay</option>
                 <option value="Tanato">Tanato</option>
                 <option value="Tenejero">Tenejero</option>
                 <option value="Tortugas">Tortugas</option>
                 <option value="Tuyo">Tuyo</option>

                </select><br>
            <label>Contact Number:</label><br>
             <input type="number" name="ContactNumber"  placeholder="ContactNumber" required><br>
              
              <label>Username:</label><br>
             <input type="textInput" name="username"  placeholder="Username" required><br>

              <label>Password:</label><br>
             <input type="Password" name="password"  placeholder="Password" required><br>


            

             <label>Submit</label>
              <input type="Submit" name="register_btn" value="Register">

               
          
            </form>