<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Barangay Bagong Silang</title>
    <meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no">
    <link rel="shortcut icon" href="favicon_16.ico"/>
    <link rel="bookmark" href="favicon_16.ico"/>
    <!-- site css -->
    <link rel="stylesheet" href="dist/css/site.min.css">
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,800,700,400italic,600italic,700italic,800italic,300italic" rel="stylesheet" type="text/css">
    <!-- <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700' rel='stylesheet' type='text/css'> -->
    <!-- HTML5 shim, for IE6-8 support of HTML5 elements. All other JS at the end of file. -->
    <!--[if lt IE 9]>
      <script src="js/html5shiv.js"></script>
      <script src="js/respond.min.js"></script>
    <![endif]-->
    <script type="text/javascript" src="dist/js/site.min.js"></script>
  </head>
  <body>

    <!--nav-->
    <nav role="navigation" class="navbar navbar-custom">
        <div class="container-fluid">
          <!-- Brand and toggle get grouped for better mobile display -->
          <div class="navbar-header">
            <button data-target="#bs-content-row-navbar-collapse-5" data-toggle="collapse" class="navbar-toggle" type="button">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a href="#" class="navbar-brand">Barangay Bagong Silang</a>
          </div>

          <!-- Collect the nav links, forms, and other content for toggling -->
          <div id="bs-content-row-navbar-collapse-5" class="collapse navbar-collapse">
            <ul class="nav navbar-nav navbar-right">
              
              <li class="dropdown">
                <a data-toggle="dropdown" class="dropdown-toggle" href="#"><i class="fa fa-user fa-fw"></i>Barangay Official <b class="caret"></b></a>
                <ul role="menu" class="dropdown-menu">
                  <li class="disabled"><a href="logout.php"><i class="fa fa-sign-out fa-fw"></i>LOGOUT</a></li>
                </ul>
              </li>
            </ul>

          </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
      </nav>
           
             
              <?php
 $servername ="localhost";
$serverusername ="root";
$serverpassword ="";
$dbname = "login";

$conn = mysqli_connect($servername, $serverusername, $serverpassword, $dbname);
if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
}


  if (isset($_POST['register_btn'])) {
    session_start();

    $FirstName = mysql_real_escape_string($_POST['FirstName']);
    $MiddleName = mysql_real_escape_string($_POST['MiddleName']);
    $LastName = mysql_real_escape_string($_POST['LastName']);
    $Barangay = mysql_real_escape_string($_POST['address_value']);
    $ContactNumber = mysql_real_escape_string($_POST['ContactNumber']);
    $username = mysql_real_escape_string($_POST['username']);
    $password = mysql_real_escape_string($_POST['password']);

   $sql = "INSERT INTO barangay_employee(fname, mname, lname, barangay, contactnumber, username, password) VALUES('$FirstName', '$MiddleName', '$LastName', '$Birthday', '$Age', '$Gender', '$Barangay', '$ContactNumber','username','password')";
    
        if (mysqli_query($conn, $sql)) {
    echo "New record created successfully";
    header('Location: register_senior.php');
} 
else {
    echo "Error: " . $sql . "<br>" . mysqli_error($conn);
}
    
mysqli_close($conn);}
?>
    <!--header-->

    <div class="container-fluid">
    <!--documents-->
        <div class="row row-offcanvas row-offcanvas-left">
          <div class="col-xs-6 col-sm-3 sidebar-offcanvas" role="navigation">
            <ul class="list-group panel">
                <li class="list-group-item"><a href="#"><i class="glyphicon glyphicon-home"></i>Dashboard </a></li>
                <li class="list-group-item"><a href="register_senior.php"><i class="fa fa-user fa-fw"></i>Register Señior Citizen </a></li>

                <li class="list-group-item"><a href="update_senior.php"><i class="glyphicon glyphicon-floppy-open"></i>Update Records</a></li>

                <li class="list-group-item"><a href="sms.php"><i class="glyphicon glyphicon-envelope"></i>SMS</a></li>
                