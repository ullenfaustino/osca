<?php

namespace Transaction\Controllers;

use \User;
use \Users;
use \App;
use \View;
use \Menu;
use \Input;
use \Sentry;
use \Request;
use \Response;
use \Exception;
use \Establishment\BaseController;
use \Cartalyst\Sentry\Users\UserNotFoundException;

use \Town;
use \Barangay;
use \SCMembers;
use \CodeGenerator;
use \Booklet;
use \BookletTransaction;
use \EstablishmentUsers;
use \EstablishmentProducts;

class TransactionController extends BaseController {

	public function __construct() {
		parent::__construct();
		$this -> data['active_menu'] = 'establishment_transaction';
	}

	/**
	 * display the admin dashboard
	 */
	public function index() {
		$user = Sentry::getUser();
		$this -> data['title'] = 'Transaction';

		$towns = Town::getAllTowns();
        $this -> data['towns'] = $towns;
        
		App::render('@transaction/index.twig', $this -> data);
	}
	
	public function transactionListView($sc_id) {
	    $user = Sentry::getUser();
        $this -> data['title'] = 'Transaction List';
        $this -> data['sc_id'] = $sc_id;
        $transactions = Users::Join("establishment_users as EU","EU.user_id","=","users.id")
                            -> Join("establishment as E","E.id","=","EU.establishment_id")
                            -> Join("booklet_transaction as BT", "BT.establishment_id","=","E.id")
                            -> Join("sc_members as SC","SC.id","=","BT.sc_member_id")
                            -> select(array("*",
                                            "BT.created_at as created_at"))
                            -> where("users.id","=",$user -> id)
                            -> where("SC.id","=",$sc_id)
                            -> get();
        $this -> data['transactions'] = $transactions;

        App::render('@transaction/transaction_list.twig', $this -> data);
	}
	
	public function transactionsView($trans_code, $sc_id) {
        $user = Sentry::getUser();
        $this -> data['title'] = 'Transaction: <b>' . $trans_code . '</b>';
        $this -> data['sc_id'] = $sc_id;
        $transactions = Booklet::leftJoin("booklet_transaction as BT","BT.id","=","booklet.booklet_transaction_id")
                            -> where("BT.trans_code","=",$trans_code)
                            -> select("*", "booklet.discount","booklet.discounted_amount")
                            -> get();
        $this -> data['transactions'] = $transactions;
        
        App::render('@transaction/view_transaction.twig', $this -> data);
	}
	
	public function memberTransactionView($sc_id) {
	    $codeGen = new CodeGenerator();
	    $user = Sentry::getUser();
	    
	    $ref_code = $codeGen -> getCodes();
	    $this -> data['title'] = 'Transaction: <b>' . $ref_code . '</b>';
	    $this -> data['sc_id'] = $sc_id;
	    $this -> data['trans_code'] = $ref_code;
	    
	    $member = SCMembers::find($sc_id);
	    $this -> data['member'] = $member;
	    
	    $est_user = EstablishmentUsers::where("user_id", "=", $user -> id) -> first();
		$establishment_id = $est_user -> establishment_id;
		$this -> data["products"] = EstablishmentProducts::where("establishment_id", "=", $establishment_id) -> get();
	    
	    App::render('@transaction/transaction.twig', $this -> data);
	}
	
	public function saveTransaction() {
	    $user = Sentry::getUser();
	    $est_user = EstablishmentUsers::where("user_id", "=", $user->id) -> first();
	    $establishment_id = $est_user -> establishment_id;
	    
	    $sc_id = Input::post("sc_id");
	    $trans_code = Input::post("trans_code");
	    $discount = Input::post("discount");
	    $total_sum = Input::post("total_sum");
	    $total_discount = Input::post("total_discount");
	    $booklet_data = Input::post("booklet_data");
	    $booklet_data = json_decode($booklet_data, TRUE);
	    
	    $transaction = new BookletTransaction();
	    $transaction -> trans_code = $trans_code;
	    $transaction -> sc_member_id = $sc_id;
	    $transaction -> establishment_id = $establishment_id;
	    $transaction -> total_amount = $total_sum;
        // $transaction -> discount = $discount;
        // $transaction -> discounted_amount = $total_discount;
        $transaction -> save();
	    
	    for ($i=0; $i < count($booklet_data); $i++) {
	        $commodities = $booklet_data[$i][1];
	        $item_name = $booklet_data[$i][2];
	        $item_desc = $booklet_data[$i][3];
	        $quantity = $booklet_data[$i][4];
	        $total_amount = $booklet_data[$i][5];
	        $discount = $booklet_data[$i][6];
	        $total_discounted_amount = $booklet_data[$i][7];
	        
	        $booklet = new Booklet();
	        $booklet -> booklet_transaction_id = $transaction -> id;
	        $booklet -> commodities = $commodities;
	        $booklet -> item_name = $item_name;
	        $booklet -> item_description = $item_desc;
	        $booklet -> quantity = $quantity;
	        $booklet -> total_amount = $total_amount;
	        $booklet -> discount = $discount;
	        $booklet -> discounted_amount = $total_discounted_amount;  
	        $booklet -> save();
		}
	    
	    App::flash('message', "Transaction Completed!");
        App::flash('message_status', true);
        Response::redirect($this -> siteUrl('establishment/transaction/list/sc/' . $sc_id));
	}
    
    public function transmembersList() {
        $barangay_id = Input::post("barangay_id");
        $limit = Input::post("length");
        $start = Input::post("start");
        $searchValue = trim(Input::post("search")['value'], '"');
        $order = Input::post("order")[0]['column'];
        $dir = Input::post("order")[0]['dir'];
        
        $members = SCMembers::leftJoin ( 'barangay as B', 'B.id', '=', 'sc_members.barangay_id' )
                            ->select ( array ('*',
                                            'sc_members.id as id',
                                            'B.name as barangay_name',
                                            'sc_members.created_at as created_at'))
                            -> where("sc_members.barangay_id","=",$barangay_id)
                            -> where("sc_members.is_deceased","=",0);
        $tmpMembers = $members;
        $totalCount = $tmpMembers->groupBy("sc_members.id")->get()->count();
        if (strlen ( $searchValue ) > 0) {
            $members = $members -> whereRaw("(sc_members.first_name like '%$searchValue%' or
                                                     sc_members.rf_id like '%$searchValue%' or 
                                                     sc_members.middle_name like '%$searchValue%' or
                                                     sc_members.last_name like '%$searchValue%' or
                                                     sc_members.created_at like '%$searchValue%' or
                                                     B.name like '%$searchValue%')");
        }
        
        $tmpMemberFiltered = $members;
        $filteredCount = $tmpMemberFiltered->groupBy("sc_members.id")->get()->count();
        $members = $members->offset($start)->take($limit);
        $members = $members->get ();
        $arrMembers = array ();
        foreach ( $members as $key=>$member ) {
            // $tmp [] = sprintf("%s",$member->id);
            // $tmp [] = sprintf("<a href='%s'>%s</a>",$member->rf_id, $member->rf_id);
            // $tmp [] = sprintf("%s",$member->first_name);
            // $tmp [] = sprintf("%s",$member->middle_name);
            // $tmp [] = sprintf("%s",$member->last_name);
            // $tmp [] = sprintf("%s",$member->gender);
            // $tmp [] = sprintf("%s",$bday->format('M d, Y'));
            // $tmp [] = sprintf("%s",$member->address);
            // $tmp [] = sprintf("%s",$member->barangay_name);
            // $c_at = new \DateTime($member->created_at);
            // $tmp [] = sprintf("%s",$c_at->format('M d, Y h:i:s a'));
            
            $bday = new \DateTime($member->birthday);
            
            $html[] = '<h2><span style="text-decoration: underline;"><span style="color: #0000ff; text-decoration: underline;"><a href="/establishment/transaction/list/sc/'.$member->id.'">'.sprintf("%s %s %s", $member -> first_name, $member -> middle_name, $member -> last_name).'</a></span></span></h2>
                    <p><span style="text-decoration: underline;">RF ID:</span> <em><strong><a href="/establishment/transaction/list/sc/'.$member->id.'">'.$member->rf_id.'</a></strong></em></p>
                    <p><span style="text-decoration: underline;">Gender:</span><em><strong>&nbsp;'.$member->gender.'</strong></em></p>
                    <p><span style="text-decoration: underline;">Birthday:</span><em><strong>&nbsp;'.$bday->format('M d, Y').'</strong></em></p>
                    <p><span style="text-decoration: underline;">Address:</span><em><strong>&nbsp;'.$member->address.' | '.$member->barangay_name.'</strong></em></p>
                    <p><span style="text-decoration: underline;">Contact Number:</span><em><strong>&nbsp;'.$member->landline.' / '.$member->mobile.'</strong></em></p>';
            
            array_push($arrMembers, $html);
            unset($html);
        }
        
        $response ['draw'] = Input::post ( "draw" );
        $response ['recordsTotal'] = $totalCount;
        $response ['recordsFiltered'] = $filteredCount;
        $response ['data'] = $arrMembers;
        Response::setBody ( json_encode ( $response ) );
    }
}
