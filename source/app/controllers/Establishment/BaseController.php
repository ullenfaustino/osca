<?php

namespace Establishment;

use \App;
use \Menu;
use \Module;
use \Sentry;
use \Users;
use \EstablishmentUsers;
use \Establishment;

class BaseController extends \BaseController {
	public function __construct() {
		parent::__construct();
		$this -> data['menu_pointer'] = '<div class="pointer"><div class="arrow"></div><div class="arrow_border"></div></div>';

		$adminMenu = Menu::create('member_sidebar');
		
		if (Sentry::check()) {
		    $user = Sentry::getUser();
		    $establishment = Users::leftJoin('establishment_users as EU',"EU.user_id","=","users.id")
		                          -> leftJoin("establishment as E","E.id","=","EU.establishment_id")
		                          -> where("users.id","=",$user -> id)
		                          -> first();
		    $this -> data["establishment"] = $establishment;
		}

		foreach (Module::getModules () as $module) {
			$module -> registerEstablishmentMenu();
		}
	}

}
