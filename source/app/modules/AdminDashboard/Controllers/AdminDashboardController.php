<?php

namespace AdminDashboard\Controllers;

use \User;
use \Users;
use \App;
use \View;
use \Menu;
use \Input;
use \Sentry;
use \Request;
use \Response;
use \Exception;
use \Admin\BaseController;
use \Cartalyst\Sentry\Users\UserNotFoundException;
use \Illuminate\Database\Capsule\Manager as DB;

use \Barangay;
use \SCMembers;
use \Town;
use \Contributions;
use \GenericHelper;
use \Booklet;
use \BookletTransaction;

class AdminDashboardController extends BaseController {

	public function __construct() {
		parent::__construct();
		$this -> data['active_menu'] = 'admin_dashboard';
	}

	public function index() {
		$user = Sentry::getUser();
		$this -> data['title'] = 'Dashboard';

		$towns = Town::getAllTowns();
        $this -> data['towns'] = $towns;
		$this -> data['years'] = $this -> getYears();
		
		App::render('@admindashboard/index.twig', $this -> data);
	}
	
	public function viewSCDetails($sc_id) {
		$member = SCMembers::find($sc_id);
		$this -> data['member'] = $member;
		$this -> data['title'] = $member -> first_name . " " . $member -> middle_name . " " . $member -> last_name;
		$this -> data['years'] = $this -> getYears();
		
		$this -> data['contributions'] = Contributions::where("sc_member_id", "=", $sc_id) -> get();
		
		$transactions = $this -> getTransactions($sc_id);
		$this -> data['transactions'] = $transactions;
		
		App::render('@admindashboard/sc_details.twig', $this -> data);
	}
	
	/*
	* API for member contributions
	*/
	public function membersContributions($sc_id, $year) {
		$months = GenericHelper::monthList();
		foreach ($months as $key => $month) {
			$total_contributions = Contributions::where("sc_member_id", "=", $sc_id) 
										-> whereRaw(sprintf("MONTH(`created_at`) = '%s' AND YEAR(`created_at`) = '%s'", $month -> month, $year)) 
										-> sum('amount');
			$month -> total_contributions = $total_contributions;
		}
		Response::setBody(json_encode($months));
	}
	
	/*
	* API for member list per barangay
	*/
	public function sc_members($town_id) {
		$barangays = Barangay::where("town_id","=",$town_id) -> get();
		foreach ($barangays as $key => $barangay) {
			$members_count = SCMembers::where("barangay_id","=",$barangay->id) -> count();
			$barangay -> members_count = $members_count;
		}
		Response::setBody(json_encode($barangays));
	}
	
	/*
	* API for member purchased transactions
	*/
	public function purchasedTransactions($sc_id, $year) {
		$establishments = BookletTransaction::leftJoin('establishment as E', "E.id","=","booklet_transaction.establishment_id")
										-> leftJoin("sc_members as SC","SC.id","=","booklet_transaction.sc_member_id")
										-> where("booklet_transaction.sc_member_id", "=", $sc_id)
										-> whereRaw(sprintf("YEAR(booklet_transaction.created_at) = '%s'", $year))
										-> groupBy("E.id")
										-> select(DB::raw('sum(booklet_transaction.total_amount) as total_amount'), 
													"E.name")
										-> get();
		Response::setBody(json_encode($establishments));
	}
	
	private function getTransactions($sc_id) {
		$transactions = BookletTransaction::leftJoin('establishment as E', "E.id","=","booklet_transaction.establishment_id")
										-> leftJoin("sc_members as SC","SC.id","=","booklet_transaction.sc_member_id")
										-> where("booklet_transaction.sc_member_id", "=", $sc_id)
										// -> groupBy("E.id")
										-> select(array("*", "booklet_transaction.id", "booklet_transaction.created_at"))
										-> get();
		return $transactions;
	}
	
	private function getYears() {
		return SCMembers::select(DB::raw('count(id) as `data`'), 
									DB::raw('YEAR(created_at) year'))
							->groupby('year')
							->orderby('updated_at','ASC')
							->get();
	}
	
	public function memberList() {
    	$barangay_id = Input::post("barangay_id");
        $limit = Input::post("length");
        $start = Input::post("start");
        $searchValue = trim(Input::post("search")['value'], '"');
        $order = Input::post("order")[0]['column'];
        $dir = Input::post("order")[0]['dir'];
        
        $members = SCMembers::leftJoin ( 'barangay as B', 'B.id', '=', 'sc_members.barangay_id' )
                            ->select ( array ('*',
                                            'sc_members.id as id',
                                            'B.name as barangay_name',
                                            'sc_members.created_at as created_at'))
                            -> where("sc_members.barangay_id","=",$barangay_id)
                            -> where("sc_members.is_deceased","=",0);
        $tmpMembers = $members;
        $totalCount = $tmpMembers->groupBy("sc_members.id")->get()->count();
        if (strlen ( $searchValue ) > 0) {
            $members = $members -> whereRaw("(sc_members.first_name like '%$searchValue%' or
        												 sc_members.rf_id like '%$searchValue%' or 
        												 sc_members.middle_name like '%$searchValue%' or
        												 sc_members.last_name like '%$searchValue%' or
        												 sc_members.created_at like '%$searchValue%' or
        												 B.name like '%$searchValue%')");
        }
        
        $tmpMemberFiltered = $members;
        $filteredCount = $tmpMemberFiltered->groupBy("sc_members.id")->get()->count();
        $members = $members->offset($start)->take($limit);
        $members = $members->get ();
        $arrMembers = array ();
        foreach ( $members as $key=>$member ) {
        	$bday = new \DateTime($member->birthday);
        	
            $tmp [] = sprintf("%s",$member->id);
            $tmp [] = sprintf("<a href='/admin/dashboard/view/details/sc/%s'>%s</a>", $member->id, $member->rf_id);
            $tmp [] = sprintf("%s",$member->first_name);
            $tmp [] = sprintf("%s",$member->middle_name);
            $tmp [] = sprintf("%s",$member->last_name);
            $tmp [] = sprintf("%s",$member->gender);
            $tmp [] = sprintf("%s",$bday->format('M d, Y'));
            $tmp [] = sprintf("%s",$member->address);
            $tmp [] = sprintf("%s",$member->barangay_name);
            $c_at = new \DateTime($member->created_at);
            $tmp [] = sprintf("%s",$c_at->format('M d, Y h:i:s a'));
            
            array_push($arrMembers, $tmp);
            unset($tmp);
        }
        
        $response ['draw'] = Input::post ( "draw" );
        $response ['recordsTotal'] = $totalCount;
        $response ['recordsFiltered'] = $filteredCount;
        $response ['data'] = $arrMembers;
        Response::setBody ( json_encode ( $response ) );
    }
}
