<?php

namespace Staff;

use \App;
use \Menu;
use \Module;
use \Sentry;
use \Users;

class BaseController extends \BaseController {
	public function __construct() {
		parent::__construct();
		$this -> data['menu_pointer'] = '<div class="pointer"><div class="arrow"></div><div class="arrow_border"></div></div>';

		$adminMenu = Menu::create('member_sidebar');
        
        if (Sentry::check()) {
            $user = Sentry::getUser();
            $brgy = Users::leftJoin('barangay_users as BU',"BU.user_id","=","users.id")
                                  -> leftJoin("barangay as B","B.id","=","BU.barangay_id")
                                  -> where("users.id","=",$user -> id)
                                  -> first();
            $this -> data["brgy"] = $brgy;
        }
        
		foreach (Module::getModules () as $module) {
			$module -> registerStaffMenu();
		}
	}

}
