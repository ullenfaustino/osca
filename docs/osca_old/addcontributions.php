<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>OSCA - Contributions</title>
		<meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no">
		<link rel="shortcut icon" href="favicon_16.ico"/>
		<link rel="bookmark" href="favicon_16.ico"/>
		<!-- site css -->
		<link rel="stylesheet" href="dist/css/site.min.css">
		<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,800,700,400italic,600italic,700italic,800italic,300italic" rel="stylesheet" type="text/css">
		<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
		<script type="text/javascript" src="dist/js/site.min.js"></script>
		<style>
			table, th, td {
			border: 2px solid black;
			border-collapse: collapse;
			border-spacing: 10px;
			}
			th,td{
			padding: 5px;

			}
		</style>
		<script src="res\jq\external/jquery/jquery.js"></script>
		<script src="res\jq\jquery-ui.js"></script>
		<link href="res\jq\jquery-ui.css" rel="stylesheet">
	</head>
	<body>
		<?php
			include 'OSCAFunctions.php';
			SESSION_START();
			if (!isset($_SESSION['userid'])){
				header("Location: login.php");
			}else{
				StartPage();
				$UserID=$_SESSION['userid'];
				$UserType=$_SESSION['usertype'];
				if ($_SESSION['usertype']=="Admin"){
					DrawAdminHeader();
				}else{
					DrawStaffHeader($_SESSION['corrbrgy']);
				}
			}
			
			if (isset($_POST['scid'])) {
				AddContr($_POST['scid'],$_POST['ContDate'],$_POST['ContAmount']);
			}
		?>
		<!--header-->

		<div class="container-fluid">
		<!--documents-->
			<div class="row row-offcanvas row-offcanvas-left">
				<?php 
					if ($_SESSION['usertype']=="Admin"){
						DrawAdminSidebar();
					}else{
						DrawStaffSidebar();
					}
					// include_once 'update_checker.php';
					echo '
						Add Contributions
						<div id="frm">
							<form action="addcontributions.php" method="GET">
								Search: <input type="text" name="id"/>
							</form>
						<div>
					';
					if (isset($_GET['id'])){
						$id = $_GET['id'];
						$con = mysqli_connect("localhost","root","","login");
						if (!$con) {
							die(mysql_error());
						}
						$results = $con->query("SELECT * FROM registration_barangay WHERE ID='$id'");
						$row=$results->fetch_array();
						if ($row['ID']==''){
							echo 'ID not existing';
						}else{
							if ($_SESSION['usertype']=="Staff"){
								if ($row['Address']!=$_SESSION['corrbrgy']){
									echo 'ID: <b>' . $row['ID'] . '</b><br /> Mismatch. The senior citizen\'s Barangay is <b>' . $row['Address'] . '</b>. This staff can add contributions under <b>' . $_SESSION['corrbrgy'] . '</b>';
								}else{
									goto PrepAddCont;
								}
							}else{
								PrepAddCont:
								echo '
									<div id="frm">
										<form action="addcontributions.php" method="POST">
											<br>
											<input type="hidden" value="'.  $row['ID']  .'" name="scid" class="textInput" placeholder="FirstName" required><br>
											
											<label>Date:</label><br>
											<input type="text" id="datepicker" value="" name="ContDate" class="textInput" placeholder="YYYY-MM-DD" required><br>

											<label>Contributions:</label><br>
											<input type="number"  value="" name="ContAmount" class="textInput" placeholder="Contributions" required><br>
											<br />
											<label>Submit</label>
											<input type="Submit" name="update_btn" value="Update">
										</form>
										<br />
										<b>
										<div id="ContTable">
											<table border=2 cellpadding>
												<tr>
													<td>
														ID: 
													</td>
													<td colspan=5>
														 '.$row['ID'].'
													</td>
												</tr>
												<tr>
													<td colspan=6>
														<br />
													</td>
												</tr>
												<tr>
													<td>
														First Name: 
													</td>
													<td>
														 '.$row['Fname'].'
													</td>
													<td>
														Middle Name: 
													</td>
													<td>
														 '.$row['Mname'].'
													</td>
													<td>
														Last Name: 
													</td>
													<td>
														 '.$row['Lname'].'
													</td>
												</tr>
												<tr>
													<td>
														Birthday: 
													</td>
													<td colspan=2>
														 '.$row['Birthday'].'
													</td>
													<td>
														Age: 
													</td>
													<td colspan=2>
														 '.date_diff(date_create($row['Birthday']), date_create('today'))->y.'
													</td>
												</tr>
												<tr>
													<td>
														Gender: 
													</td>
													<td colspan=2>
														 '.$row['Gender'].'
													</td>
													<td>
														Status: 
													</td>
													<td colspan=2>
														 '.$row['SCStatus'].'
													</td>
												</tr>
												<tr>
													<td>
														Address: 
													</td>
													<td colspan=2>
														 '.$row['Address'].'
													</td>
													<td>
														Contact Number: 
													</td>
													<td colspan=2>
														 '.$row['Contactnumber'].'
													</td>
												</tr>
												<tr>
													<td colspan=6>
														<br />
													</td>
												</tr>
												<tr>
													<td colspan=6>
														Contributions
													</td>
												</tr>
												<tr>
													<td colspan=3>
														Date
													</td>
													<td colspan=3>
														Amount
													</td>
												</tr>
												';
												
												$querygetcont=mysql_query("SELECT * FROM  `barangay_contributions` WHERE id='".$row['ID']."'  ORDER BY `Date` ASC");
												while ($rowcont=mysql_fetch_array($querygetcont)){
													echo '
														<tr>
															<td colspan=3>
																'.$rowcont['Date'].'
															</td>
															<td colspan=3>
																'.$rowcont['Contributions'].'
															</td>
														</tr>
													';
												}
												
											echo'</table>
											<form action="printcontributions.php" target="_blank" method="GET">
												<input type="hidden" value="'.  $row['ID']  .'" name="printcontid" class="textInput" placeholder="" required><br>
												<input type="submit" value="Print Contribution Report" />
											</form>
										</div>
										<br /><br />
										</b>
									</div>
								';
							}
						}
					}
				?>
			</div>
		</div>
		<script>
			$( "#datepicker" ).datepicker({
				inline: true,
				dateFormat: 'yy-mm-dd'
			});
		</script>
	</body>
<html>