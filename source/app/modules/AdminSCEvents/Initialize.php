<?php

namespace AdminSCEvents;

use \App;
use \Menu;
use \Route;

class Initialize extends \SlimStarter\Module\Initializer {

	public function getModuleName() {
		return 'AdminSCEvents';
	}

	public function getModuleAccessor() {
		return 'adminscevents';
	}

	// public function registerMemberMenu(){
	// }

	public function registerAdminRoute() {
		Route::resource('/events', 'AdminSCEvents\Controllers\AdminSCEventsController');
		
		Route::get('/events/show/all', 'AdminSCEvents\Controllers\AdminSCEventsController:getAllEvents');
		Route::post('/events/add/new', 'AdminSCEvents\Controllers\AdminSCEventsController:setEvent') -> name("admin_add_event");
		Route::post('/events/delete', 'AdminSCEvents\Controllers\AdminSCEventsController:deleteEvent') -> name("admin_delete_event");
	}

}
