<?php

class User extends Model {
	protected $table = "users";
	public function sponsor($id)
    {
    	return User::join('direct_referrals as dr','dr.recruitee_id', '=', 'users.id')
    			->leftJoin('users as u','u.id', '=', 'dr.recruiter_id')
    			->leftJoin('users as us','us.id', '=', 'dr.recruitee_id')
    			->where('users.id', '<>', $id)
    			->where('users.username', '<>', 'admin')
    			->select(['*','dr.recruitee_id as uid','u.username as sponsor', 'users.username as username'
    				, 'users.first_name as first_name', 'users.middle_name as middle_name'
    				, 'users.first_name as first_name', 'users.last_name as last_name', 'us.created_at as created_at']);
    }
    
    public function members($id) {
    	return User::leftJoin('hierarchy_siblings as HS','HS.recruitee_id','=','users.id')
    				->where('HS.recruitee_id', '<>', $id)
    				->where('users.username', '<>', 'power7')
					->where('users.is_guest', '<>', 1)
    				->groupBy('HS.recruitee_id')
    				->select(array('*','HS.created_at as created_at','users.id as uid'));
    }

    public function groupByCreatedAt($id) {
    	return User::where('users.id', '<>', $id)
					->whereNull('permissions')
					->where('users.username', '<>', 'power7')
					->where('users.is_guest', '<>', 1)
    				->groupBy(new raw('DAY(created_at)'))
    				->select([new raw('DATE(created_at) as date ,count(*) as count')]);

    }
    
    public function getUserDetails($id) {
		return Users::join('members_profile as MF','MF.user_id', '=', 'users.id')
	       ->where('users.id', '=', $id)
	       ->select(['*', 'users.id as uid']);
	}
	
	public function getUsersByDate($date) {
		$arrUsers = array();
		$users = Users::where(new raw('DATE(created_at)'), '=', $date)
				->get();
		foreach ($users as $key => $user) {
			if ($user != null && $user->permissions === null) {
				$_user = DirectReferrals::getDirect($user->id)->first();

				if($_user == null) {
					$user->sponsor_name = "";
				} else {
					$user->sponsor_name = $_user->username;
				}

				array_push($arrUsers, $user);
			}
		}
		return $arrUsers;
	}
}
