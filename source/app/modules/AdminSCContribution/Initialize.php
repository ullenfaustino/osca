<?php

namespace AdminSCContribution;

use \App;
use \Menu;
use \Route;

class Initialize extends \SlimStarter\Module\Initializer {

	public function getModuleName() {
		return 'AdminSCContribution';
	}

	public function getModuleAccessor() {
		return 'adminsccontribution';
	}

	// public function registerMemberMenu(){
	// }

	public function registerAdminRoute() {
		Route::resource('/contribution', 'AdminSCContribution\Controllers\AdminSCContributionController');
		Route::get('/contribution/view/:sc_id', 'AdminSCContribution\Controllers\AdminSCContributionController:contributionsView');
		Route::get('/contribution/print/receipt/:sc_id', 'AdminSCContribution\Controllers\AdminSCContributionController:printReceipt');
		
		Route::post('/contribution/search/member', 'AdminSCContribution\Controllers\AdminSCContributionController:memberList') -> name("admin_contribution_member_list");
		Route::post('/contribution/member/add', 'AdminSCContribution\Controllers\AdminSCContributionController:addContribution') -> name("admin_contribution_member_add");
		Route::post('/contribution/member/delete', 'AdminSCContribution\Controllers\AdminSCContributionController:deleteContribution') -> name("admin_contribution_member_delete");
	}

}
