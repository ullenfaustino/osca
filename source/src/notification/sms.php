<?php
include __DIR__ . "/../../app/bootstrap/start.php";

class Notification extends BaseController {

    function __construct() {
        $this -> notifications();
    }

    public function notifications() {
        echo "[Notification][notifications]\n";
        $today = new \DateTime('now');
        $today = $today -> format('Y-m-d H:i:s');
        echo "[Notification][notifications][Today]|---> $today\n";

        $events = Events::where("is_notified", "=", 0) -> whereRaw(sprintf("`notif_date` <= '%s'", $today)) -> get();
        foreach ($events as $key => $event) {
            echo sprintf("[Notification][notifications][Event] %s \n", $event -> title);

            $event -> is_notified = 1;
            $event -> save();

            $members = SCMembers::where("barangay_id", "=", $event -> barangay_id) -> where("is_deceased", "=", 0) -> get();
            foreach ($members as $key => $member) {
                echo sprintf("[Notification][notifications][Member] %s %s \n", $member -> first_name, $member -> last_name);
                $this -> sendSMS($event, $member);
            }
        }
    }

    private function sendSMS($event, $member) {
        $dt_start = new \DateTime($event -> event_start);
        $dt_start = $dt_start -> format('F d, Y h:i a');
        
        $dt_end = new \DateTime($event -> event_end);
        $dt_end = $dt_end -> format('F d, Y h:i a');
        
        $mobile = $member -> mobile;
        echo "[SENDTO]|--> $mobile\n";
        $message = "[Reminders from OSCA]\n";
        $message .= sprintf("Event Title: %s\n", $event -> title);
        $message .= sprintf("When: %s to %s\n", $dt_start, $dt_end);
        echo $message;
        $sms = GenericHelper::sendSMS($mobile, $message);
        print_r($sms);
    }

}

new Notification();
?>