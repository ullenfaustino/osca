<?php

namespace AdminManagement;

use \App;
use \Menu;
use \Route;

class Initialize extends \SlimStarter\Module\Initializer {

	public function getModuleName() {
		return 'AdminManagement';
	}

	public function getModuleAccessor() {
		return 'adminmanagement';
	}

	// public function registerMemberMenu(){
	// }

	public function registerAdminRoute() {
		Route::resource('/management', 'AdminManagement\Controllers\AdminManagementController');
		Route::get('/management/view/establishment', 'AdminManagement\Controllers\AdminManagementController:establishmentView');
		Route::get('/management/view/barangay', 'AdminManagement\Controllers\AdminManagementController:barangayView');
		
		Route::post('/management/view/establishment/add', 'AdminManagement\Controllers\AdminManagementController:addEstablishment') -> name('add_establishment');
		Route::post('/management/view/brgy/add', 'AdminManagement\Controllers\AdminManagementController:addBarangay') -> name('add_brgy');
        Route::post('/management/view/brgy/add/town', 'AdminManagement\Controllers\AdminManagementController:addTown') -> name('add_town');
    }

}
