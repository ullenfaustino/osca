<?php
	//----------------------- Start the page with session
	function StartPage(){
		$con=mysql_connect('localhost','root','');
		mysql_select_db('login',$con);
		$message='';
	}
	
	function UserLogin($uname,$pword){
		$query=mysql_query("SELECT * FROM `user` WHERE Username='$uname' and Password='$pword'");// and Status='Active'");
		if ($query){
			$numofrows=mysql_num_rows($query);
			if ($numofrows==1){
				$_SESSION['userid']=mysql_result($query,0,'id');
				$_SESSION['usertype']=mysql_result($query,0,'User_type');
				$_SESSION['corrbrgy']=mysql_result($query,0,'CorrBrgy');
				if ($_SESSION['usertype']=="Admin"){
					header("Location: process.php");
				}else{
					header("Location: addcontributions.php");
				}
			}else{					
				echo '<script>';
					echo 'alert("Access Denied")';
				echo '</script>';
			}
		}else{				
			echo '<script>';
				echo 'alert("Access Denied")';
			echo '</script>';
		}
	}
	
	function Logout(){
		// Quote the SESSION_START(); if session is already started
		SESSION_START();
		session_destroy();
		header("Location: login.php");
	}
	
	//Message box for PHP
	function MyPHPMsgBox($Message){
		echo '
			<script>
				alert ("' . $Message . '")
			</script>
		';
	}
	
	function VDate($tdate)
	{
		$d = DateTime::createFromFormat('Y-m-d', $tdate);
		return $d && $d->format('Y-m-d') === $tdate;
	}
	
	//Draw your Sidebar
	function DrawAdminSidebar(){
		echo '
			<div class="col-xs-6 col-sm-3 sidebar-offcanvas" role="navigation">
				<ul class="list-group panel">
					<li class="list-group-item">
						<a href="process.php">
							<i class="glyphicon glyphicon-home"></i>
							Dashboard
						</a>
					</li>
					<li class="list-group-item">
						<a href="register_senior.php">
							<i class="glyphicon glyphicon-user"></i>
							Register Señior Citizen
						</a>
					</li>
					<li class="list-group-item">
						<a href="update_senior.php">
							<i class="glyphicon glyphicon-floppy-open"></i>
							Update Records
						</a>
					</li>
					<li class="list-group-item">
						<a href="adduser.php">
							<i class="glyphicon glyphicon-plus"></i>
							Add User
						</a>
					</li>
					<li class="list-group-item">
						<a href="addcontributions.php">
							<i class="fa fa-money"></i>
							Contributions
						</a>
					</li>
					<li class="list-group-item">
						<a href="mortalityrate.php">
							<i class="fa fa-line-chart"></i>
							Mortality Rate
						</a>
					</li>
					<li class="list-group-item">
						<a href="events.php">
							<i class="glyphicon glyphicon-calendar"></i>
							Events
						</a>
					</li>
					<li class="list-group-item">
						<a href="purchasebook.php">
							<i class="glyphicon glyphicon-book"></i>
							Purchase Book
						</a>
					</li>
				</ul>
			</div>
		';
	}
	
	//Draw your Header
	function DrawAdminHeader(){
		echo '
			<!--nav-->
			<nav role="navigation" class="navbar navbar-custom">
				<div class="container-fluid">
					<!-- Brand and toggle get grouped for better mobile display -->
					<div class="navbar-header">
						<button data-target="#bs-content-row-navbar-collapse-5" data-toggle="collapse" class="navbar-toggle" type="button">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						</button>
						<a href="#" class="navbar-brand">Administrator Panel</a>
					</div>

					<!-- Collect the nav links, forms, and other content for toggling -->
					<div id="bs-content-row-navbar-collapse-5" class="collapse navbar-collapse">
						<ul class="nav navbar-nav navbar-right">
							<li class="dropdown">
								<a data-toggle="dropdown" class="dropdown-toggle" href="#"><i class="fa fa-user fa-fw"></i>ADMINISTRATOR<b class="caret"></b></a>
								<ul role="menu" class="dropdown-menu">
									<li class="disabled">
										<a href="logout.php"><i class="fa fa-sign-out fa-fw"></i>LOGOUT</a>
									</li>
								</ul>
							</li>
						</ul>

					</div><!-- /.navbar-collapse -->
				</div><!-- /.container-fluid -->
			</nav>
		';
	}
	
	function DrawStaffSidebar(){
		echo '
			<div class="col-xs-6 col-sm-3 sidebar-offcanvas" role="navigation">
				<ul class="list-group panel">
					<li class="list-group-item">
						<a href="addcontributions.php">
							<i class="fa fa-money"></i>
							Contributions
						</a>
					</li>
					<li class="list-group-item">
						<a href="events.php">
							<i class="glyphicon glyphicon-calendar"></i>
							Events
						</a>
					</li>
				</ul>
			</div>
		';
	}
	
	function DrawStaffHeader($brgy){
		echo '
			<!--nav-->
			<nav role="navigation" class="navbar navbar-custom">
				<div class="container-fluid">
					<!-- Brand and toggle get grouped for better mobile display -->
					<div class="navbar-header">
						<button data-target="#bs-content-row-navbar-collapse-5" data-toggle="collapse" class="navbar-toggle" type="button">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						</button>
						<a href="#" class="navbar-brand">Staff Panel | '.strtoupper  ($brgy).'</a>
					</div>

					<!-- Collect the nav links, forms, and other content for toggling -->
					<div id="bs-content-row-navbar-collapse-5" class="collapse navbar-collapse">
						<ul class="nav navbar-nav navbar-right">
							<li class="dropdown">
								<a data-toggle="dropdown" class="dropdown-toggle" href="#"><i class="fa fa-user fa-fw"></i>STAFF<b class="caret"></b></a>
								<ul role="menu" class="dropdown-menu">
									<li class="disabled">
										<a href="logout.php"><i class="fa fa-sign-out fa-fw"></i>LOGOUT</a>
									</li>
								</ul>
							</li>
						</ul>

					</div><!-- /.navbar-collapse -->
				</div><!-- /.container-fluid -->
			</nav>
		';
	}
	
	function NewUser($ut, $un, $pw, $brgy,$rpw){
		if ($pw==$rpw){
			$userexist = False;
			$querygettype=mysql_query("SELECT * FROM  `user` WHERE Username='".$un."'");
			while ($row=mysql_fetch_array($querygettype)){
				$userexist = True;
			}
			if ($userexist==False){
				$queryadd=mysql_query("INSERT INTO `user`(`User_type`, `Username`, `Password`, `CorrBrgy`) VALUES ('".$ut."','".$un."','".$pw."','".$brgy."')");
				MyPHPMsgBox("New User Added!");
				// echo '&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp New User Successfully Added | <b>' . $un . '</b>';
			}else{
				MyPHPMsgBox("Username already existed!");
			}
		}else{
			MyPHPMsgBox("Passwords do not match!");
		}
	}
	
	function UpdateSC($id, $fn, $mn, $ln, $bd, $gd, $address, $stat, $cn){
		if (VDate($bd)==1){
			$queryupd=mysql_query("UPDATE `registration_barangay` SET `Fname`='" . $fn . "',`Mname`='" . $mn . "',`Lname`='" . $ln . "',`Birthday`='" . $bd . "',`Gender`='" . $gd . "',`Address`='" . $address . "',`SCStatus`='" . $stat . "',`Contactnumber`='" . $cn . "' WHERE ID='" . $id . "';");
			header("Location: update_senior.php?id=" . $id);
			MyPHPMsgBox("Record Update!");
		}else{
			MyPHPMsgBox ('Date format for Birthdate is not valid!');
			echo '<section style="color:red">Date format for Birthdate is not valid!</section>';
		}
	}
	
	function AddContr($id,$dateadd,$contr){
		if (VDate($dateadd)==1){
			$queryadd=mysql_query("INSERT INTO `barangay_contributions`(`id`, `Date`, `Contributions`) VALUES ('".$id."','".$dateadd."','".$contr."')");
			MyPHPMsgBox("Contributions Added!");
			header("Location: addcontributions.php?id=" . $id);
		}else{
			MyPHPMsgBox ('Format for date is not valid!');
			echo '<section style="color:red">Format for date is not valid!</section>';
		}
	}
	
	function DrawGraph_MortRate($df,$dt){
		$querymortrate=mysql_query("SELECT Year, SUM(SA) AS SA, SUM(SD) AS SD FROM
		(
			SELECT Year, SUM(Alive) AS SA, SUM(Deceased) AS SD FROM
			(
				SELECT YEAR(`Birthday`) as Year, 
				(CASE WHEN `SCStatus`='Alive' THEN 1 ELSE 0 END) AS Alive,
				(CASE WHEN `SCStatus`='Deceased' THEN 1 ELSE 0 END) AS Deceased

				FROM `registration_barangay`
				WHERE YEAR(`Birthday`) BETWEEN " .$df. " AND " .$dt. "
			) AS MR
			GROUP BY Year, Alive, Deceased
		) RMR
		GROUP BY Year");
		$numResults = mysql_num_rows($querymortrate);
		$counter = 0;
		$yl='';
		$ac='';
		$dc='';
		while ($row=mysql_fetch_array($querymortrate)){
			if (++$counter == $numResults) {
				$yl .= "'" . $row['Year'] . "'";
				$ac .= $row['SA'];
				$dc .= $row['SD'];
			} else {
				$yl .= "'" . $row['Year'] . "',";
				$ac .= $row['SA'] . ",";
				$dc .= $row['SD'] . ",";
			}
		}
		if ($numResults>0){
			echo "
				$(function () {
					Highcharts.chart('container', {
						title: {
							text: 'Yearly Mortality Rate',
							x: -20 //center
						},
						subtitle: {
							text: 'Year: ".$df."-".$dt."',
							x: -20
						},
						xAxis: {
							categories: [".$yl."]
						},
						yAxis: {
							title: {
								text: 'Count'
							},
							plotLines: [{
								value: 0,
								width: 1,
								color: '#808080'
							}]
						},
						tooltip: {
							valueSuffix: ''
						},
						legend: {
							layout: 'vertical',
							align: 'left',
							verticalAlign: 'middle',
							borderWidth: 0
						},
						series: [{
							name: 'Alive',
							data: [".$ac."]
						}, {
							name: 'Deceased',
							data: [".$dc."]
						}]
					});
				});
			";
		}else{
			echo 'alert ("0 Records")';
		}		
	}
	
	function NewSC(){
		//From Prev v
		$servername ="localhost";
		$serverusername ="root";
		$serverpassword ="";
		$dbname = "login";

		$conn = mysqli_connect($servername, $serverusername, $serverpassword, $dbname);
		if (!$conn) {
			die("Connection failed: " . mysqli_connect_error());
		}
		$userexist=False;
		$checkid=mysql_query("SELECT * FROM  `registration_barangay` WHERE id='".$_POST['SCID']."'");
		while ($row=mysql_fetch_array($checkid)){
			$userexist = True;
		}
		if ($userexist==False){
			if (VDate($_POST['Birthday'])==1){
				$FirstName = mysql_real_escape_string($_POST['FirstName']);
				$MiddleName = mysql_real_escape_string($_POST['MiddleName']);
				$LastName = mysql_real_escape_string($_POST['LastName']);
				$Birthday = mysql_real_escape_string($_POST['Birthday']);
				// $Age = mysql_real_escape_string($_POST['Age']);
				$Gender = mysql_real_escape_string($_POST['Gender']);
				$Barangay = mysql_real_escape_string($_POST['address_value']);
				$ContactNumber = mysql_real_escape_string($_POST['ContactNumber']);
				$SCStatus = 'Alive';
				$SCID = mysql_real_escape_string($_POST['SCID']);
				$sql = "INSERT INTO registration_barangay(id, Fname, Mname, Lname, Birthday, Gender, Address, SCStatus, Contactnumber) VALUES('$SCID', '$FirstName', '$MiddleName', '$LastName', '$Birthday', '$Gender', '$Barangay', '$SCStatus', '$ContactNumber')";

				if (mysqli_query($conn, $sql)) {
					echo "New record created successfully";
					header('Location: process.php');
				}else{
					echo "Error: " . $sql . "<br>" . mysqli_error($conn);
				}

				mysqli_close($conn);
			}else{
				MyPHPMsgBox ('Date format for Birthdate is not valid!');
				echo 'Date format for Birthdate is not valid! <br /> <a href="register_senior.php">Click here to go back to <b>Register Señior Citizen Page</b></a>';
			}
		}else{
			MyPHPMsgBox("ID already existed!");
		}
	}
	
	function DrawEvents(){
		echo '
			<table style="width: 100%" id="evtable">
				<tr>
					<th style="width: 10%">
						Date
					</th>
					<th style="width: 20%">
						Event
					</th>
					<th style="width: 20%">
						Place
					</th>
					<th style="width: 10%">
						Budget
					</th>
					<th style="width: 30%">
						Remarks
					</th>
					<th style="width: 10%">
						Action
					</th>
				<tr>';
				$eventlistquery=mysql_query("SELECT * FROM  `events_list` ORDER BY evwhen, eventsid");
				while ($row=mysql_fetch_array($eventlistquery)){
					echo '
						<tr>
							<td style="width: 10%">
								'.$row['evwhen'].'
							</td>
							<td style="width: 20%">
								'.$row['evwhat'].'
							</td>
							<td style="width: 20%">
								'.$row['evwhere'].'
							</td>
							<td style="width: 10%">
								'.$row['budget'].'
							</td>
							<td style="width: 40%">
								'.nl2br($row['remarks']).'
							</td>
							<th style="width: 40%">
								<form action="events.php" method="GET">
									<input type="hidden" name="evid" value="'.$row['eventsid'].'">
									<input type="submit" value="Edit">
								</form>
							</th>
						<tr>
					';
				}
			echo '</table>
		';
	}
	
	function DrawEditEvent($evid){
		$queryupdevent=mysql_query("SELECT * FROM  `events_list` WHERE eventsid='".$evid."'  ORDER BY evwhen, eventsid");
		while ($row=mysql_fetch_array($queryupdevent)){
			echo'
				<td style="border: 0 solid black;">
					<div>
						<form action="events.php" method="POST">
							<table style="width: 39%; border:thin white inset">
								<tr>
									<td colspan=2 style="border: 0 solid black;">
										<u>Edit Event</u>
										<input type="hidden" value="'.$evid.'" name="updevid" class="textInput" />
									</td>
								</tr>
								<tr>
									<td colspan=2 style="border: 0 solid black;">
									<label>Date:</label><br>
										<input type="text" id="datepicker2" value="'.$row['evwhen'].'" name="updevdate" class="textInput" placeholder="YYYY-MM-DD" required>
									</td>
								</tr>
								<tr>
									<td style="border: 0 solid black;">
										<label>Event:</label><br>
										<input type="text" value="'.$row['evwhat'].'" name="updevwhat" class="textInput" placeholder="Event" required>
									</td>
									<td style="border: 0 solid black;">
										<label>Place:</label><br>
										<input type="text" value="'.$row['evwhere'].'" name="updevwhere" class="textInput" placeholder="Place" required>
									</td>
								</tr>
								<tr>
									<td colspan=2 style="border: 0 solid black;">
										<label>Budget:</label><br>
										<input type="number"  value="'.$row['budget'].'" name="updbudget" class="textInput" placeholder="Budget" required>
									</td>
								</tr>
								<tr>
									<td colspan=2 style="border: 0 solid black;">
										<label>Remarks:</label><br>
										<textarea rows="4" cols="50" name="updremarks" placeholder="Put Remarks here..." required>'.$row['remarks'].'</textarea>
									</td>
								</tr>
								<tr>
									<td colspan=2 style="border: 0 solid black;">							
										<input type="Submit" value="Update Event">
									</td>
								</tr>
							</table>
						</form>
						<br /><br />
					</div>
				</td>
			';
		}
	}
	
	function NewEvent($evwhat,$evwhere,$evwhen,$budget,$remarks){
		if (VDate($evwhen)==1){
			$queryadd=mysql_query("INSERT INTO `events_list`(`evwhat`, `evwhere`, `evwhen`, `budget`, `remarks`) VALUES ('".$evwhat."','".$evwhere."','".$evwhen."','".$budget."','".$remarks."')");
			MyPHPMsgBox("New Event Added!");
			// echo '&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp New User Successfully Added | <b>' . $un . '</b>';
		}else{
			MyPHPMsgBox("Date format for event date is not valid!");
		}
	}
	
	function UpdateEvent($evid,$evwhat,$evwhere,$evwhen,$budget,$remarks){
		if (VDate($evwhen)==1){
			$queryadd=mysql_query("UPDATE `events_list` SET `evwhat`='".$evwhat."',`evwhere`='".$evwhere."',`evwhen`='".$evwhen."',`budget`='".$budget."',`remarks`='".$remarks."' WHERE `eventsid`='".$evid."'");
			MyPHPMsgBox("Event Updated!");
			// echo '&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp New User Successfully Added | <b>' . $un . '</b>';
		}else{
			MyPHPMsgBox("Date format for event date is not valid!");
		}
	}
	
	function AddtoCart($tid,$iid,$iqty){
		$queryaddtocart=mysql_query("INSERT INTO `pos_trans`(`invoiceid`, `itemid`, `qty`) VALUES ('".$tid."','".$iid."','".$iqty."')");
	}
	
	function ShowCart($id){
		$queryscart=mysql_query("SELECT pos_trans.`itemid`,pos_items.`itemdesc`,pos_items.`itemprice`,pos_trans.`qty`,(pos_items.`itemprice`) * (pos_trans.`qty`) AS amt
		FROM pos_trans INNER JOIN pos_items ON
		pos_trans.itemid = pos_items.itemid
		WHERE pos_trans.invoiceid='".$id."'");
		while($row=mysql_fetch_array($queryscart)){
			echo '
				<tr>
					<td>
						'.$row['itemid'].'
					</td>
					<td>
						'.$row['itemdesc'].'
					</td>
					<td>
						'.$row['itemprice'].'
					</td>
					<td>
						'.$row['qty'].'
					</td>
					<td>
						'.$row['amt'].'
					</td>
				</tr>
			';
		}
	}
	
	function CalcTotal($id){
		$queryscart=mysql_query("SELECT SUM(amt) AS TOTAL FROM
		(SELECT pos_trans.`itemid`,pos_items.`itemdesc`,pos_items.`itemprice`,pos_trans.`qty`,(pos_items.`itemprice`) * (pos_trans.`qty`) AS amt
		FROM pos_trans INNER JOIN pos_items ON
		pos_trans.itemid = pos_items.itemid
		WHERE pos_trans.invoiceid='".$id."') CartItems");
		while($row=mysql_fetch_array($queryscart)){
			return $row['TOTAL'];
		}
	}
	
	function CalcTotalwdisc($id,$scid){
		$scexist = False;
		$querygettype=mysql_query("SELECT * FROM `registration_barangay` WHERE `ID`='".$scid."'");
		while ($row=mysql_fetch_array($querygettype)){
			$scexist = True;
		}
		if ($scexist==True){
			MyPHPMsgBox ("5% Discount is applied for this transaction. " . CalcTotal($id) . " -> " . (CalcTotal($id)*.95));
			return (CalcTotal($id)*.95);
		}else{
			MyPHPMsgBox ("Senior Citizen ID not recognized");
			return CalcTotal($id);
		}
	}
	
	function Checkout($invid,$scid,$commodity,$da,$ab){
		$queryco=mysql_query("INSERT INTO `pos_trans_summary`(`invoiceid`, `scid`, `commodity`, `dateassessed`, `assessedby`, `total`) VALUES ('$invid','$scid','$commodity',CURDATE(),'$da','$ab')");
		MyPHPMsgBox ("Check out successful");
	}
	
	function DrawPurchaseBook($scid){
		$querypb=mysql_query("SELECT `pos_trans_summary`.`commodity` AS C, SUM(`pos_trans`.`qty`) AS QTY, `pos_trans_summary`.`total` AS T, `pos_trans_summary`.`dateassessed` AS DA, 'Elizabeth Supermarket' AS NOE
		FROM `pos_trans_summary` INNER JOIN `pos_trans` ON
		`pos_trans_summary`.`invoiceid` = `pos_trans`.`invoiceid`
		WHERE `pos_trans_summary`.`scid`='".$scid."'
		GROUP BY `pos_trans_summary`.`invoiceid`");
		echo '
			<table>
				<b><tr>
					<td>
						Commodity
					</td>
					<td>
						QTY
					</td>
					<td>
						Total
					</td>
					<td>
						Date Assessed
					</td>
					<td>
						Name of Establishment
					</td>
				</tr></b>
		';
		while($row=mysql_fetch_array($querypb)){
			echo '
				<tr>
					<td>
						'.$row['C'].'
					</td>
					<td>
						'.$row['QTY'].'
					</td>
					<td>
						'.$row['T'].'
					</td>
					<td>
						'.$row['DA'].'
					</td>
					<td>
						'.$row['NOE'].'
					</td>
				</tr>
			';
		}
		echo '';
		echo '</table>';
	}
?>