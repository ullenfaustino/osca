<?php

namespace SlimStarter\TwigExtension;
use \Slim;

use \Sentry;
use \User;
use \Users;

class Service extends \Twig_Extension
{
    public function getName()
    {
        return 'service';
    }

    public function getFunctions()
    {
        return array(
            new \Twig_SimpleFunction('getUserdetails', array($this, 'getUserdetails')),
        	new \Twig_SimpleFunction('getAvatarPath', array($this, 'getAvatarPath')),
        	new \Twig_SimpleFunction('toBase64', array($this, 'toBase64')),
        	new \Twig_SimpleFunction('decodeBase64', array($this, 'decodeBase64')),
        );
    }

	public function getAvatarPath() {
		return AVATAR_PATH;
	}

    public function getUserdetails($id) {
    	return User::getUserDetails($id)->first();
    }
    
    public function toBase64($str) {
        return base64_encode($str);
    }
    
    public function decodeBase64($hash) {
        return base64_decode($hash);
    }
}
