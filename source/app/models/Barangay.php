<?php
class Barangay extends Model {
    protected $table = "barangay";
 
    public function getAllBarangays() {
        $barangays = Barangay::leftJoin("town as T","T.id","=","barangay.town_id") 
                            -> select(array("barangay.id as id",
                                            "barangay.name as brgy_name",
                                            "T.name as town_name",
                                            "T.id as town_id",
                                            "barangay.created_at"))
                            -> get();
        return $barangays;
    }   
}
