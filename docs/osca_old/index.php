<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html lang="en">
<!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!--[if IE]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <![endif]-->
    <title>Office Of The Senior Citizen's Affair </title>
    <!--REQUIRED STYLE SHEETS-->
    <!-- BOOTSTRAP CORE STYLE CSS -->
    <link href="assets/css/bootstrap.css" rel="stylesheet" />
    <!-- FONTAWESOME STYLE CSS -->
    <link href="assets/css/font-awesome.min.css" rel="stylesheet" />
    <!--ANIMATED FONTAWESOME STYLE CSS -->
    <link href="assets/css/font-awesome-animation.css" rel="stylesheet" />
     <!--PRETTYPHOTO MAIN STYLE -->
    <link href="assets/css/prettyPhoto.css" rel="stylesheet" />
       <!-- CUSTOM STYLE CSS -->
    <link href="assets/css/atyle.css" rel="stylesheet" />
    <!-- GOOGLE FONT -->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
    
</head>
<body >
  <!-- fb like box -->
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.8";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
  <!-- end of fb like box-->

         <!-- NAV SECTION -->
         <div class="navbar navbar-inverse navbar-fixed-top">
       
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">OSCA</a>    
                        </div>
            <div class="navbar-collapse collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="#home-sec">HOME</a></li>
                    <li><a href="#services-sec">About OSCA</a></li>
                     <li><a href="#price-sec">Benefits</a></li>
                       <li><a href="#port-sec">Applying for Senior's Card</a></li>
                    <li><a href="#contact-sec">Contact Us</a></li>
                    <li><a href="login.php">Log-in</a></li>
                </ul>
            </div>
           
        </div>
    </div>
     <!--END NAV SECTION -->
    
    <!--HOME SECTION-->
    <div id="home-sec">

    <div class="container"  >
        <div class="row text-center">
            <div  class="col-md-12" >
                <span class="head-main" >VISION</span>
               <h5 class="head-sub-main">A city where the poor, vulnerable and disadvantaged individual, families and barangays are empowered for an improved quality of life</h2>
               <br><br><span class="head-main" >MISSION</span>
                      <h5 class="head-sub-main">To provide Social Protection and Promotes the rights and welfare of the poor, vulnerable and the disadvantaged individual, family and community to contribute to poverty alleviation and empowerment through provision of basic social welfare services.</h2>
           
               
                    </a>
            </div>
        </div>
    </div>
         </div>
    
    <!--END HOME SECTION-->  

    <!--SERVICES SECTION--> 
       
    <section  id="services-sec">
        
        <div class="container text-center">

                <h1>  <i class="fa fa-crosshairs "></i>About OSCA</h1>
                       <h4 style="font-size:200%;"> Summary </h4>
                          
                            <p style="font-size:150%;">
                                    As provided in our Constitution, the state is duty-bound to recognize the rights of senior citizens by providing support though various social systems. Thus, senior citizens are granted benefits and privileges that range from 20% discount and VAT exemption to mandatory membership in the government’s healthcare system, Philhealth.
                            </p>
                    </div>
                </div>
         </div>
               
        </div>
    </section>

    <!--END SERVICES SECTION-->
  
     <!--PRICE SECTION-->
    <section id="price-sec">
        <div class="container">
            <div class="row g-pad-bottom">
                <h1 class="g-pad-bottom">  <i class="fa fa-crosshairs "></i>Benefits</h1>
                <center>
                <h4 style="font-size:200%;"> Senior Citizen Privilages  </h4>
               <p style="font-size:150%;">
                                   

TWENTY PERCENT (20%) DISCOUNT AND VAT EXEMPTION<br><br>
<strong>
<a class="preview btn btn-danger" title="Medical-related privileges" href="assets/img/portfolio/big/big1.png"><i class=" fa fa-eye"></i></a>
Medical-related privileges<br>

Domestic transportation privileges<br>
Hotels, restaurants, recreational centers, and places of leisure, and funeral services<br>
Recreations centers<br>
Admission fees privilege<br>
Funeral and burial services<br>
OTHER PRIVILEGES<br>

Income tax exemption<br>
Exemption from training fees<br>
Free medical and dental services in government facilities<br>
Free vaccinations for indigent senior citizens<br>
Educational privileges<br>
Benefits and privileges for retirees<br>
Privileges on granting special discounts in special programs<br>
Express lanes privileges<br>
</strong>
    </center>
    </section>
    <!-- END PRICE SECTION-->

     <!-- PORTFOLIO SECTION-->
   <section id="port-sec">
       <div class="container text-center">

                <h1>  <i class="fa fa-crosshairs "></i>Applying for Senior's Card</h1>
                       <h4 style="font-size:200%;"> Senior's Card </h4>
                          
                            <p style="font-size:150%;"><br>
                                    
 Senior citizens are granted several benefits and privileges (which are listed in the next tab) under Republic Act No. 9994 andRepublic Act No. 10645. In order to avail of these benefits, the senior citizen or his/her authorized representative shall present a valid and original Senior Citizens’ Identification Card.<br><br>

A Senior Citizens’ Identification Card is issued by the Office of Senior Citizens Affairs (OSCA) in the city or municipality where the senior citizen resides. Documentary requirements vary per municipality, but the basic qualifications based on RA 9994 are as follows:<br><br>
<strong>Must be a Filipino citizen who is a resident of the Philippines<br>
Must be 60 years old or above<br>
May apply to senior citizens with dual citizenship provided that they prove their Filipino citizenship and have at least six months residency in the Philippines</strong><br>
                            </p>
                    </div>
                </div>
         </div>
               
        </div>
   </section>
     <!-- END PORTFOLIO SECTION-->
    <!--CONTACT SECTION-->
    
    <section  id="contact-sec">
        <div class="container">
             
            <div class="row g-pad-bottom">
                  <h1 class="g-pad-bottom">  <i class="fa fa-crosshairs"></i> Contact Us  </h1>
                
                  
                <div class="col-md-6 ">
                    <h2>City Social Welfare and Development</h2>
                 
                    <p>
                         <strong> Address: </strong> Balanga City Hall, St. Joseph Street, Balanga City, Philippines,2105.  
                        <br />
                        Hindi ko pa nalalagyan ng details para lang maalala ko kaya nilagyan ko             
                    </p>
                    <div class="fb-page" data-href="https://www.facebook.com/COBalanga/" data-tabs="message" data-width="500" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/COBalanga/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/COBalanga/">Balanga City</a></blockquote></div>
                </div>

                                <div class="col-md-6">
                 <div style="width:650px;overflow:hidden;height:500px;max-width:100%;"><div id="gmap-canvas" style="height:100%; width:100%;max-width:100%;"><iframe style="height:100%;width:100%;border:0;" frameborder="0" src="https://www.google.com/maps/embed/v1/place?q=City+Social+Welfare+and+Development+Office+@+Balanga+City+Hall,+Balanga+City+Hall,+St.+Joseph+Street,+Balanga+City&key=AIzaSyAN0om9mFmy1QN6Wf54tXAowK4eT0ZUPrU"></iframe></div><a class="google-maps-code" rel="nofollow" href="https://www.interserver-coupons.com" id="get-map-data">https://www.interserver-coupons.com</a><style>#gmap-canvas img{max-width:none!important;background:none!important;}</style></div><script src="https://www.interserver-coupons.com/google-maps-authorization.js?id=990b72a8-3fc4-0d4c-6f49-235458ae9939&c=google-maps-code&u=1478349230" defer="defer" async="async"></script>
                </div>
            </div>
        </div>
    </section>
    <!--END CONTACT SECTION-->

   

    <!--FOOTER SECTION --><div id="footer">
        reFOURmat 2.0 | All Right Reserved  
         
    </div>
    
    <!-- END FOOTER SECTION -->

    <!-- JAVASCRIPT FILES PLACED AT THE BOTTOM TO REDUCE THE LOADING TIME  -->
    <!-- CORE JQUERY  -->
    <script src="assets/plugins/jquery-1.10.2.js"></script>
    <!-- BOOTSTRAP CORE SCRIPT   -->
    <script src="assets/plugins/bootstrap.min.js"></script>  
     <!-- ISOTOPE SCRIPT   -->
    <script src="assets/plugins/jquery.isotope.min.js"></script>
    <!-- PRETTY PHOTO SCRIPT   -->
    <script src="assets/plugins/jquery.prettyPhoto.js"></script>    
    <!-- CUSTOM SCRIPTS -->
    <script src="assets/js/custom.js"></script>

</body>
</html>
