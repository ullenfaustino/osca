<!DOCTYPE html>
<?php
	include 'OSCAFunctions.php';
?>
<html>
	<head>
		<meta charset="utf-8">
		<title>OSCA - Mortality Rate</title>
		<meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no">
		<link rel="shortcut icon" href="favicon_16.ico"/>
		<link rel="bookmark" href="favicon_16.ico"/>
		<link rel="stylesheet" href="dist/css/site.min.css">
		<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,800,700,400italic,600italic,700italic,800italic,300italic" rel="stylesheet" type="text/css">
		<script type="text/javascript" src="dist/js/site.min.js"></script>
		<!--Script for Charts-->
		<script type="text/javascript" src="res/hc/jquery.min.js"></script>
		<script src="res/hc/highcharts.js"></script>
		<script src="res/hc/exporting.js"></script>
		<style type="text/css">
			${demo.css}
		</style>
		<script type="text/javascript">
			<?php
				StartPage();
				if (isset($_POST['df'])){
					DrawGraph_MortRate($_POST['df'],$_POST['dt']);
				}
			?>
		</script>
	</head>
	<body>
		<?php
			SESSION_START();
			if (!isset($_SESSION['userid'])){
				header("Location: login.php");
			}else{
				StartPage();
				$UserID=$_SESSION['userid'];
				$UserType=$_SESSION['usertype'];
				if ($_SESSION['usertype']=="Admin"){
				}else{
					Logout();
				}
			}
			DrawAdminHeader();
		?>
		<div class="container-fluid">
			<!--documents-->
			<div class="row row-offcanvas row-offcanvas-left">
				<?php DrawAdminSidebar(); ?>
				Mortality Rate
				<div id="frm">
					<form action="mortalityrate.php" method="POST">
						From: <input type="number" name="df" placeholder="Input FROM (year)" required/>
						To: <input type="number" name="dt" placeholder="Input TO (year)" required/>
						<br /><input type="submit">
					</form>
				<div>
				<br /><br />
				<div id="container" style="min-width: 310px; width:79%; height: 400px;"></div>
			</div>
		</div>
		</body>
</html>