<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>OSCA - User</title>
		<meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no">
		<link rel="shortcut icon" href="favicon_16.ico"/>
		<link rel="bookmark" href="favicon_16.ico"/>
		<link rel="stylesheet" href="dist/css/site.min.css">
		<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,800,700,400italic,600italic,700italic,800italic,300italic" rel="stylesheet" type="text/css">
		<script type="text/javascript" src="dist/js/site.min.js"></script>
	</head>
	<body>
		<?php
			include 'OSCAFunctions.php';
			SESSION_START();
			if (!isset($_SESSION['userid'])){
				header("Location: login.php");
			}else{
				StartPage();
				$UserID=$_SESSION['userid'];
				$UserType=$_SESSION['usertype'];
				if ($_SESSION['usertype']=="Admin"){
				}else{
					Logout();
				}
			}
			if (isset($_POST['UName'])){
				NewUser('Staff',$_POST['UName'],$_POST['PWord'],$_POST['address_value'],$_POST['RepeatPWord']);
			}
			
			DrawAdminHeader();
		?>
		<div class="container-fluid">
			<!--documents-->
			<div class="row row-offcanvas row-offcanvas-left">
				<?php DrawAdminSidebar(); ?>
				<div id="frm">
					<form action="adduser.php" method="POST">
					<label for="Barangay"> User Type:  </label><br>
					<u>Staff</u><br><br>
					<!--	<select id="UType" name="UType" >
						 <option value="Staff">Staff</option>
						 <option value="Admin">Admin</option>
					</select><br>-->
					
					<label>Username:</label> <br>
					<input type="text" name="UName" class="textInput" placeholder="Username" required><br>

					<label>Password:</label><br>
					<input type="password" name="PWord" class="textInput" placeholder="Password" required><br>

					<label>Repeat Password:</label><br>
					<input type="password" name="RepeatPWord" class="textInput" placeholder="Repeat Password" required><br>
					
					<label for="Barangay"> Barangay:  </label><br>
					<select id="adress_option" name="address_value" >
					<option value="Bagong Silang">Bagong Silang</option>
					<option value="Bgumbayan">Bgumbayan</option>
					<option value="Cabog-Cabog">Cabog-Cabog</option>
					<option value="Camacho">Camacho</option>
					<option value="Cataning">Cataning</option>
					<option value="Central">Central</option>
					<option value="Cupang North">Cupang North</option>
					<option value="Cupang West">Cupang West</option>
					<option value="Cupang Proper">Cupang Proper</option>
					<option value="Dangcol">Dangcol</option>
					<option value="Doña Francisca">Doña Francisca</option>
					<option value="Ibayo">Ibayo</option>
					<option value="Malabia">Malabia</option>
					<option value="Munting Batangas">Munting Batangas</option>
					<option value="Poblacion">Poblacion</option>
					<option value="Pto. Rivas Ibaba">Pto. Rivas Ibaba</option>
					<option value="Pto. Rivas Itaas">Pto. Rivas Itaas</option>
					<option value="Lote Pto. Rivas">Lote Pto. Rivas</option>
					<option value="San Jose">San Jose</option>
					<option value="Sibacan">Sibacan</option>
					<option value="Talisay">Talisay</option>
					<option value="Tanato">Tanato</option>
					<option value="Tenejero">Tenejero</option>
					<option value="Tortugas">Tortugas</option>
					<option value="Tuyo">Tuyo</option>
					</select><br><br>
					
					<input type="Submit" name="register_btn" value="Register">
					</form>
				</div>
			</div>
		</div>
	</body>
</html>