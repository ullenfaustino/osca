<?php

namespace AdminSCContribution\Controllers;

use \raw;
use \User;
use \Users;
use \App;
use \View;
use \Menu;
use \Input;
use \Sentry;
use \Request;
use \Response;
use \Exception;
use \Admin\BaseController;
use \Cartalyst\Sentry\Users\UserNotFoundException;
use \Illuminate\Database\Capsule\Manager as DB;

use \SCMembers;
use \Contributions;
use \Town;
use \Barangay;
use \BarangayUsers;
use \CodeGenerator;
use \GenericHelper;

class AdminSCContributionController extends BaseController {

	public function __construct() {
		parent::__construct();
		$this -> data['active_menu'] = 'admin_contribution';
	}

	/**
	 * display the admin dashboard
	 */
	public function index() {
		$user = Sentry::getUser();
		$this -> data['title'] = 'Senior Citizen List';
		
		$towns = Town::getAllTowns();
        $this -> data['towns'] = $towns;
        
		App::render('@adminsccontribution/index.twig', $this -> data);
	}
    
    public function contributionsView($sc_id) {
    	$this -> data['title'] = 'Member Contributions';
    	
    	$contributions = Contributions::where("sc_member_id", "=", $sc_id) -> get();
    	$this -> data['contributions'] = $contributions;
    	
    	$this -> data["member"] = SCMembers::find($sc_id);
    	$this -> data["months"] = GenericHelper::monthList();
    	$this -> data["str_months"] = base64_encode(json_encode(GenericHelper::monthList()));
    	
    	$month = new \DateTime('now');
		$month -> modify('first day of this month');
		$month = $month -> format('F');
		$this -> data["month_now"] = $month;
    	
    	App::render('@adminsccontribution/contributions.twig', $this -> data);
    }
    
    public function addContribution() {
    	$codeGen = new CodeGenerator();
    	
    	$today = new \DateTime('now');
		$today -> modify('first day of this month');
		$year = $today -> format('Y');
		$day = $today -> format('d');
		$time = $today -> format('h:i:s');
		
		$month = Input::post("month");
		$uptoMonth = Input::post("upto_month");
		
    	$contribution_id = Input::post("contribution_id");
    	$sc_id = Input::post("sc_id");
    	$barangay_id = Input::post("barangay_id");
    	// $amount = Input::post("amount");
    	$amount = 30;
    	
		$contribution = Contributions::find($contribution_id);
		if (!$contribution) {
			$contribution = new Contributions();
		}
		
		for ($i = $month; $i <= $uptoMonth; $i++) { 
			$hasContribution = Contributions::where("sc_member_id", "=", $sc_id)
											-> whereRaw(sprintf("MONTH(`created_at`) = '%s' AND YEAR(`created_at`) = '%s'", $i, $year)) -> first();
			if ($hasContribution) {
                App::flash('message_status', false);
                App::flash('message', "Oops, This member has already paid for this month contribution.");
                Response::redirect($this -> siteUrl('admin/contribution/view/' . $sc_id));
                return;
			} else {
				$dtpayment = sprintf("%s-%s-%s %s", $year, $i, $day, $time);
				$dtpayment = strtotime($dtpayment);
		
				$contribution = new Contributions();
				$contribution -> trans_code = $codeGen -> getCodes();
		    	$contribution -> sc_member_id = $sc_id;
		    	$contribution -> barangay_id = $barangay_id;
		    	$contribution -> amount = $amount;
		    	$contribution -> created_at = $dtpayment;
		    	$contribution -> save();
			}
		}
    	
    	App::flash('message', "Transaction Completed!");
        App::flash('message_status', true);
    	Response::redirect($this -> siteUrl('admin/contribution/view/' . $sc_id));
    }
    
    public function deleteContribution() {
    	$sc_id = Input::post("sc_id");
    	$contribution_id = Input::post("del_contribution_id");
    	Contributions::find($contribution_id) -> delete();
    	
    	App::flash('message', "Transaction Deleted!");
        App::flash('message_status', true);
        Response::redirect($this -> siteUrl('admin/contribution/view/' . $sc_id));
    }
    
    public function printReceipt($sc_id) {
    	$contribution = Contributions::leftJoin("sc_members as SC", "SC.id", "=", "contributions.sc_member_id")
    								-> where("contributions.sc_member_id", "=", $sc_id)
    								-> whereRaw(sprintf("MONTH(contributions.updated_at) = '%s'", date("m")))
    	                            -> select(array("*","contributions.created_at as created_at"))
    	                            -> first();
    	$this -> data['contribution'] = $contribution;
    	$this -> data['contribution_amount'] = Contributions::leftJoin("sc_members as SC", "SC.id", "=", "contributions.sc_member_id")
    														-> where("contributions.sc_member_id", "=", $sc_id)
						    								-> whereRaw(sprintf("MONTH(contributions.updated_at) = '%s'", date("m")))
						    	                            -> select(array("*","contributions.created_at as created_at"))
						    	                            -> sum("amount");
						    	                         
		$months = Contributions::leftJoin("sc_members as SC", "SC.id", "=", "contributions.sc_member_id")
							-> where("contributions.sc_member_id", "=", $sc_id)
							-> whereRaw(sprintf("MONTH(contributions.updated_at) = '%s'", date("m")))
                            -> select(array("*","contributions.created_at as created_at",
                            				new raw("Date_format(contributions.created_at, '%M') as month_label")))
                            -> get();
        $months = json_decode($months, TRUE);
        if (count($months) > 1) {
        	$this -> data['contribution_range'] = $months[0]['month_label'] . " to " . $months[count($months) -1]['month_label'];      
        } else {
        	$this -> data['contribution_range'] = $months[0]['month_label']; 
        }
    	
    	$staff = BarangayUsers::leftJoin("users as User","User.id","=","barangay_users.user_id")
    						-> where("barangay_id", "=", $contribution -> barangay_id) -> first();
    	$this -> data['staff'] = $staff;
    	
    	$today = new \DateTime('now');
        $today = $today -> format('F d, Y');
        $this -> data['_today'] = $today;
    	
    	App::render('@adminsccontribution/receipt_view.twig', $this -> data);
    }
    
    public function memberList() {
    	$barangay_id = Input::post("barangay_id");
        $limit = Input::post("length");
        $start = Input::post("start");
        $searchValue = trim(Input::post("search")['value'], '"');
        $order = Input::post("order")[0]['column'];
        $dir = Input::post("order")[0]['dir'];
        
        $members = SCMembers::leftJoin ( 'barangay as B', 'B.id', '=', 'sc_members.barangay_id' )
                            ->select ( array ('*',
                                            'sc_members.id as id',
                                            'B.name as barangay_name',
                                            'sc_members.created_at as created_at'))
                            -> where("sc_members.barangay_id","=",$barangay_id)
                            -> where("sc_members.is_deceased","=",0);
        $tmpMembers = $members;
        $totalCount = $tmpMembers->groupBy("sc_members.id")->get()->count();
        if (strlen ( $searchValue ) > 0) {
            $members = $members -> whereRaw("(sc_members.first_name like '%$searchValue%' or
        												 sc_members.rf_id like '%$searchValue%' or 
        												 sc_members.middle_name like '%$searchValue%' or
        												 sc_members.last_name like '%$searchValue%' or
        												 sc_members.created_at like '%$searchValue%' or
        												 B.name like '%$searchValue%')");
        }
        
        $tmpMemberFiltered = $members;
        $filteredCount = $tmpMemberFiltered->groupBy("sc_members.id")->get()->count();
        $members = $members->offset($start)->take($limit);
        $members = $members->get ();
        $arrMembers = array ();
        foreach ( $members as $key=>$member ) {
        	$bday = new \DateTime($member->birthday);
        	
            $tmp [] = sprintf("%s",$member->id);
            $tmp [] = sprintf("<a href='/admin/contribution/view/%s'>%s</a>", $member->id, $member->rf_id);
            $tmp [] = sprintf("%s",$member->first_name);
            $tmp [] = sprintf("%s",$member->middle_name);
            $tmp [] = sprintf("%s",$member->last_name);
            $tmp [] = sprintf("%s",$member->gender);
            $tmp [] = sprintf("%s",$bday->format('M d, Y'));
            $tmp [] = sprintf("%s",$member->address);
            $tmp [] = sprintf("%s",$member->barangay_name);
            $c_at = new \DateTime($member->created_at);
            $tmp [] = sprintf("%s",$c_at->format('M d, Y h:i:s a'));
            
            array_push($arrMembers, $tmp);
            unset($tmp);
        }
        
        $response ['draw'] = Input::post ( "draw" );
        $response ['recordsTotal'] = $totalCount;
        $response ['recordsFiltered'] = $filteredCount;
        $response ['data'] = $arrMembers;
        Response::setBody ( json_encode ( $response ) );
    }
}
