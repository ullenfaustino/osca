<?php

namespace AdminMortality;

use \App;
use \Menu;
use \Route;

class Initialize extends \SlimStarter\Module\Initializer {

	public function getModuleName() {
		return 'AdminMortality';
	}

	public function getModuleAccessor() {
		return 'adminmortality';
	}

	// public function registerMemberMenu(){
	// }

	public function registerAdminRoute() {
		Route::resource('/mortality', 'AdminMortality\Controllers\AdminMortalityController');
		
		Route::get('/mortality/month/:barangay_id/:year', 'AdminMortality\Controllers\AdminMortalityController:mortalityRatePerMonth');
		Route::get('/mortality/year/:barangay_id', 'AdminMortality\Controllers\AdminMortalityController:mortalityRatePerYear');
		Route::get('/mortality/yearly/:year', 'AdminMortality\Controllers\AdminMortalityController:mortalityBarangaysPerYear');
	}

}
