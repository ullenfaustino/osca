<?php

namespace AdminDashboard;

use \App;
use \Menu;
use \Route;

class Initialize extends \SlimStarter\Module\Initializer {

	public function getModuleName() {
		return 'AdminDashboard';
	}

	public function getModuleAccessor() {
		return 'admindashboard';
	}

	// public function registerMemberMenu(){
	// }

	public function registerAdminRoute() {
		Route::resource('/dashboard', 'AdminDashboard\Controllers\AdminDashboardController');
		
		Route::get('/dashboard/graph/sc/:town_id', 'AdminDashboard\Controllers\AdminDashboardController:sc_members');
		Route::get('/dashboard/view/details/sc/:sc_id', 'AdminDashboard\Controllers\AdminDashboardController:viewSCDetails');
		Route::get('/dashboard/graph/contributions/sc/:sc_id/:year', 'AdminDashboard\Controllers\AdminDashboardController:membersContributions');
		Route::get('/dashboard/graph/transactions/sc/:sc_id/:year', 'AdminDashboard\Controllers\AdminDashboardController:purchasedTransactions');
		
		Route::post('/dashboard/search/member', 'AdminDashboard\Controllers\AdminDashboardController:memberList') -> name("admin_dashboard_memberlist");
	}

}
