<?php

namespace AdminManagement\Controllers;

use \User;
use \Users;
use \App;
use \View;
use \Menu;
use \Input;
use \Sentry;
use \Request;
use \Response;
use \Exception;
use \Admin\BaseController;
use \Cartalyst\Sentry\Users\UserNotFoundException;

use \Town;
use \Barangay;
use \Establishment;

class AdminManagementController extends BaseController {

	public function __construct() {
		parent::__construct();
		$this -> data['active_menu'] = 'admin_management';
	}

	// public function index() {
	// $user = Sentry::getUser();
	// $this -> data['title'] = 'Registration';
	// App::render('@adminregistration/index.twig', $this -> data);
	// }
	
	public function establishmentView() {
		$user = Sentry::getUser();
		$this -> data['title'] = 'Establishment';
		$this -> data['sub_active_menu'] = 'admin_manage_establishment';
		
		$this -> data['establishments'] = Establishment::all();
		
		App::render('@adminmanagement/establishment.twig', $this -> data);
	}
	
	public function barangayView() {
        $user = Sentry::getUser();
        $this -> data['title'] = 'Barangay';
        $this -> data['sub_active_menu'] = 'admin_manage_barangay';
        
        $towns = Town::getAllTowns();
        $this -> data['towns'] = $towns;
        
        $barangays = Barangay::getAllBarangays();
        $this -> data['barangays'] = $barangays;
        
        App::render('@adminmanagement/barangay.twig', $this -> data);
    }
	
	public function addEstablishment() {
	    $establishment_id = Input::post("establishment_id");
	    $establishment_name = Input::post("establishment_name");
	    $establishment_desc = Input::post("establishment_desc");
	    $establishment_address = Input::post("establishment_address");
	    
	    $establishment = Establishment::find($establishment_id);
	    if (!$establishment) {
	        $establishment = new Establishment();
	    }
	    $establishment -> name = $establishment_name;
	    $establishment -> description = $establishment_desc;
	    $establishment -> address = $establishment_address;
	    $establishment -> save();
	    
	    App::flash('message', "Establishment successfully Saved.");
        App::flash('message_status', true);
        Response::redirect($this -> siteUrl('admin/management/view/establishment'));
	}
	
	public function addTown() {
	    $town_name = Input::post('town_name');
	    
	    $town = new Town();
	    $town -> name = $town_name;
	    $town -> save();
	    
	    App::flash('message', "Town successfully Saved.");
        App::flash('message_status', true);
        Response::redirect($this -> siteUrl('admin/management/view/barangay'));
	}
	
	public function addBarangay() {
	    $brgy_id = Input::post('brgy_id');
        $town = Input::post('town');
        $brgy_name = Input::post('brgy_name');
        
        $brgy = Barangay::find($brgy_id);
        if (!$brgy) {
            $brgy = new Barangay();
        }
        $brgy -> town_id = $town;
        $brgy -> name = $brgy_name;
        $brgy -> save();
        
        App::flash('message', "Barangay successfully Saved.");
        App::flash('message_status', true);
        Response::redirect($this -> siteUrl('admin/management/view/barangay'));
    }
}
