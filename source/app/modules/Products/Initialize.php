<?php

namespace Products;

use \App;
use \Menu;
use \Route;

class Initialize extends \SlimStarter\Module\Initializer {

    public function getModuleName() {
        return 'Products';
    }

    public function getModuleAccessor() {
        return 'products';
    }

    // public function registerMemberMenu(){
    // }

    public function registerEstablishmentRoute() {
        Route::resource('/products', 'Products\Controllers\ProductsController');
        
        Route::post('/products/add/new', 'Products\Controllers\ProductsController:addProducts') -> name("add_products");
    }

}
