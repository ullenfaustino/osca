<?php

define('APP_PATH'   , __DIR__.'/app/');

$config = array();
require __DIR__.'/vendor/autoload.php';
require __DIR__.'/app/config/database.php';

include __DIR__.'/src/Helper/CodeGenerator.php';;

use Illuminate\Database\Capsule\Manager as Capsule;
use Cartalyst\Sentry\Facades\Native\Sentry;
use Helper\CodeGenerator;
    
class Migrator{

    protected $config;

    public function __construct($config){
        $this->config = $config;
        $this->makeConnection($config);
    }

    /**
     * create connection to the database based on given configuration
     */
    private function makeConnection($config){
        try{
            $capsule = new Capsule;

            $capsule->addConnection($config);
            $capsule->setAsGlobal();
            $capsule->bootEloquent();

            Sentry::setupDatabaseResolver($capsule->connection()->getPdo());

        }catch(Exception $e){
            throw $e;
        }
    }

    /**
     * migrate the database schema
     */
    public function migrate(){
        /**
         * create table for sentry user
         */
        if (!Capsule::schema()->hasTable('users')){
            Capsule::schema()->create('users', function($table)
            {
                $table->increments('id');
                $table->string('email');
                $table->string('password');
                $table->text('permissions')->nullable();
                $table->boolean('activated')->default(0);
                $table->string('activation_code')->nullable();
                $table->timestamp('activated_at')->nullable();
                $table->timestamp('last_login')->nullable();
                $table->string('persist_code')->nullable();
                $table->string('reset_password_code')->nullable();
                $table->string('first_name')->nullable();
                $table->string('last_name')->nullable();
                $table->timestamps();

                // We'll need to ensure that MySQL uses the InnoDB engine to
                // support the indexes, other engines aren't affected.
                $table->engine = 'InnoDB';
                $table->unique('email');
                $table->index('activation_code');
                $table->index('reset_password_code');
            });
        }

        /**
         * create table for sentry group
         */
        if (!Capsule::schema()->hasTable('groups')){
            Capsule::schema()->create('groups', function($table)
            {
                $table->increments('id');
                $table->string('name');
                $table->text('permissions')->nullable();
                $table->timestamps();

                // We'll need to ensure that MySQL uses the InnoDB engine to
                // support the indexes, other engines aren't affected.
                $table->engine = 'InnoDB';
                $table->unique('name');
            });
        }

        /**
         * create user-group relation
         */
        if (!Capsule::schema()->hasTable('users_groups')){
            Capsule::schema()->create('users_groups', function($table)
            {
                $table->integer('user_id')->unsigned();
                $table->integer('group_id')->unsigned();

                // We'll need to ensure that MySQL uses the InnoDB engine to
                // support the indexes, other engines aren't affected.
                $table->engine = 'InnoDB';
                $table->primary(array('user_id', 'group_id'));
            });
        }

        /**
         * create throttle table
         */
        if (!Capsule::schema()->hasTable('throttle')){
            Capsule::schema()->create('throttle', function($table)
            {
                $table->increments('id');
                $table->integer('user_id')->unsigned();
                $table->string('ip_address')->nullable();
                $table->integer('attempts')->default(0);
                $table->boolean('suspended')->default(0);
                $table->boolean('banned')->default(0);
                $table->timestamp('last_attempt_at')->nullable();
                $table->timestamp('suspended_at')->nullable();
                $table->timestamp('banned_at')->nullable();

                // We'll need to ensure that MySQL uses the InnoDB engine to
                // support the indexes, other engines aren't affected.
                $table->engine = 'InnoDB';
                $table->index('user_id');
            });
        }
    }

    /**
     * seed the database with initial value
     */
    public function seed(){
        $codeGen = new CodeGenerator();
        
        Sentry::getGroupProvider()->create(array(
            'name'        => 'Administrators',
            'permissions' => array(
                'admin' => 1,
            ),
        ));
        Sentry::getGroupProvider()->create(array(
            'name'        => 'Establishment',
            'permissions' => array(
                'establishment' => 1,
            ),
        ));
        Sentry::getGroupProvider()->create(array(
            'name'        => 'Staff',
            'permissions' => array(
                'staff' => 1,
            ),
        ));
        
        for ($i=1; $i <= 2; $i++) { 
            try{
                $admin = Sentry::createUser([
                    'ref_code'    => $codeGen -> getReferenceCode(),
                    'username'    => 'admin' . $i,
                    'email'       => 'admin'. $i .'@osca.org',
                    'password'    => 'password',
                    'canonical_hash'    => base64_encode('password'),
                    'full_name'  => 'Administrator ' . $i,
                    'activated'   => 1,
                    'permissions' => array(
                        'admin'     => 1
                    ),
                    'user_type' => 1
                ]);
                $admin->addGroup( Sentry::getGroupProvider()->findByName('Administrators') );
            }catch(Exception $e){
                echo $e->getMessage()."\n";
            }
		}
        try{
            $est = Sentry::createUser([
                'ref_code'    => $codeGen -> getReferenceCode(),
                'username'    => 'store1',
                'email'       => 'store1@osca.org',
                'password'    => 'password',
                'canonical_hash'    => base64_encode('password'),
                'full_name'  => 'user1',
                'activated'   => 1,
                'permissions' => array(
                    'establishment'     => 1
                ),
                'user_type' => 3
            ]);
            $est->addGroup( Sentry::getGroupProvider()->findByName('Establishment') );
            
            $brgy = Sentry::createUser([
                'ref_code'    => $codeGen -> getReferenceCode(),
                'username'    => 'staff1',
                'email'       => 'staff1@osca.org',
                'password'    => 'password',
                'canonical_hash'    => base64_encode('password'),
                'full_name'  => 'brgy1',
                'activated'   => 1,
                'permissions' => array(
                    'staff'     => 1
                ),
                'user_type' => 2
            ]);
            $brgy->addGroup( Sentry::getGroupProvider()->findByName('Staff') );
        }catch(Exception $e){
            echo $e->getMessage()."\n";
        }
    }
}

$migrator = new Migrator($config['database']['connections'][$config['database']['default']]);

// $migrator->migrate();
$migrator->seed();
?>