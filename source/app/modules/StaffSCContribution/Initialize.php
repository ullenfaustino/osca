<?php

namespace StaffSCContribution;

use \App;
use \Menu;
use \Route;

class Initialize extends \SlimStarter\Module\Initializer {

	public function getModuleName() {
		return 'StaffSCContribution';
	}

	public function getModuleAccessor() {
		return 'staffsccontribution';
	}

	// public function registerMemberMenu(){
	// }

	public function registerStaffRoute() {
		Route::resource('/contribution', 'StaffSCContribution\Controllers\StaffSCContributionController');
		Route::get('/contribution/view/:sc_id', 'StaffSCContribution\Controllers\StaffSCContributionController:contributionsView');
		Route::get('/contribution/print/receipt/:sc_id', 'StaffSCContribution\Controllers\StaffSCContributionController:printReceipt');
		
		Route::post('/contribution/search/member', 'StaffSCContribution\Controllers\StaffSCContributionController:memberList') -> name("contribution_member_list");
		Route::post('/contribution/member/add', 'StaffSCContribution\Controllers\StaffSCContributionController:addContribution') -> name("contribution_member_add");
		Route::post('/contribution/member/delete', 'StaffSCContribution\Controllers\StaffSCContributionController:deleteContribution') -> name("contribution_member_delete");
	}

}
