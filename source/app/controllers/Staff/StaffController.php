<?php

namespace Staff;

use \App;
use \View;
use \Input;
use \Sentry;
use \Menu;
use \Response;

class StaffController extends BaseController {

	/**
	 * display the member dashboard
	 */
	public function index() {
		Response::Redirect($this -> siteUrl("staff/contribution"));
	}

}
