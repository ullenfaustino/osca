<?php

namespace Establishment;

use \App;
use \View;
use \Input;
use \Sentry;
use \Menu;
use \Response;

class EstablishmentController extends BaseController {

	/**
	 * display the member dashboard
	 */
	public function index() {
		Response::Redirect($this -> siteUrl("establishment/transaction"));
	}

}
