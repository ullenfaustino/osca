<?php

namespace AdminRegistration;

use \App;
use \Menu;
use \Route;

class Initialize extends \SlimStarter\Module\Initializer {

	public function getModuleName() {
		return 'AdminRegistration';
	}

	public function getModuleAccessor() {
		return 'adminregistration';
	}

	// public function registerMemberMenu(){
	// }

	public function registerAdminRoute() {
		Route::resource('/registration', 'AdminRegistration\Controllers\AdminRegistrationController');
		Route::get('/registration/staff/view', 'AdminRegistration\Controllers\AdminRegistrationController:staffRegistrationView');
		Route::get('/registration/establishment/view', 'AdminRegistration\Controllers\AdminRegistrationController:establishmentRegistrationView');
		Route::get('/registration/sc/view', 'AdminRegistration\Controllers\AdminRegistrationController:scRegistrationView');
		
		Route::post('/registration/staff/add', 'AdminRegistration\Controllers\AdminRegistrationController:addStaff') -> name("admin_add_staff");
		Route::post('/registration/staff/email', 'AdminRegistration\Controllers\AdminRegistrationController:sendCredentialsToEmail') -> name("send_to_email");
		
		Route::post('/registration/establishment/add', 'AdminRegistration\Controllers\AdminRegistrationController:addEstablishment') -> name("admin_add_establishment");
        Route::post('/registration/establishment/email', 'AdminRegistration\Controllers\AdminRegistrationController:sendCredentialsToEmailEstablishment') -> name("send_to_email_establishment");
	
		Route::post('/registration/sc/add/new', 'AdminRegistration\Controllers\AdminRegistrationController:addSCMember') -> name('admin_add_sc_member');
	}

}
