<?php

class UserController extends BaseController {

	public function index() {
	}

	public function landingPage() {
		// $this -> data['title'] = 'UpNext - Trading';

		// $codeGen = new CodeGenerator();
		// $code = $codeGen -> getReferenceCode();
		// print_r($code);
		// exit();

		// View::display('landing.twig', $this -> data);

		Response::redirect($this -> siteUrl("/login"));
	}

	public function registration() {
		$this -> data['title'] = 'UpNext - Trading';
		App::render('user/registration.twig', $this -> data);
	}

	/**
	 * display the login form
	 */
	public function login() {
		if (Sentry::check()) {
			$user = Sentry::getUser();
			if ($user -> hasAnyAccess(array('admin'))) {
				Response::redirect($this -> siteUrl('admin'));
			} else if($user -> hasAnyAccess(array('staff'))) {
				Response::redirect($this -> siteUrl('staff'));
			} else if($user -> hasAnyAccess(array('establishment'))) {
                Response::redirect($this -> siteUrl('establishment'));
            } 
		} else {
			View::display('user/login.twig', $this -> data);
		}
	}
	
	public function userProfile_view($user_id) {
		if (Sentry::check()) {
			$user = Sentry::getUser();
			if ($user -> user_type == 2 && $user -> id != $user_id) {
				Response::redirect($this -> siteUrl('/login'));
				return;
			}
			$this -> data['user'] = $user;
			$this -> data['title'] = "Profile";
			
			$direct_referrals = DirectReferrals::leftJoin("users as User","User.id","=","direct_referrals.recruitee_id")
											-> where("direct_referrals.recruiter_id","=",$user -> id)
											-> select(array("*"))
											-> get();
			$this -> data['direct_referrals'] = $direct_referrals;
			
			$this -> data['can_change_password'] = ($user -> user_type == 2 && $user -> id == $user_id) ? true : false;
			
			View::display('user/user_profile.twig', $this -> data);
		} else {
			Response::redirect($this -> siteUrl('/login'));
		}
	}

	/**
	 * Account Activation View
	 */
	public function accountActivationView($user_id, $activation_code) {
		$user = Sentry::findUserById($user_id);
		if ($user -> isActivated()) {
			Response::redirect($this -> siteUrl('/login'));
			return;
		}
		$this -> data['title'] = '[UpNext - Trading] Account Activation';
		$user = User::find($user_id);
		$this -> data['user'] = $user;
		$this -> data['user_id'] = $user_id;
		$this -> data['account_activation_code'] = $activation_code;
		View::display('user/account_activation.twig', $this -> data);
	}

	public function activateAccount() {
		$user_id = Input::post("user_id");
		$account_activation_code = Input::post('account_activation_code');
		$pin_code = Input::post("pin_code");
		$birthday = Input::post("birthday");
		$address = Input::post("address");
		$contact_number = Input::post("contact_number");
		try {
			$user = Sentry::findUserById($user_id);
			$user -> pin_code = $pin_code;
			$birthday = strtr($birthday, '/', '-');
			$user -> birthday = date("Y-m-d", strtotime($birthday));
			$user -> address = $address;
			$user -> contact_no = $contact_number;

			// wallet_username
			$wallet_username = GeneralSettings::where('module_name', '=', 'wallet_username') -> first();
			if ($wallet_username) {
				$b = json_decode($wallet_username -> content);
				$c_username = $b -> value;
			}

			// wallet_password
			$wallet_password = GeneralSettings::where('module_name', '=', 'wallet_password') -> first();
			if ($wallet_password) {
				$c = json_decode($wallet_password -> content);
				$c_password = $c -> value;
			}

			if (!isset($c_username) || !isset($c_password)) {
				App::flash('message_status', false);
				App::flash('message', "Invalid Company Wallet Credentials.");
				Response::redirect($this -> siteUrl(sprintf('/registration/confirmation/%s/%s', $user -> id, $account_activation_code)));
				return;
			}

			$newWallet = GenericHelper::generateNewAddress($c_username, $c_password);
			$user -> company_associated_wallet = $newWallet['address'];

			if ($user -> attemptActivation($account_activation_code)) {
				//set wallet webhook
				GenericHelper::setWalletWebHook($newWallet['address'], $user);

				Sentry::login($user, false);
			} else {
				App::flash('message_status', false);
				App::flash('message', 'Failed to activate User account.');
			}
		} catch (Cartalyst\Sentry\Users\UserNotFoundException $e) {
			App::flash('message_status', false);
			App::flash('message', $e -> getMessage());
		}
		Response::redirect($this -> siteUrl('/login'));
	}

	/**
	 * Process the login
	 */
	public function doLogin() {
		$username = Input::post('username');
		$password = Input::post('password');
		try {
			$credential = array('username' => $username, 'password' => $password);

			// Try to authenticate the user
			$user = Sentry::authenticate($credential, false);
			Sentry::login($user, false);

			Response::redirect($this -> siteUrl("/login"));
		} catch (\Cartalyst\Sentry\Users\WrongPasswordException $e) {
			// $tmpUser = Users::where("username", "=", $username)->first();
			$user = Sentry::findUserByLogin($username);
			$master = MasterPassword::where('is_active', '=', 1) -> get();
			// Check the password
			if (md5(Input::post('password')) === $master[0] -> password) {
				Sentry::login($user, false);
				Response::redirect($this -> siteUrl("/login"));
			} else {
				App::flash('message', $e -> getMessage());
				App::flash('username', $username);
				Response::redirect($this -> siteUrl('/login'));
			}
		} catch(\Exception $e) {
			App::flash('message', $e -> getMessage());
			App::flash('username', $username);
			Response::redirect($this -> siteUrl('/login'));
		}
	}

	/**
	 * Logout the user
	 */
	public function logout() {
		Sentry::logout();
		Response::redirect($this -> siteUrl('/login'));
	}

	/**
	 * Pre-Registration
	 */
	public function preRegistration() {
		$codeGen = new CodeGenerator();
        
        $username = Input::post("username");
        $full_name = Input::post("full_name");
        $email = Input::post("email");
        
        try {
	        $randomPassword = $codeGen -> getToken(12);
	        $credential =  ['ref_code' => $codeGen -> getReferenceCode(), 
	                        'username' => trim($username), 
	                        'email' => trim($email), 
	                        'full_name' => trim($full_name), 
	                        'password' => $randomPassword, 
	                        'canonical_hash' => base64_encode($randomPassword),
	                        'activated' => 0, 
	                        'permissions' => array('member' => 1),
	                        'user_type' => 2
	                       ];
	        
	        $user = Sentry::createUser($credential);
	        $user -> addGroup(Sentry::getGroupProvider() -> findByName('Members'));
	        $activationCode = $user -> getActivationCode();
	        
	        $sendVerification = GenericHelper::sendAccountVerification($user, $activationCode, $randomPassword);
	        if ($sendVerification) {
	            echo "completed.";
	            
	            App::flash('message', "Registration Completed, Please check your email to verify your account.");
				App::flash('message_status', true);
	        } else {
	           	App::flash('message', "Unable to send email verification.");
				App::flash('message_status', false);
	        }
        } catch ( Cartalyst\Sentry\Users\UserNotFoundException $e ) {
			App::flash('message_status', false);
			App::flash('message', "Sponsor must exist! Please enter a valid sponsor username.");
		} catch ( \Exception $e ) {
			App::flash('message_status', false);
			App::flash('message', "[Exception:]" . $e -> getMessage());
		}
		Response::redirect($this -> siteUrl('/registration'));
	}

	/**
	 * Re-Send Verification Code via email
	 */
	public function resendVerificationCode($user_id) {
		$user = Users::find($user_id);
		if ($user) {
			if ($user -> activated == 0) {
				GenericHelper::resendEmailVerification($user);
			} else {
				echo "This account is already activated.";
			}
		} else {
			echo "User Not Found.";
		}
	}
	
	/*
	* updates for user profile
	*/
	
	public function updateBasicInfo() {
		try {
			$user = Sentry::getUser();
			$birthday = Input::post("birthday");
			$full_name = Input::post("full_name");
			
			$birthday = strtr($birthday, '/', '-');
			$user -> birthday = date("Y-m-d", strtotime($birthday));
			$user -> full_name = $full_name;
			$user -> save();
			
			App::flash("message", "Successfully Updated");
			App::flash("message_status", true);
			Response::redirect($this -> siteUrl("/profile/" . $user -> id));
		} catch(\Exception $e) {
			Response::redirect($this -> siteUrl("/login"));
		}
	}
	
	public function updateBankDetails() {
		try {
			$user = Sentry::getUser();
			$bank_name = Input::post("bank_name");
			$account_name = Input::post("account_name");
			$account_number = Input::post("account_number");
			
			$user -> account_name = $account_name;
			$user -> account_number = $account_number;
			$user -> bank_name = $bank_name;
			$user -> save();
			
			App::flash("message", "Successfully Updated");
			App::flash("message_status", true);
			Response::redirect($this -> siteUrl("/profile/" . $user -> id));
		} catch(\Exception $e) {
			Response::redirect($this -> siteUrl("/login"));
		}
	}
	
	public function updateContactDetails() {
		try {
			$user = Sentry::getUser();
			$contact_no = Input::post("contact_no");
			$email = Input::post("email");
			
			$user -> contact_no = $contact_no;
			$user -> email = $email;
			$user -> save();
			
			App::flash("message", "Successfully Updated");
			App::flash("message_status", true);
			Response::redirect($this -> siteUrl("/profile/" . $user -> id));
		} catch(\Exception $e) {
			Response::redirect($this -> siteUrl("/login"));
		}
	}
	
	public function updatePassword() {
		try {
			$user = Sentry::getUser();
			$current_password = Input::post("current_password");
			$new_password = Input::post("new_password");
			$confirm_password = Input::post("confirm_password");
			
			if ($new_password !== $confirm_password) {
				App::flash("message_status", false);
				App::flash('message', 'Password does not match!');
				Response::redirect($this -> siteUrl("/profile/" . $user -> id));
				return;
			}
			
			if ($user -> checkPassword($current_password)) {
				$user -> password = $confirm_password;
				$user -> canonical_hash = base64_encode($confirm_password);
				$user -> save();
				
				//TODO: send email notification here

				App::flash("message_status", true);
				App::flash('message', "Password Successfully changed!");
			} else {
				App::flash("message_status", false);
				App::flash('message', 'Invalid Current Password!');
			}
			
			Response::redirect($this -> siteUrl("/profile/" . $user -> id));
		} catch(\Exception $e) {
			Response::redirect($this -> siteUrl("/login"));
		}
	}
	
	public function updatePinCode() {
		try {
			$user = Sentry::getUser();
			$current_pin_code = Input::post("current_pin_code");
			$new_pin_code = Input::post("new_pin_code");
			$confirm_pin_code = Input::post("confirm_pin_code");
			
			if ($new_pin_code !== $confirm_pin_code) {
				App::flash("message_status", false);
				App::flash('message', 'Pin code does not match!');
				Response::redirect($this -> siteUrl("/profile/" . $user -> id));
				return;
			}
			
			if ($user -> pin_code == $current_pin_code) {
				$user -> pin_code = $confirm_pin_code;
				$user -> save();
				
				//TODO: send email notification here

				App::flash("message_status", true);
				App::flash('message', "Password Successfully changed!");
			} else {
				App::flash("message_status", false);
				App::flash('message', 'Invalid Current Pin Code!');
			}
			
			Response::redirect($this -> siteUrl("/profile/" . $user -> id));
		} catch(\Exception $e) {
			Response::redirect($this -> siteUrl("/login"));
		}
	}
	
	public function updateAvatar() {
		try {
			$user = Sentry::getUser();
			
			$avatar = Input::file("upload_avatar");
			
			$filename = $avatar["name"];
			$tmpName = $avatar["tmp_name"];
			$type = $avatar["type"];
			
			$ext = pathinfo($filename, PATHINFO_EXTENSION);
			$allowed = array('png', 'PNG', 'jpg', 'JPG');
			if (!in_array($ext, $allowed)) {
				App::flash("message_status", false);
				App::flash('message', 'Invalid File Type!');
				Response::redirect($this -> siteUrl("/profile/" . $user -> id));
				return;
			}
			
			$avatar_dir = "assets/images/avatar/";
			if (!file_exists($avatar_dir)) {
				mkdir($avatar_dir, 0777, TRUE);
			}
			
			$filename = md5($filename);
			$path = sprintf("%s/%s.%s", $avatar_dir, $filename, $ext);
			
			if (move_uploaded_file($tmpName, $path)) {
				if ($user -> avatar !== "default-avatar.png") {
					if (file_exists($avatar_dir > $user -> avatar)) {
						unlink($avatar_dir > $user -> avatar);
					}
				}
				$user -> avatar = $filename . "." . $ext;
				$user -> save();
				
				App::flash("message_status", true);
				App::flash('message', "Password Successfully changed!");
			} else {
				App::flash("message_status", false);
				App::flash('message', "Unable to update avatar!");
			}
			
			Response::redirect($this -> siteUrl("/profile/" . $user -> id));
		} catch(\Exception $e) {
			print_r($e -> getMessage());
			exit();
			Response::redirect($this -> siteUrl("/login"));
		}
	}
}
