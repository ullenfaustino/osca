<?php
class SCMembers extends Model {
    protected $table = "sc_members";
 
    public function getAllMembers($brgy_id) {
        $members = SCMembers::leftJoin("barangay as B","B.id","=","sc_members.barangay_id") 
                            -> select(array("*", "sc_members.id as id"))
                            -> where("sc_members.barangay_id","=",$brgy_id)
                            -> get();
        return $members;
    }   
}
