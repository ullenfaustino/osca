<?php
class Users extends Model {
    protected $table = "users";

    public function getAllStaffs() {
        $staffs = Users::leftJoin("barangay_users as BU","BU.user_id","=","users.id")
                        -> leftJoin("barangay as B","B.id","=","BU.barangay_id") 
                        -> leftJoin("town as T","T.id","=","B.town_id")
                        -> select(array("users.id as id",
                                        "users.ref_code",
                                        "username", "canonical_hash",
                                        "T.name as town_name",
                                        "B.name as brgy_name",
                                        "B.id as brgy_id",
                                        "full_name", "email", 
                                        "users.created_at"
                                        ))
                        -> where("user_type", "=", 2) -> get();
        return $staffs;
    }
    
    public function getAllEstablishments() {
        $establishments = Users::leftJoin("establishment_users as EU","EU.user_id","=","users.id")
                        -> leftJoin("establishment as E","E.id","=","EU.establishment_id") 
                        -> select(array("users.id as id",
                                        "users.ref_code",
                                        "username", "canonical_hash",
                                        "E.name as establishment_name",
                                        "E.description as establishment_desc",
                                        "E.address as establishment_address",
                                        "E.id as establishment_id",
                                        "full_name", "email", 
                                        "users.created_at"
                                        ))
                        -> where("user_type", "=", 3) -> get();
        return $establishments;
    }

}
