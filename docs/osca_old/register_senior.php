<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>OSCA - Register Senior</title>
    <meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no">
    <link rel="shortcut icon" href="favicon_16.ico"/>
    <link rel="bookmark" href="favicon_16.ico"/>
    <!-- site css -->
    <link rel="stylesheet" href="dist/css/site.min.css">
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,800,700,400italic,600italic,700italic,800italic,300italic" rel="stylesheet" type="text/css">
    <script type="text/javascript" src="dist/js/site.min.js"></script>
	<script src="res\jq\external/jquery/jquery.js"></script>
	<script src="res\jq\jquery-ui.js"></script>
	<link href="res\jq\jquery-ui.css" rel="stylesheet">
  </head>
  <body>
		<?php
			include 'OSCAFunctions.php';
			SESSION_START();
			if (!isset($_SESSION['userid'])){
				header("Location: login.php");
			}else{
				StartPage();
				$UserID=$_SESSION['userid'];
				$UserType=$_SESSION['usertype'];
				if ($_SESSION['usertype']=="Admin"){
				}else{
					Logout();
				}
			}
			if (isset($_POST['SCID'])) {
				NewSC();
			}
			DrawAdminHeader();
		?>
    <div class="container-fluid">
        <div class="row row-offcanvas row-offcanvas-left">
			<?php DrawAdminSidebar(); ?>
        <div id="frm">
         <form action="register_senior.php" method="POST">
           <label>First Name:</label> <br>
              <input type="text" name="FirstName" class="textInput" placeholder="FirstName" required><br>
             
           <label>Middle Name:</label><br>
            <input type="text" name="MiddleName" class="textInput" placeholder="MiddleName" required><br>
              
             <label>Last Name:</label><br>
             <input type="text" name="LastName" class="textInput" placeholder="LastName" required><br>

          <label>Birthday:</label><br>
             <input id="datepicker" type="Birthday" name="Birthday" class="Birthday" placeholder="YYYY-MM-DD" required><br>

             <!--<label>Age:</label><br>
             <input type="Age" name="Age" class="textInput" placeholder="Age" required><br>-->
              
             <label>Gender:</label><br>
            <input type="Gender" name="Gender" class="textInput" placeholder="Male/Female" required><br>

                <label for="Barangay"> Barangay:  </label><br>
                <select id="adress_option" name="address_value" >
                 <option value="Unspecified">Select Barangay</option>
                 <option value="Bagong Silang">Bagong Silang</option>
                 <option value="Bgumbayan">Bgumbayan</option>
                 <option value="Cabog-Cabog">Cabog-Cabog</option>
                 <option value="Camacho">Camacho</option>
                 <option value="Cataning">Cataning</option>
                 <option value="Central">Central</option>
                 <option value="Cupang North">Cupang North</option>
                 <option value="Cupang West">Cupang West</option>
                 <option value="Cupang Proper">Cupang Proper</option>
                 <option value="Dangcol">Dangcol</option>
                 <option value="Doña Francisca">Doña Francisca</option>
                 <option value="Ibayo">Ibayo</option>
                 <option value="Malabia">Malabia</option>
                 <option value="Munting Batangas">Munting Batangas</option>
                 <option value="Poblacion">Poblacion</option>
                 <option value="Pto. Rivas Ibaba">Pto. Rivas Ibaba</option>
                 <option value="Pto. Rivas Itaas">Pto. Rivas Itaas</option>
                 <option value="Lote Pto. Rivas">Lote Pto. Rivas</option>
                 <option value="San Jose">San Jose</option>
                 <option value="Sibacan">Sibacan</option>
                 <option value="Talisay">Talisay</option>
                 <option value="Tanato">Tanato</option>
                 <option value="Tenejero">Tenejero</option>
                 <option value="Tortugas">Tortugas</option>
                 <option value="Tuyo">Tuyo</option>

                </select><br>
			
			<select id="SCStatus" name="SCStatus" style="display: none">
				<option value="Alive">Alive</option>
				<option value="Deceased">Deceased</option>
			</select>
			
            <label>Contact Number:</label><br>
             <input type="number" name="ContactNumber"  placeholder="ContactNumber" required>

            <label>Señior Citizen ID:</label> <br>
              <input type="text" name="SCID" class="textInput" placeholder="Swipe tag for new ID..." required><br><br>
           

             <label>Submit</label>
              <input type="Submit" name="register_btn" value="Register">

               
          
            </form>
      
</div>
<script>
	$( "#datepicker" ).datepicker({
		inline: true,
		dateFormat: 'yy-mm-dd'
	});
</script>
</body>
</html>
