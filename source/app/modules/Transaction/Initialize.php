<?php

namespace Transaction;

use \App;
use \Menu;
use \Route;

class Initialize extends \SlimStarter\Module\Initializer {

    public function getModuleName() {
        return 'Transaction';
    }

    public function getModuleAccessor() {
        return 'transaction';
    }

    // public function registerMemberMenu(){
    // }

    public function registerEstablishmentRoute() {
        Route::resource('/transaction', 'Transaction\Controllers\TransactionController');

        Route::get('/transaction/list/sc/:sc_id', 'Transaction\Controllers\TransactionController:transactionListView');
        Route::get('/transaction/new/sc/:sc_id', 'Transaction\Controllers\TransactionController:memberTransactionView');
        Route::get('/transaction/view/transcode/:trans_code/:sc_id', 'Transaction\Controllers\TransactionController:transactionsView');
        Route::post('/transaction/memberlist/search/member', 'Transaction\Controllers\TransactionController:transmembersList') -> name('search_members');
        Route::post('/transaction/add/new', 'Transaction\Controllers\TransactionController:saveTransaction') -> name('save_transaction');
    }

}
