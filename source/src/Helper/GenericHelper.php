<?php

namespace Helper;

use \Illuminate\Database\Capsule\Manager as DB;
use \Request;
use \Users;
use \User;

class GenericHelper {
	
	public function sendSMS($mobile, $message) {
		$codeGen = new CodeGenerator();
		$chika_api_url = "https://post.chikka.com/smsapi/request";
		$body = "message_type=SEND&mobile_number=" . $mobile . 
				"&shortcode=" . CHIKKA_SHORTCODE . 
				"&message_id=" . $codeGen -> getToken(8) . 
				"&message=" . $message . 
				"&client_id=" . CHIKKA_CLIENT_ID . 
				"&secret_key=" . CHIKKA_SECRET_KEY;
				
		$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL, $chika_api_url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $body); 
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: text/plain')); 
		
		$result=curl_exec ($ch);
		$data = json_decode($result);
		return ($data -> status == 200) ? true : false;
	}
	
    public function timeAgo($time_ago) {
        $time_ago = strtotime($time_ago);
        $cur_time = time();
        $time_elapsed = $cur_time - $time_ago;
        $seconds = $time_elapsed;
        $minutes = round($time_elapsed / 60);
        $hours = round($time_elapsed / 3600);
        $days = round($time_elapsed / 86400);
        $weeks = round($time_elapsed / 604800);
        $months = round($time_elapsed / 2600640);
        $years = round($time_elapsed / 31207680);
        // Seconds
        if ($seconds <= 60) {
            return "just now";
        }
        //Minutes
        else if ($minutes <= 60) {
            if ($minutes == 1) {
                return "one minute ago";
            } else {
                return "$minutes minutes ago";
            }
        }
        //Hours
        else if ($hours <= 24) {
            if ($hours == 1) {
                return "an hour ago";
            } else {
                return "$hours hrs ago";
            }
        }
        //Days
        else if ($days <= 7) {
            if ($days == 1) {
                return "yesterday";
            } else {
                return "$days days ago";
            }
        }
        //Weeks
        else if ($weeks <= 4.3) {
            if ($weeks == 1) {
                return "a week ago";
            } else {
                return "$weeks weeks ago";
            }
        }
        //Months
        else if ($months <= 12) {
            if ($months == 1) {
                return "a month ago";
            } else {
                return "$months months ago";
            }
        }
        //Years
        else {
            if ($years == 1) {
                return "one year ago";
            } else {
                return "$years years ago";
            }
        }
    }
    
    public function monthList() {
    	$months = array(
			array("month_label" => "January", "month" => 1),
			array("month_label" => "February", "month" => 2),
			array("month_label" => "March", "month" => 3),
			array("month_label" => "April", "month" => 4),
			array("month_label" => "May", "month" => 5),
			array("month_label" => "June", "month" => 6),
			array("month_label" => "July", "month" => 7),
			array("month_label" => "August", "month" => 8),
			array("month_label" => "September", "month" => 9),
			array("month_label" => "October", "month" => 10),
			array("month_label" => "November", "month" => 11),
			array("month_label" => "December", "month" => 12),
		);
		return json_decode(json_encode($months));
    }

    public function sendMail($sendTo, $subject, $contentBody = "") {
        try {
            $_username = 'support@upnext.shop';

            $mail = new \PHPMailer;

            $mail -> SMTPDebug = 0;

            $mail -> isSMTP();
            $mail -> Host = 'smtp.gmail.com';
            $mail -> SMTPAuth = true;
            $mail -> Username = 'upnexttrading@gmail.com';
            $mail -> Password = 'upnext-trading';
            $mail -> SMTPSecure = 'ssl';
            $mail -> Port = 465;

            $mail -> setFrom($_username, '[OSCA] - No reply');
            $mail -> addAddress($sendTo);
            // $mail -> addAddress('ellen@example.com');
            // $mail -> addReplyTo('info@example.com', 'Information');
            // $mail -> addCC('cc@example.com');
            // $mail -> addBCC('bcc@example.com');
            // $mail -> addAttachment('/var/tmp/file.tar.gz');
            $mail -> isHTML(true);

            $mail -> Subject = $subject;
            $mail -> Body = $contentBody;
            // $mail -> AltBody = 'This is the body in plain text for non-HTML mail clients';
            if (!$mail -> send()) {
                echo 'Message could not be sent.';
                echo 'Mailer Error: ' . $mail -> ErrorInfo;
                return 'Mailer Error: ' . $mail -> ErrorInfo;
            } else {
                echo 'Message has been sent';
                return true;
            }
        } catch(\Exception $e) {
            echo $e -> getMessage();
            return 'Mailer Error: ' . $e -> getMessage();
        }
    }

    public function baseUrl() {
        $path = dirname($_SERVER['SCRIPT_NAME']);
        $path = trim($path, '/');
        $baseUrl = Request::getUrl();
        $baseUrl = trim($baseUrl, '/');
        return $baseUrl . '/' . $path . ($path ? '/' : '');
    }

    public function sendAccountVerification($user, $activationCode, $password) {
        $to = $user -> email;
        $subject = "[UpNext - Trading] Account Verification";
        $body = "<p style='color: blue; font-weight: bold;'><b>Your have successfully Registered to UpNext Trading.</b><p>";
        $body .= "<p>";
        $body .= "<span>Username</span>: <b>" . $user -> username . "</b>";
        $body .= "</p>";
        $body .= "<p>";
        $body .= "<span>Password</span>: <b>" . $password . "</b>";
        $body .= "</p>";
        $body .= "<p>click here to verify your account </br>" . "<b>" . sprintf('%sregistration/confirmation/%s/%s', GenericHelper::baseUrl(), $user -> id, $activationCode) . "</b></p>";
        return GenericHelper::sendMail($to, $subject, $body);
    }

    public function resendEmailVerification($user) {
        $to = $user -> email;

        $subject = "[UpNext - Trading] Account Verification";
        $body = "<p style='color: blue; font-weight: bold;'><b>Your have successfully Registered to UpNext Trading.</b><p>";
        $body .= "<p>";
        $body .= "<span>Username</span>: <b>" . $user -> username . "</b>";
        $body .= "</p>";
        $body .= "<p>";
        $body .= "<span>Password</span>: <b>" . base64_decode($user -> canonical_hash) . "</b>";
        $body .= "</p>";
        $body .= "<p>click here to verify your account </br>" . "<b>" . sprintf('%sregistration/confirmation/%s/%s', GenericHelper::baseUrl(), $user -> id, $user -> activation_code) . "</b></p>";
        return GenericHelper::sendMail($to, $subject, $body);
    }

}
