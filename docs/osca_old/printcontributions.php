<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>OSCA - Print Contribution Report</title>
		<meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no">
		<link rel="shortcut icon" href="favicon_16.ico"/>
		<link rel="bookmark" href="favicon_16.ico"/>
		<!-- site css -->
		<link rel="stylesheet" href="dist/css/site.min.css">
		<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,800,700,400italic,600italic,700italic,800italic,300italic" rel="stylesheet" type="text/css">
		<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
		<script type="text/javascript" src="dist/js/site.min.js"></script>
		<style>
			table#t01 th td {
				border: 2px solid black;
				border-collapse: collapse;
				border-spacing: 10px;
				
			}
			table#t01 td{
				padding: 5px;
			}
		</style>
		<script src="res\jq\external/jquery/jquery.js"></script>
		<script src="res\jq\jquery-ui.js"></script>
		<link href="res\jq\jquery-ui.css" rel="stylesheet">
	</head>
	<body>
		<?php
			include 'OSCAFunctions.php';
			SESSION_START();
			if (!isset($_SESSION['userid'])){
				header("Location: login.php");
			}else{
				StartPage();
				$UserID=$_SESSION['userid'];
				$UserType=$_SESSION['usertype'];
			}
		?>
		<!--header-->

		<div class="container-fluid">
		<!--documents-->
			<?php 
				if (isset($_GET['printcontid'])){
					$id = $_GET['printcontid'];
					$con = mysqli_connect("localhost","root","","login");
					if (!$con) {
						die(mysql_error());
					}
					$results = $con->query("SELECT * FROM registration_barangay WHERE ID='$id'");
					$row=$results->fetch_array();
					if ($row['ID']==''){
						echo 'ID not existing <br /> <a href="addcontributions.php">Click here to go back to Contributions Page</a>';
					}else{
						if ($_SESSION['usertype']=="Staff"){
							if ($row['Address']!=$_SESSION['corrbrgy']){
								echo 'ID: <b>' . $row['ID'] . '</b><br /> Mismatch. The senior citizen\'s Barangay is <b>' . $row['Address'] . '</b>. This staff can add contributions under <b>' . $_SESSION['corrbrgy'] . '</b><br /> <a href="addcontributions.php">Click here to go back to Contributions Page</a>';
							}else{
								goto PrepAddCont;
							}
						}else{
							PrepAddCont:
							echo '
								<div id="PrintCont" style="width: 8.5in; border: thin black inset; margin: 0 auto; padding: 20px 20px 20px 20px">
									<table>
										<tr>
											<td rowspan=3 style="padding: 0px 25px 0px 25px">
												<img src="logo.png" style="width: 80px"/>
											</td>
											<td style="text-align: center">
												<b><font size="5">Online Senior Citizen Management System</font></b>
											</td>
										</tr>
										<tr>
											<td style="text-align: center">
												<b><font size="5">Senior Citizen Contribution Report</font></b>
											</td>
										</tr>
										<tr>
											<td style="text-align: center">
												<b><font size="5">OSCA</font></b>
											</td>
										</tr>
										<tr>
											<td style="text-align: center">
												<i>CSWD-OSCA</i>
											</td>
										<tr>
									</table>
									<br />
									<table id="t01" border=2 cellpadding style="margin: 0 auto">
										<tr>
											<td>
												ID: 
											</td>
											<td colspan=5>
												 '.$row['ID'].'
											</td>
										</tr>
										<tr>
											<td colspan=6>
												<br />
											</td>
										</tr>
										<tr>
											<td>
												First Name: 
											</td>
											<td>
												 '.$row['Fname'].'
											</td>
											<td>
												Middle Name: 
											</td>
											<td>
												 '.$row['Mname'].'
											</td>
											<td>
												Last Name: 
											</td>
											<td>
												 '.$row['Lname'].'
											</td>
										</tr>
										<tr>
											<td>
												Birthday: 
											</td>
											<td colspan=2>
												 '.$row['Birthday'].'
											</td>
											<td>
												Age: 
											</td>
											<td colspan=2>
												 '.date_diff(date_create($row['Birthday']), date_create('today'))->y.'
											</td>
										</tr>
										<tr>
											<td>
												Gender: 
											</td>
											<td colspan=2>
												 '.$row['Gender'].'
											</td>
											<td>
												Status: 
											</td>
											<td colspan=2>
												 '.$row['SCStatus'].'
											</td>
										</tr>
										<tr>
											<td>
												Address: 
											</td>
											<td colspan=2>
												 '.$row['Address'].'
											</td>
											<td>
												Contact Number: 
											</td>
											<td colspan=2>
												 '.$row['Contactnumber'].'
											</td>
										</tr>
										<tr>
											<td colspan=6>
												<br />
											</td>
										</tr>
										<tr>
											<td colspan=6>
												<b>Contributions</b>
											</td>
										</tr>
										<tr>
											<td colspan=3>
												<b>Date</b>
											</td>
											<td colspan=3>
												<b>Amount</b>
											</td>
										</tr>
										';
										
										$querygetcont=mysql_query("SELECT * FROM  `barangay_contributions` WHERE id='".$row['ID']."'  ORDER BY `Date` ASC");
										while ($rowcont=mysql_fetch_array($querygetcont)){
											echo '
												<tr>
													<td colspan=3>
														'.$rowcont['Date'].'
													</td>
													<td colspan=3>
														'.$rowcont['Contributions'].'
													</td>
												</tr>
											';
										}
										
										$querygetsumcont=mysql_query("SELECT SUM(Contributions) AS TC FROM  `barangay_contributions` WHERE id='".$row['ID']."'  ORDER BY `Date` ASC");
										while ($rowsumcont=mysql_fetch_array($querygetsumcont)){
											echo '
												<tr>
													<td colspan=3>
														<b>TOTAL</b>
													</td>
													<td colspan=3>
														<b>'.$rowsumcont['TC'].'</b>
													</td>
												</tr>
											';
										}
										
									echo'</table>
								</div>
							';
						}
					}
				}
			?>
		</div>
	</body>
<html>