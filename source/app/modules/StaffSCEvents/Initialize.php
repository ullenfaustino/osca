<?php

namespace StaffSCEvents;

use \App;
use \Menu;
use \Route;

class Initialize extends \SlimStarter\Module\Initializer {

	public function getModuleName() {
		return 'StaffSCEvents';
	}

	public function getModuleAccessor() {
		return 'staffscevents';
	}

	// public function registerMemberMenu(){
	// }

	public function registerStaffRoute() {
		Route::resource('/events', 'StaffSCEvents\Controllers\StaffSCEventsController');
		
		Route::get('/events/show/all', 'StaffSCEvents\Controllers\StaffSCEventsController:getAllEvents');
		Route::post('/events/add/new', 'StaffSCEvents\Controllers\StaffSCEventsController:setEvent') -> name("staff_add_event");
		Route::post('/events/delete', 'StaffSCEvents\Controllers\StaffSCEventsController:deleteEvent') -> name("staff_delete_event");
	}

}
