<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>OSCA - Update Senior</title>
		<meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no">
		<link rel="shortcut icon" href="favicon_16.ico"/>
		<link rel="bookmark" href="favicon_16.ico"/>
		<!-- site css -->
		<link rel="stylesheet" href="dist/css/site.min.css">
		<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,800,700,400italic,600italic,700italic,800italic,300italic" rel="stylesheet" type="text/css">
		<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
		<script type="text/javascript" src="dist/js/site.min.js"></script>
		<script src="res\jq\external/jquery/jquery.js"></script>
		<script src="res\jq\jquery-ui.js"></script>
		<link href="res\jq\jquery-ui.css" rel="stylesheet">
		<style>
			table, th, td {
			border: 2px solid black;
			border-collapse: collapse;
			border-spacing: 10px;
			}
			th,td{
			padding: 5px;

			}
		</style>
	</head>
	<body>
		<?php
			include 'OSCAFunctions.php';
			SESSION_START();
			if (!isset($_SESSION['userid'])){
				header("Location: login.php");
			}else{
				StartPage();
				$UserID=$_SESSION['userid'];
				$UserType=$_SESSION['usertype'];
				if ($_SESSION['usertype']=="Admin"){
				}else{
					Logout();
				}
			}
			DrawAdminHeader();
		?>
		<!--header-->

		<div class="container-fluid">
		<!--documents-->
			<div class="row row-offcanvas row-offcanvas-left">
				<?php 
					DrawAdminSidebar();
					// include_once 'update_checker.php';
					echo '
						Purchase Book
						<div id="frm">
							<form action="purchasebook.php" method="GET">
								Search: <input type="text" name="scid" placeholder="Swipe RFID Tag"/>
							</form>
						<div>
						<br /><br />
					';
					if (isset($_GET['scid'])) {
						StartPage();
						DrawPurchaseBook ($_GET['scid']);
					}
				?>
			</div>
		</div>
		<script>
			$( "#datepicker" ).datepicker({
				inline: true,
				dateFormat: 'yy-mm-dd'
			});
		</script>
	</body>
</html>